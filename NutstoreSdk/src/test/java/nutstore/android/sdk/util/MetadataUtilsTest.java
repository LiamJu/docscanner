package nutstore.android.sdk.util;

import org.junit.Assert;
import org.junit.Test;

import nutstore.android.sdk.module.Metadata;

/**
 * @author Zhu Liang
 */
public class MetadataUtilsTest {
    @Test
    public void getPath() throws Exception {
        Metadata metadata = new Metadata();
        metadata.setPath("/a");
        String path = MetadataUtils.getPath(metadata, "t.txt");
        Assert.assertEquals("/a/t.txt", path);

        metadata.setPath("/a/");
        path = MetadataUtils.getPath(metadata, "t.txt");
        Assert.assertEquals("/a/t.txt", path);
    }

}