package nutstore.android.sdk;

import org.junit.Test;
import org.reactivestreams.Publisher;

import java.util.Locale;
import java.util.UUID;

import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import nutstore.android.sdk.internal.HttpConfig;
import nutstore.android.sdk.module.Metadata;
import nutstore.android.sdk.module.Sandbox;
import nutstore.android.sdk.module.UserInfo;
import nutstore.android.sdk.util.SandboxUtils;
import okhttp3.RequestBody;

/**
 * @author Zhu Liang
 */
public class NutstoreUploadFileApiTest {

    private NutstoreAPI nutstoreAPI;
    private NutstoreUploadFileApi nutstoreUploadFileApi;

    /**
     * 测试上传文件接口
     *
     * @throws Exception
     */
    @Test
    public void uploadFile() throws Exception {
        initNutstoreAPI_Token();

        // NutstoreSdk

        nutstoreAPI
                .getUserInfoV2()
                .flatMap(new Function<UserInfo, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(UserInfo userInfo) throws Exception {
                        return Flowable.fromIterable(userInfo.getSandboxes());
                    }
                })
                .filter(new Predicate<Sandbox>() {
                    @Override
                    public boolean test(Sandbox sandbox) throws Exception {
                        return sandbox.getName().equals("NutstoreSdk");
                    }
                })
                .first(Sandbox.DEFAULT)
                .toFlowable()
                .subscribe(
                        new Consumer<Sandbox>() {
                            @Override
                            public void accept(Sandbox sandbox) throws Exception {
                                if (sandbox != Sandbox.DEFAULT) {
                                    uploadFileInternal(sandbox);
                                }
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                System.out.println(throwable);
                            }
                        }
                );
    }

    private void uploadFileInternal(Sandbox sandbox) {

        RequestBody requestBody = RequestBody.create(null, "test_upload_file");

        nutstoreUploadFileApi
                .uploadFile(SandboxUtils.encodeSandboxId(sandbox),
                        SandboxUtils.encodeMagic(sandbox),
                        "/test_upload_file.txt", requestBody)
                .subscribe(
                        new Consumer<Metadata>() {
                            @Override
                            public void accept(Metadata metadata) throws Exception {
                                System.out.println(metadata);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                System.out.println(throwable);
                            }
                        }
                );
    }

    private void initNutstoreAPI_Token() {
        String authorization = HttpConfig.getAuthorization("droideep@163.com", HttpConfig.uuidToString(UUID.randomUUID()), "lNpYz9tiUjiTU5TKcJWGe9ATu5JfMbMwMc1aQ5AdHAI=");
        nutstoreAPI = ApiManager.getNutstoreAPI("https://app.jianguoyun.com",
                authorization, Locale.getDefault().equals(Locale.PRC) ? Locale.PRC.toString() : Locale.US.toString(),
                HttpConfig.getUserAgent("1.0.0"));

        nutstoreUploadFileApi = ApiManager.getNutstoreUploadFileApi("https://app.jianguoyun.com",
                authorization, Locale.getDefault().equals(Locale.PRC) ? Locale.PRC.toString() : Locale.US.toString(),
                HttpConfig.getUserAgent("1.0.0"));
    }

}