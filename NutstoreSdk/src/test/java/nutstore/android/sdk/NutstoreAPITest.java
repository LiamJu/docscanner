package nutstore.android.sdk;

import org.junit.Assert;
import org.junit.Test;
import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.reactivex.Flowable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.subscribers.TestSubscriber;
import nutstore.android.sdk.consts.PathConsts;
import nutstore.android.sdk.internal.HttpConfig;
import nutstore.android.sdk.module.CaptchaResponse;
import nutstore.android.sdk.module.CreateSandboxInfo;
import nutstore.android.sdk.module.LoginBody;
import nutstore.android.sdk.module.Metadata;
import nutstore.android.sdk.module.MetadataList;
import nutstore.android.sdk.module.PathInternal;
import nutstore.android.sdk.module.Sandbox;
import nutstore.android.sdk.module.UserInfo;
import nutstore.android.sdk.module.VerifyEmailBody;
import nutstore.android.sdk.module.VerifyEmailResponse;
import nutstore.android.sdk.module.VerifyResult;
import nutstore.android.sdk.util.MetadataUtils;
import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.SandboxUtils;
import retrofit2.Call;

import static java.lang.System.err;
import static java.lang.System.out;

/**
 * @author Zhu Liang
 */
public class NutstoreAPITest {

    public static final String DISPLAY_NAME = "坚果扫描";

    private NutstoreAPI mNutstoreAPI;

    /**
     * 测试登录接口（没有设置验证码）
     */
    @Test
    public void loginV1() throws Exception {
        initNutstoreAPI_NoToken();
        mNutstoreAPI
                .loginV1(new LoginBody("droideep@163.com", "Zl4059878/", null))
                .subscribe(
                        new Consumer<VerifyResult>() {
                            @Override
                            public void accept(VerifyResult verifyResult) throws Exception {
                                out.println(verifyResult);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                err.println(throwable);
                            }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                out.println("onComplete");
                            }
                        });
    }

    /**
     * 测试登录接口失败（需要微信验证码）
     */
    @Test
    public void loginV1_Failed_NeedWechatAuthentication() throws Exception {
        initNutstoreAPI_NoToken();
        mNutstoreAPI
                .loginV1(new LoginBody("liamju@163.com", "Zl4059878", null))
                .subscribe(
                        new Consumer<VerifyResult>() {
                            @Override
                            public void accept(VerifyResult verifyResult) throws Exception {
                                out.println(verifyResult);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                err.println(throwable);
                            }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                out.println("onComplete");
                            }
                        });
    }

    @Test
    public void verifyWxSso() throws Exception {
        initNutstoreAPI_NoToken();
        TestSubscriber<VerifyResult> testSubscriber = new TestSubscriber<>();
        mNutstoreAPI.verifyWxSso("001V7V8R1H6wfa1NLd7R1hfy8R1V7V8Z").subscribe(testSubscriber);
        Preconditions.checkNotNull(testSubscriber.values().get(0));
    }

    @Test
    public void bindWxSso() throws Exception {
    }

    @Test
    public void getFallbackCaptchaV1() throws Exception {
        initNutstoreAPI_NoToken();

        TestSubscriber<CaptchaResponse> testSubscriber = new TestSubscriber<>();
        mNutstoreAPI.getFallbackCaptchaV1().subscribe(testSubscriber);
        Assert.assertNotNull(testSubscriber.values());
    }

    @Test
    public void verifyEmailV1() throws Exception {

        initNutstoreAPI_NoToken();

        mNutstoreAPI
                .getFallbackCaptchaV1()
                .flatMap(new Function<CaptchaResponse, Publisher<VerifyEmailResponse>>() {
                    @Override
                    public Publisher<VerifyEmailResponse> apply(CaptchaResponse captchaResponse) throws Exception {

                        VerifyEmailBody body = new VerifyEmailBody();
                        body.setEmail("liamju@163.com");
                        body.setCustom_ticket(captchaResponse.getCustom_ticket());
                        body.setExp(captchaResponse.getExp());
                        body.setSig(captchaResponse.getSig());
                        body.setReusable(captchaResponse.getReusable());
                        return mNutstoreAPI.verifyEmailV1(body);
                    }
                })
                .subscribe(new Consumer<VerifyEmailResponse>() {
                    @Override
                    public void accept(VerifyEmailResponse verifyEmailResponse) throws Exception {
                        Assert.assertNotNull(verifyEmailResponse);
                    }
                });
    }

    @Test
    public void getUserInfo() throws Exception {
        initNutstoreAPI_Token();
        TestSubscriber<UserInfo> testSubscriber = new TestSubscriber<>();
        mNutstoreAPI.getUserInfoV2(1).subscribe(testSubscriber);

        Assert.assertNotNull(testSubscriber.values());
    }

    /**
     * 获取"坚果扫描"同步文件夹
     * 测试接口：
     * 1. {@link NutstoreAPI#getUserInfoV2()},
     * 2. {@link NutstoreAPI#createSandboxV1(CreateSandboxInfo)},
     * 3. {@link NutstoreAPI#getMetadataList(String, String, String, String, String)}
     */
    @Test
    public void getSandboxScanner() throws Exception {
        initNutstoreAPI_Token();

        mNutstoreAPI
                .getUserInfoV2() // 测试 NutstoreAPI#getUserInfoV2
                .flatMap(new Function<UserInfo, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(UserInfo userInfo) throws Exception {
                        return Flowable.fromIterable(userInfo.getSandboxes());
                    }
                })

                .filter(new Predicate<Sandbox>() {
                    @Override
                    public boolean test(Sandbox sandbox) throws Exception {
                        String name = sandbox.getName();
                        return name.equals(DISPLAY_NAME);
                    }
                })
                .first(Sandbox.DEFAULT)
                .toFlowable()
                .flatMap(new Function<Sandbox, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(Sandbox sandbox) throws Exception {
                        if (sandbox != Sandbox.DEFAULT) {
                            // 如果同步文件夹已存在，则直接返回
                            return Flowable.just(sandbox);
                        } else {
                            // 如果同步文件夹不存在，则创建这个同步文件夹
                            CreateSandboxInfo csi = new CreateSandboxInfo(DISPLAY_NAME, true);
                            // 测试 NutstoreAPI#createSandboxV1
                            return mNutstoreAPI.createSandboxV1(csi);
                        }
                    }
                })
               /* .flatMap(new Function<Sandbox, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(Sandbox sandbox) throws Exception {
                        return mNutstoreAPI.getSandboxDetailV1(SandboxUtils.encodeSandboxId(sandbox),
                                SandboxUtils.encodeMagic(sandbox));
                    }
                })*/
                .map(new Function<Sandbox, List<Metadata>>() {
                    @Override
                    public List<Metadata> apply(Sandbox sandbox) throws Exception {
                        Call<MetadataList> metadataListCall = mNutstoreAPI.getMetadataList(SandboxUtils.encodeSandboxId(sandbox),
                                SandboxUtils.encodeMagic(sandbox),
                                "/",
                                null, null);
                        return metadataListCall.execute().body().getContents();
                    }
                })
                .subscribe(
                        new Consumer<List<Metadata>>() {
                            @Override
                            public void accept(List<Metadata> metadataList) throws Exception {
                                out.println(metadataList);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                err.println(throwable);
                            }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                out.println("onComplete");
                            }
                        });

    }

    @Test
    public void sendTfSms() throws Exception {
        initNutstoreAPI_NoToken();
        mNutstoreAPI
                .sendTfSms(new LoginBody("liamju@163.com", "Zl4059878", null))
                .subscribe(
                        new Consumer<Void>() {
                            @Override
                            public void accept(Void aVoid) throws Exception {
                                out.println("onNext()");
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                err.println(throwable);
                            }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                out.println("onComplete()");
                            }
                        }
                );

    }

    /**
     * 获取"坚果扫描"同步文件夹下所有子文件
     */
    @Test
    public void listNutScanMetadatas() throws Exception {
        initNutstoreAPI_Token();

        mNutstoreAPI.getUserInfoV2()
                .flatMap(new Function<UserInfo, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(UserInfo userInfo) throws Exception {
                        return Flowable.fromIterable(userInfo.getSandboxes());
                    }
                })
                .filter(new Predicate<Sandbox>() {
                    @Override
                    public boolean test(Sandbox sandbox) throws Exception {
                        String name = sandbox.getName();
                        return name.equals(DISPLAY_NAME);
                    }
                })
                .first(Sandbox.DEFAULT)
                .toFlowable()
                .flatMap(new Function<Sandbox, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(Sandbox sandbox) throws Exception {
                        if (sandbox != Sandbox.DEFAULT) {
                            // 如果同步文件夹已存在，则直接返回
                            return Flowable.just(sandbox);
                        } else {
                            // 如果同步文件夹不存在，则创建这个同步文件夹
                            CreateSandboxInfo csi = new CreateSandboxInfo(DISPLAY_NAME, true);
                            return mNutstoreAPI.createSandboxV1(csi);
                        }
                    }
                })
                .map(new Function<Sandbox, List<Metadata>>() {
                    @Override
                    public List<Metadata> apply(Sandbox sandbox) throws Exception {
                        String tag = null;
                        String marker = null;
                        List<Metadata> metadatas = new ArrayList<>();
                        MetadataList metadataList;
                        do {
                            Call<MetadataList> metadataListCall = mNutstoreAPI.getMetadataList(SandboxUtils.encodeSandboxId(sandbox),
                                    SandboxUtils.encodeMagic(sandbox), PathConsts.PATH_SEPARATOR, tag, marker);
                            metadataList = metadataListCall.execute().body();

                            tag = metadataList.getEtag();

                            List<Metadata> contents = metadataList.getContents();
                            if (contents != null && !contents.isEmpty()) {
                                marker = MetadataUtils.getDisplayName(contents.get(contents.size() - 1));
                            }

                            metadatas.addAll(contents);

                        } while (metadataList.isTruncated() && marker != null);

                        return metadatas;
                    }
                })
                .subscribe(
                        new Consumer<List<Metadata>>() {
                            @Override
                            public void accept(List<Metadata> metadata) throws Exception {
                                out.println(metadata);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                err.print(throwable);
                            }
                        }
                );
    }

    /**
     * 测试创建文件夹接口
     */
    @Test
    public void mkdir() throws Exception {
        initNutstoreAPI_Token();

        mNutstoreAPI
                .getUserInfoV2()
                .flatMap(new Function<UserInfo, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(UserInfo userInfo) throws Exception {
                        return Flowable.fromIterable(userInfo.getSandboxes());
                    }
                })
                .filter(new Predicate<Sandbox>() {
                    @Override
                    public boolean test(Sandbox sandbox) throws Exception {
                        return sandbox.isIsDefault();
                    }
                })
                .firstOrError()
                .toFlowable()
                .map(new Function<Sandbox, Metadata>() {
                    @Override
                    public Metadata apply(Sandbox sandbox) throws Exception {
                        Call<Metadata> call = mNutstoreAPI.mkdir(SandboxUtils.encodeSandboxId(sandbox),
                                SandboxUtils.encodeMagic(sandbox), new PathInternal("/坚果云相册"));
                        return call.execute().body();
                    }
                })
                .subscribe(
                        new Consumer<Metadata>() {
                            @Override
                            public void accept(Metadata metadata) throws Exception {
                                out.println(metadata);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                err.println(throwable);
                            }
                        });
    }

    private void initNutstoreAPI_NoToken() {
        mNutstoreAPI = ApiManager.getNutstoreAPI("https://app.jianguoyun.com",
                null, Locale.getDefault().equals(Locale.PRC) ? Locale.PRC.toString() : Locale.US.toString(),
                HttpConfig.getUserAgent("1.0.0"));

        //
    }

    private void initNutstoreAPI_Token() {
        String authorization = HttpConfig.getAuthorization("droideep@163.com", HttpConfig.uuidToString(UUID.randomUUID()), "lNpYz9tiUjiTU5TKcJWGe9ATu5JfMbMwMc1aQ5AdHAI=");
        mNutstoreAPI = ApiManager.getNutstoreAPI("https://app.jianguoyun.com",
                authorization, Locale.getDefault().equals(Locale.PRC) ? Locale.PRC.toString() : Locale.US.toString(),
                HttpConfig.getUserAgent("1.0.0"));
    }

}