package nutstore.android.sdk.internal;

import android.os.Build;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.UUID;

import nutstore.android.sdk.exception.FatalException;

/**
 * @author Zhu Liang
 */

public class HttpConfig {

    public static final String AUTHORIZATION = "Authorization";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String USER_AGENT = "User-Agent";
    public static final String CONTENT_TYPE = "Content-Type";

    public static final String GZIP = "application/gzip";

    public static final String UTF_8 = "UTF-8";

    public static String getAcceptLanguage() {
        return Locale.getDefault().toString();
    }

    public static String getUserAgent(String versionName) {
        return String.format(
                "%s-%s-%s-%s",
                "NutstoreApp-Android",
                Build.VERSION.RELEASE,
                Build.MODEL,
                versionName);
    }

    public static String getAuthorization(String userName, String machineName, String token) {
        return String.format("%s-%s:%s",
                stringToUTF8Base64(userName),
                stringToUTF8Base64(machineName),
                token);
    }

    public static String getSndId(long sndId) {
        return toHexString(sndId);
    }

    public static String getSndMagic(long sndMagic) {
        return toHexString(sndMagic);
    }

    private static String toHexString(long l) {
        return Long.toHexString(l);
    }

    /**
     * Encode the input UTF8 string to Base64 string
     *
     * @param str the imput string
     * @return base64 string
     */
    public static String stringToUTF8Base64(String str) {
        return bytesToBase64(stringToUTF8Bytes(str));
    }

    /**
     * Encode a bytes array into Base64 string
     *
     * @param bytes Bytes array to be encoded
     * @return Base64 encoded string, null if no bytes are specified
     */
    public static String bytesToBase64(byte[] bytes) {
        return new String(Base64Coder.encode(bytes));
    }

    /**
     * @param str String to be encoded
     * @return Array of UTF-8 bytes, null if no string is specified
     */
    public static byte[] stringToUTF8Bytes(String str) {
        if (isNullOrEmpty(str)) return null;
        return getBytes(str, UTF_8);
    }

    private static byte[] getBytes(String str, String charsetName) {
        try {
            return str.getBytes(charsetName);
        } catch (UnsupportedEncodingException e) {
            throw new FatalException(e);
        }
    }

    /**
     * Generate a compact url safe base 64 encoding of UUID.
     *
     * @param uuid the UUID
     * @return compact base64 presentation of UUID.
     */
    public static String uuidToString(UUID uuid) {
        byte[] bytes = uuidToBytes(uuid);
        return bytesToBase64(bytes);
    }

    public static byte[] uuidToBytes(UUID uuid) {
        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();

        byte[] bytes = new byte[2 * 8];
        ByteBuffer buf = ByteBuffer.wrap(bytes);
        buf.putLong(msb);
        buf.putLong(lsb);

        return bytes;
    }

    /**
     * Returns {@code true} if the given string is null or is the empty string.
     * <p>
     * <p>Consider normalizing your string references with {@link #isNullOrEmpty}.
     * If you do, you can use {@link String#isEmpty()} instead of this
     * method, and you won't need special null-safe forms of methods like {@link
     * String#toUpperCase} either. Or, if you'd like to normalize "in the other
     * direction," converting empty strings to {@code null}, you can use {@link
     * #isNullOrEmpty(String)}.
     *
     * @param str a string reference to check
     * @return {@code true} if the string is null or is the empty string
     */
    public static boolean isNullOrEmpty(String str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }
}
