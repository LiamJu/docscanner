package nutstore.android.sdk.internal;

import android.support.annotation.Nullable;

/**
 * @author Zhu Liang
 */

public class Optional<T> {

    private final T value;

    private static final Optional<?> EMPTY = new Optional<>();


    private Optional() {
        this.value = null;
    }

    private Optional(T value) {
        this.value = value;
    }

    public static <T> Optional<T> empty() {
        @SuppressWarnings("unchecked")
        Optional<T> t = (Optional<T>) EMPTY;
        return t;
    }

    public static <T> Optional<T> of(@Nullable T value) {
        return new Optional<>(value);
    }

    @Nullable
    public T get() {
        return value;
    }
}
