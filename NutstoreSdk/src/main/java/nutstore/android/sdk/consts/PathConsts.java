package nutstore.android.sdk.consts;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/10 下午5:33
 */

public class PathConsts {
    public static final String ROOT_NUTSTORE_PATH = "/";
    public static final String PATH_SEPARATOR = "/";
}
