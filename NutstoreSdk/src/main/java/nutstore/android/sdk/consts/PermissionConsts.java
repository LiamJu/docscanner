package nutstore.android.sdk.consts;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/6 下午6:08
 */

public class PermissionConsts {
    private static final int PREVIEW_CAP = 1;
    private static final int READ_CAP = 1 << 1;
    private static final int WRITE_CAP = 1 << 2;
    private static final int MANAGE_CAP = 1 << 3;

    public static final Integer NONE = 0;
    public static final Integer READ_ONLY = PREVIEW_CAP | READ_CAP;
    public static final Integer WRITE_ONLY = WRITE_CAP;
    public static final Integer READ_WRITE = PREVIEW_CAP | READ_CAP | WRITE_CAP;
    public static final Integer MANAGE = PREVIEW_CAP | READ_CAP | WRITE_CAP | MANAGE_CAP;
    public static final Integer PREVIEW_ONLY = PREVIEW_CAP;
    public static final Integer PREVIEW_WRITE = PREVIEW_CAP | WRITE_CAP;

}
