package nutstore.android.sdk.consts;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/7 上午9:53
 */

public class MetaDataTypeConsts {
    public static final String DIRECTORY = "directory";
    public static final String FILE = "file";
}
