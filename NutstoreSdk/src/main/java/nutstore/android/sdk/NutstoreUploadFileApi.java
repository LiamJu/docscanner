package nutstore.android.sdk;

import io.reactivex.Flowable;
import nutstore.android.sdk.module.Metadata;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author Zhu Liang
 */

public interface NutstoreUploadFileApi {

    /**
     * 上传文件到服务器
     *
     * @param sndId    同步文件夹的id值（注意传入的参数需要经过{@link Long#toHexString(long)}编码）
     * @param sndMagic 同步文件夹的magic值（注意传入的参数需要经过{@link Long#toHexString(long)}编码）
     * @param path     上传到服务器端的目标路径
     * @param file     源文件
     * @return 返回文件元数据对象
     */
    @POST("/mcmd/uploadFile")
    Flowable<Metadata> uploadFile(@Query("sndId") String sndId, @Query("sndMagic") String sndMagic,
                                  @Query("path") String path, @Body RequestBody file);

}
