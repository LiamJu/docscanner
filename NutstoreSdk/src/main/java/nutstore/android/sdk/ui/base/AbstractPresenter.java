package nutstore.android.sdk.ui.base;

import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;
import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider;

import static nutstore.android.sdk.util.Preconditions.checkNotNull;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/12 上午9:13
 */

public abstract class AbstractPresenter<V extends BaseView> implements BasePresenter {
    protected final V mView;
    protected final BaseSchedulerProvider mSchedulerProvider;
    protected final CompositeDisposable mDisposable = new CompositeDisposable();

    @SuppressWarnings("unchecked")
    public AbstractPresenter(@NonNull V view,
                             @NonNull BaseSchedulerProvider schedulerProvider) {
        mView = checkNotNull(view);
        mSchedulerProvider = Preconditions.checkNotNull(schedulerProvider);

        mView.setPresenter(this);
    }

    @Override
    public final void unsubscribe() {
        mDisposable.clear();
    }
}
