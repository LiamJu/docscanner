package nutstore.android.sdk.ui.base;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/7 下午9:53
 */

public interface NutStorePresenter extends BasePresenter {
    void handleError(Throwable e);
}
