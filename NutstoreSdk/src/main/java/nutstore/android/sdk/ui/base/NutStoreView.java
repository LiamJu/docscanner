package nutstore.android.sdk.ui.base;

import nutstore.android.sdk.exception.ServerException;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/7 下午9:52
 */

public interface NutStoreView<P extends NutStorePresenter> extends BaseView<P> {

    /**
     * 显示或隐藏进度条
     *
     * @param active 为true则显示进度条，否则隐藏进度条
     */
    void setLoadingIndicator(boolean active);

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#SANDBOX_NOT_FOUND
     */
    void showSandboxNotFoundError(String detailMsg);

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#SANDBOX_ACCESS_DENIED
     */
    void showSandboxDeniedError(String detailMsg);

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#UNAUTHORIZED
     */
    void showUnauthorizedError(String detailMsg);

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#DISABLED_FOR_FREE_USER
     */
    void showDisabledForFreeUserError(String detailMsg);

    /**
     * 显示网络异常
     */
    void showNetworkError();

    /**
     * 显示未知错误信息
     *
     * @param message 位置错误信息
     */
    void showUnknownError(String message);

    /**
     * 短信发送次数过多
     */
    void showRequestSmsTooManyError();
}
