package nutstore.android.sdk.ui.base;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements BaseView<P> {
    @NonNull
    protected P mPresenter;

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public final void setPresenter(@NonNull P presenter) {
        if (presenter == null) {
            throw new NullPointerException("presenter must not be null");
        }
        mPresenter = presenter;
    }
}
