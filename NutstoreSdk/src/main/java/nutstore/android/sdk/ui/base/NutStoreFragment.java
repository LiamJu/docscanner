package nutstore.android.sdk.ui.base;

import android.util.Log;
import android.widget.Toast;

import nutstore.android.sdk.R;
import nutstore.android.sdk.exception.ServerException;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/10 下午9:52
 */

public abstract class NutStoreFragment<P extends NutStorePresenter> extends BaseFragment<P> implements NutStoreView<P> {
    private static final String TAG = NutStoreFragment.class.getSimpleName();

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#SANDBOX_NOT_FOUND
     */
    @Override
    public void showSandboxNotFoundError(String detailMsg) {
        showUnknownError(detailMsg);
    }

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#SANDBOX_ACCESS_DENIED
     */
    @Override
    public void showSandboxDeniedError(String detailMsg) {
        showUnknownError(detailMsg);
    }

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#UNAUTHORIZED
     */
    @Override
    public void showUnauthorizedError(String detailMsg) {
        showUnknownError(detailMsg);
    }

    /**
     * @param detailMsg 错误码的提示信息
     * @see ServerException#DISABLED_FOR_FREE_USER
     */
    @Override
    public void showDisabledForFreeUserError(String detailMsg) {
        showUnknownError(detailMsg);
    }

    @Override
    public void showNetworkError() {
        showUnknownError(getString(R.string.common_connection_error));
    }

    @Override
    public void showRequestSmsTooManyError() {
        showUnknownError(getString(R.string.common_resend_blocked_msg));
    }

    /**
     * 显示未知错误信息
     *
     * @param message 位置错误信息
     */
    @Override
    public void showUnknownError(String message) {
        if (getActivity() != null) {
            Log.e(TAG, "showUnknownError: " + message);
            showToast(message);
        }
    }

    private void showToast(String text) {
        if (getContext() != null) {
            Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
        }
    }
}
