package nutstore.android.sdk.ui.base;

public interface BasePresenter {

    void subscribe();

    void unsubscribe();

}
