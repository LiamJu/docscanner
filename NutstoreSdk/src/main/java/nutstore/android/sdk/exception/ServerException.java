package nutstore.android.sdk.exception;

import nutstore.android.sdk.module.ErrorDetail;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/5/1 下午11:48
 */

public class ServerException extends RuntimeException {

    public static final String ILLEGAL_ARGUMENT = "IllegalArgument";
    public static final String CONCURRENT_UPDATE = "ConcurrentUpdate";
    public static final String DUPLICATE_NAME = "DuplicateName";
    public static final String TOO_MANY_EVENTS = "TooManyEvents";
    public static final String NO_SUCH_USER = "NoSuchUser";
    public static final String AUTHENTICATION_FAILED = "AuthenticationFailed";
    public static final String UNAUTHORIZED = "UnAuthorized";
    public static final String SANDBOX_ACCESS_DENIED = "SandboxAccessDenied";
    public static final String TWOFACTOR_AUTH_FAILED = "TwoFactorsAuthenticationFailed";
    public static final String STORAGESPACE_EXHAUSTED = "StorageSpaceExhausted";
    public static final String TRAFFIC_RATE_EXHAUSTED = "TrafficRateExhausted";
    public static final String BLOCKED_TEMPORARILY = "BlockedTemporarily";
    public static final String REFERRAL_EXHAUSTED = "ReferralExhausted";
    public static final String INTERNAL_ERROR = "InternalError";
    public static final String TOO_BIG_ENTITY = "TooBigEntity";
    public static final String TOO_MANY_BLOCKS = "TooManyBlocks";
    public static final String TOO_MANY_OBJECTS = "TooManyObjects";
    public static final String BLOCK_NOT_FOUND = "BlockNotFound";
    public static final String BLOCKLIST_NOT_FOUND = "BlockListNotFound";
    public static final String OBJECT_NOT_FOUND = "ObjectNotFound";
    public static final String SANDBOX_NOT_FOUND = "SandboxNotFound";
    public static final String STORAGEDOMAIN_NOT_FOUND = "StorageDomainNotFound";
    public static final String SERVICE_UNAVAILABLE = "ServiceUnAvailable";
    public static final String USER_EXISTED = "UserExisted";
    public static final String UNSUPPORTED_ENCODING = "UnsupportedEncoding";
    public static final String RANGE_NOT_SATISFIED = "RangeNotSatisfied";
    public static final String DELTAOBJECT_NOT_ACCEPTED = "DeltaObjectNotAccepted";
    public static final String URI_EXPIRED = "URIExpired";
    public static final String TOO_MANY_COLLABORATORS = "TooManyCollaborators";
    public static final String ACCOUNT_EXPIRED = "AccountExpired";
    public static final String NEED_TWO_FACTORS_AUTHENTICATION = "NeedTwoFactorsAuthentication";
    public static final String NEED_SMS_AUTHENTICATION = "NeedSmsAuthentication";
    public static final String NEED_WECHAT_AUTHENTICATION = "NeedWechatAuthentication";
    public static final String NEED_SETUP_TWO_FACTORS_AUTH = "NeedSetupTwoFactorsAuth";
    public static final String INVALID_ACCOUNT_STATE = "InvalidAccountState";
    public static final String DUPLICATE_ORDER = "DuplicateOrder";
    public static final String TOO_MANY_TEAM_MEMBERS = "TooManyTeamMembers";
    public static final String NO_SUCH_TEAM = "NoSuchTeam";
    public static final String NO_SUCH_GROUP = "NoSuchGroup";
    public static final String TOO_MANY_GROUPS = "TooManyGroups";
    public static final String NESTED_GROUP_LOOP = "NestedGroupLoop";
    public static final String TOO_MANY_NESTED_LEVELS = "TooManyNestedLevel";
    public static final String DISABLED_FOR_PRIVATE_DEPLOYMENT = "DisabledForPrivateDeployment";
    public static final String INTERNAL_SERVICE_DISABLED = "InternalServiceDisabled";
    public static final String DISABLED_FOR_FREE_USER = "DisabledForFreeUser";
    public static final String DISABLED_BY_TEAM_ADMIN = "DisabledByTeamAdmin";
    public static final String OPERATION_NOT_ALLOWED = "OperationNotAllowed";
    public static final String GROUP_SHARING_DENIED = "GroupSharingDenied";
    public static final String TOO_MANY_LOGIN_FAILURES = "TooManyLoginFailures";
    public static final String NOT_IN_ACCESSIBLE_NETWORK = "NotInAccessibleNetwork";

    private ErrorDetail mErrorDetail;

    public ServerException(ErrorDetail errorDetail) {
        mErrorDetail = errorDetail;
    }

    @Override
    public String getMessage() {
        return "errorCode='" + mErrorDetail.getErrorCode() + '\'' +
                ", detailMsg='" + mErrorDetail.getDetailMsg() + '\'' +
                ", payload='" + mErrorDetail.getPayload() + '\'';
    }

    public String getErrorCode() {
        return mErrorDetail.getErrorCode();
    }

    public String getDetailMsg() {
        return mErrorDetail.getDetailMsg();
    }

    public String getPayload() {
        return mErrorDetail.getPayload();
    }

}
