package nutstore.android.sdk.exception;

/**
 * @author Zhu Liang
 */

public class FatalException extends RuntimeException {
    public FatalException(Throwable cause) {
        super(cause);
    }
}
