package nutstore.android.sdk;

import android.support.annotation.NonNull;

import java.io.IOException;

import nutstore.android.sdk.exception.ServerException;
import nutstore.android.sdk.module.ErrorDetail;
import nutstore.android.sdk.util.JsonWrapper;
import nutstore.android.sdk.util.Preconditions;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Zhu Liang
 */

public class ApiManager {

    private static NutstoreAPI sNutstoreAPI;
    private static NutstoreUploadFileApi sNutstoreUploadFileApi;

    public static NutstoreAPI getNutstoreAPI(String baseUrl, String authToken,
                                             @NonNull String acceptLanguage,
                                             @NonNull String userAgent) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .addInterceptor(new HeadersInterceptor(acceptLanguage, userAgent))
                .addInterceptor(new ApiErrorInterceptor());

        if (authToken != null && authToken.length() > 0) {
            httpClient.addInterceptor(new AuthenticationInterceptor(authToken));
        }

        if (sNutstoreAPI == null) {
            synchronized (ApiManager.class) {
                if (sNutstoreAPI == null) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(baseUrl)
                            .client(httpClient.build())
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .baseUrl(baseUrl)
                            .build();
                    sNutstoreAPI = retrofit.create(NutstoreAPI.class);
                }
            }
        }
        return sNutstoreAPI;
    }

    public static NutstoreUploadFileApi getNutstoreUploadFileApi(@NonNull String baseUrl,
                                                                 @NonNull String authToken,
                                                                 @NonNull String acceptLanguage,
                                                                 @NonNull String userAgent) {
        Preconditions.checkNotNull(baseUrl);
        Preconditions.checkNotNull(authToken);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .addInterceptor(new UploadFileHeadersInterceptor(acceptLanguage, userAgent))
                .addInterceptor(new ApiErrorInterceptor())
                .addInterceptor(new AuthenticationInterceptor(authToken));

        if (sNutstoreUploadFileApi == null) {
            synchronized (ApiManager.class) {
                if (sNutstoreUploadFileApi == null) {
                    sNutstoreUploadFileApi = new Retrofit.Builder()
                            .baseUrl(baseUrl)
                            .client(httpClient.build())
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .build()
                            .create(NutstoreUploadFileApi.class);
                }
            }
        }
        return sNutstoreUploadFileApi;
    }

    public static void forceReleaseNutstoreAPI() {
        sNutstoreAPI = null;
    }

    public static void forceReleaseNutstoreUploadFileApi() {
        sNutstoreUploadFileApi = null;
    }

    private static class ApiErrorInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Response response = chain.proceed(request);
            if (response.isSuccessful()) {
                return response;
            } else {
                ErrorDetail errorDetail = JsonWrapper.fromJson(response.body().string(), ErrorDetail.class);
                throw new ServerException(errorDetail);
            }
        }
    }

    private static class HeadersInterceptor implements Interceptor {

        @NonNull
        private String acceptLanguage;
        @NonNull
        private String userAgent;

        private HeadersInterceptor(@NonNull String acceptLanguage,
                                   @NonNull String userAgent) {
            this.acceptLanguage = Preconditions.checkNotNull(acceptLanguage);
            this.userAgent = Preconditions.checkNotNull(userAgent);
        }

        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Accept-Language", acceptLanguage)
                    .header("User-Agent", userAgent)
                    .addHeader("Content-Type", "application/json;charset=utf-8")
                    .addHeader("Content-Type", "application/octet-stream")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        }
    }

    private static class AuthenticationInterceptor implements Interceptor {

        private String authToken;

        private AuthenticationInterceptor(String authToken) {
            this.authToken = authToken;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Authorization", authToken)
                    .build();
            return chain.proceed(request);
        }
    }

    private static class UploadFileHeadersInterceptor implements Interceptor {

        @NonNull
        private String acceptLanguage;
        @NonNull
        private String userAgent;

        private UploadFileHeadersInterceptor(@NonNull String acceptLanguage,
                                             @NonNull String userAgent) {
            this.acceptLanguage = Preconditions.checkNotNull(acceptLanguage);
            this.userAgent = Preconditions.checkNotNull(userAgent);
        }

        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Accept-Language", acceptLanguage)
                    .header("User-Agent", userAgent)
                    .addHeader("Content-Type", "application/json;charset=utf-8")
                    .addHeader("Content-Type", "application/octet-stream")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        }
    }
}
