package nutstore.android.sdk;

import android.support.annotation.Nullable;

import io.reactivex.Flowable;
import nutstore.android.sdk.module.CaptchaResponse;
import nutstore.android.sdk.module.CreateSandboxInfo;
import nutstore.android.sdk.module.LoginBody;
import nutstore.android.sdk.module.Metadata;
import nutstore.android.sdk.module.MetadataList;
import nutstore.android.sdk.module.PathInternal;
import nutstore.android.sdk.module.Sandbox;
import nutstore.android.sdk.module.UserInfo;
import nutstore.android.sdk.module.VerifyEmailBody;
import nutstore.android.sdk.module.VerifyEmailResponse;
import nutstore.android.sdk.module.VerifyResult;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author Zhu Liang
 */

public interface NutstoreAPI {

    /**
     * 账号、密码登录
     *
     * @param loginBody 账号、密码
     */
    @POST("cmd/loginV1")
    Flowable<VerifyResult> loginV1(@Body LoginBody loginBody);

    /**
     * 微信授权登录，将微信客户端返回的 code 发送的我们的服务器，验证用户是否是坚果云用户。
     * 如果用户已经是坚果云用户，则返回：
     * {
     * "token":"NUTSTORE_TOKEN",
     * "userName":"USER_NAME"
     * }
     * 否则返回：
     * {
     * "unionid":"UNION_ID"
     * }
     *
     * @param code 微信SDK返回的code
     */
    @GET(value = "3rdpartysso/verify?type=mobile_wx")
    Flowable<VerifyResult> verifyWxSso(@Query("code") String code);

    /**
     * 注册/绑定用户
     * {
     * "token":"NUTSTORE_TOKEN",
     * "userName":"USER_NAME"
     * }
     *
     * @param username 用户名(邮箱)
     * @param password 只有在绑定既有用户的时候需要
     * @param authid   微信unionid
     */
    @FormUrlEncoded
    @POST(value = "3rdpartysso/binding?type=mobile_wx")
    Flowable<VerifyResult> bindWxSso(@Field("username") String username, @Field("password") String password, @Field("authid") String authid);

    /**
     * 获取自定义验证码
     */
    @GET(value = "cmd/getFallbackCaptchaV1")
    Flowable<CaptchaResponse> getFallbackCaptchaV1();

    /**
     * 验证邮箱
     */
    @POST(value = "cmd/verifyEmailV1")
    Flowable<VerifyEmailResponse> verifyEmailV1(@Body VerifyEmailBody body);

    @GET("cmd/getUserInfoV2?hasPreviewOnly=1")
    Flowable<UserInfo> getUserInfoV2(@Query("start") int start);

    @GET("cmd/getUserInfoV2?hasPreviewOnly=1")
    Flowable<UserInfo> getUserInfoV2();

    /**
     * 创建同步文件夹
     */
    @POST("cmd/createSandboxV1")
    Flowable<Sandbox> createSandboxV1(@Body CreateSandboxInfo createSandboxInfo);

    /**
     * 获取同步文件夹
     *
     * @param sndId    同步文件夹的id值（注意传入的参数需要经过{@link Long#toHexString(long)}编码）
     * @param sndMagic 同步文件夹的magic值（注意传入的参数需要经过{@link Long#toHexString(long)}编码）
     */
    @GET("cmd/getSandboxDetailV1")
    Flowable<Sandbox> getSandboxDetailV1(@Query("sndId") String sndId, @Query("sndMagic") String sndMagic);

    /**
     * 获取 path 对应文件夹下子文件列表
     * 注意：
     * 如果子文件过多，服务器端可能无法一次将所有的元数据返回到客户端，相当于“分页操作”，具体的操作步骤如下：
     * <p>
     * 1. 传入必选参数，以及 list = true，返回的 Json 数据中包含 etag,truncated 这两个字段
     * 2. 如果 truncated 为 true，说明子文件过多，没有全部返回，继续执行步骤 3，否则，已全部返回，结束
     * 3. 传入必选参数，以及 list = true , 步骤 1 返回的 etag , marker = 返回的最后一个子文件的文件名，调用接口，继续执行步骤2。
     *
     * @param sndId    与 sndMagic 共同确定一个 Sandbox
     * @param sndMagic 与 sndId 共同确定一个 Sandbox
     * @param path     文件夹绝对路径
     * @param etag
     * @param marker
     * @return
     */
    @GET("mcmd/getMetaData?list=true")
    Call<MetadataList> getMetadataList(@Query("sndId") String sndId,
                                       @Query("sndMagic") String sndMagic,
                                       @Query("path") String path,
                                       @Nullable @Query("etag") String etag,
                                       @Nullable @Query("marker") String marker);

    @POST("cmd/sendTfSms")
    Flowable<Void> sendTfSms(@Body LoginBody body);

    /**
     * 新建目录
     */
    @POST("/mcmd/mkdir")
    Call<Metadata> mkdir(@Query("sndId") String sndId,
                         @Query("sndMagic") String sndMagic,
                         @Body PathInternal pathInternal);
}
