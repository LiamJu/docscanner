package nutstore.android.sdk.api;

public interface ProgressRequestListener {
    void onRequestProgress(long paramLong1, long paramLong2);
}
