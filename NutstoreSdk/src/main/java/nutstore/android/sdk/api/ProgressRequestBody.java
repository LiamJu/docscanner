package nutstore.android.sdk.api;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

public class ProgressRequestBody extends RequestBody {
    private static final int SEGMENT_SIZE = 2048;
    private File mFile;
    private ProgressRequestListener mListener;

    public ProgressRequestBody(File file, ProgressRequestListener listener) {
        this.mFile = file;
        this.mListener = listener;
    }

    public MediaType contentType() {
        return null;
    }

    public long contentLength()
            throws IOException {
        return this.mFile.length();
    }

    public void writeTo(BufferedSink sink)
            throws IOException {
        if (this.mListener == null) {
            this.mListener = this.DEFAULT_LISTENER;
        }
        Source source = null;
        try {
            source = Okio.source(this.mFile);
            long byteCount = 0L;

            long contentLength = contentLength();
            long read;
            while ((read = source.read(sink.buffer(), 2048L)) != -1L) {
                byteCount += read;
                sink.flush();
                this.mListener.onRequestProgress(byteCount, contentLength);
            }
        } finally {
            Util.closeQuietly(source);
        }
    }

    private ProgressRequestListener DEFAULT_LISTENER = new ProgressRequestListener() {
        public void onRequestProgress(long bytesWritten, long contentLength) {
        }
    };
}
