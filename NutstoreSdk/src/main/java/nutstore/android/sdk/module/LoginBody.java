package nutstore.android.sdk.module;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import nutstore.android.sdk.internal.HttpConfig;
import nutstore.android.sdk.util.Preconditions;

/**
 * @author Zhu Liang
 */

public class LoginBody {

    private String userName;
    private String password;
    private String tfPasscode;

    public LoginBody(@NonNull String userName, @NonNull String password, @Nullable String tfPasscode) {
        Preconditions.checkNotNull(userName);
        Preconditions.checkNotNull(password);

        this.userName = HttpConfig.stringToUTF8Base64(userName);
        this.password = HttpConfig.stringToUTF8Base64(password);

        this.tfPasscode = tfPasscode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTfPasscode() {
        return tfPasscode;
    }

    public void setTfPasscode(String tfPasscode) {
        this.tfPasscode = tfPasscode;
    }
}
