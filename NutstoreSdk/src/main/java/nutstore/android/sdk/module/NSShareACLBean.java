package nutstore.android.sdk.module;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import nutstore.android.sdk.consts.PermissionConsts;

import static nutstore.android.sdk.util.PermissionUtils.checkPermission;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/6 下午6:06
 */

class NSShareACLBean {
    public Integer anonymous = PermissionConsts.NONE; //default settings
    public Integer signed = PermissionConsts.NONE; //default settings
    public Map<String, Integer> users;
    public Map<String, String> userNicks;
    public List<GroupItemBean> groups;

    public NSShareACLBean() {
    }

    public NSShareACLBean(@NonNull Integer anonymous, @NonNull Integer signed,
                          @Nullable Map<String, Integer> users,
                          @Nullable Map<String, String> userNicks,
                          @Nullable List<GroupItemBean> groups) {
        this.anonymous = checkPermission(anonymous);
        this.signed = checkPermission(signed);
        this.users = users;
        this.userNicks = userNicks;
        this.groups = groups;
    }
}
