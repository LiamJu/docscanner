package nutstore.android.sdk.module;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Sandbox implements Parcelable {
    /**
     * name :
     * sandboxId : 33
     * magic : 9092323561394054155
     * owner : liamju@163.com
     * ownerNick :
     * permission : 4
     * caps : 15
     * pathPerms : [{"path":"/","permission":4,"caps":15}]
     * isDefault : true
     * isOwner : true
     * exclusiveUser : true
     * desc :
     * lanSyncCipher : raUj3jpsWOF0nwQCLRmOPwuedJmotCa8RvE5T0FEF5k=
     * channelId : d-79Db2BqIpaYwHlOo6wOw9Q
     * usedSpace : 3387812
     * isPhotoBucket : false
     * canSync : false
     * doNotSync : false
     */

    private String name;
    private long sandboxId;
    private long magic;
    private String owner;
    private String ownerNick;
    private int permission;
    private boolean isDefault;
    private boolean isOwner;
    private boolean exclusiveUser;
    private String desc;
    private String lanSyncCipher;
    private String channelId;
    private long usedSpace;
    private boolean isPhotoBucket;
    private boolean canSync;
    private boolean doNotSync;
    private List<PathPermsBean> pathPerms;

    public static final Sandbox DEFAULT = new Sandbox();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSandboxId() {
        return sandboxId;
    }

    public void setSandboxId(long sandboxId) {
        this.sandboxId = sandboxId;
    }

    public long getMagic() {
        return magic;
    }

    public void setMagic(long magic) {
        this.magic = magic;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerNick() {
        return ownerNick;
    }

    public void setOwnerNick(String ownerNick) {
        this.ownerNick = ownerNick;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public boolean isIsOwner() {
        return isOwner;
    }

    public void setIsOwner(boolean isOwner) {
        this.isOwner = isOwner;
    }

    public boolean isExclusiveUser() {
        return exclusiveUser;
    }

    public void setExclusiveUser(boolean exclusiveUser) {
        this.exclusiveUser = exclusiveUser;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLanSyncCipher() {
        return lanSyncCipher;
    }

    public void setLanSyncCipher(String lanSyncCipher) {
        this.lanSyncCipher = lanSyncCipher;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public long getUsedSpace() {
        return usedSpace;
    }

    public void setUsedSpace(long usedSpace) {
        this.usedSpace = usedSpace;
    }

    public boolean isIsPhotoBucket() {
        return isPhotoBucket;
    }

    public void setIsPhotoBucket(boolean isPhotoBucket) {
        this.isPhotoBucket = isPhotoBucket;
    }

    public boolean isCanSync() {
        return canSync;
    }

    public void setCanSync(boolean canSync) {
        this.canSync = canSync;
    }

    public boolean isDoNotSync() {
        return doNotSync;
    }

    public void setDoNotSync(boolean doNotSync) {
        this.doNotSync = doNotSync;
    }

    public List<PathPermsBean> getPathPerms() {
        return pathPerms;
    }

    public void setPathPerms(List<PathPermsBean> pathPerms) {
        this.pathPerms = pathPerms;
    }

    public static class PathPermsBean implements Parcelable {
        /**
         * path : /
         * permission : 4
         * caps : 15
         */

        private String path;
        private int permission;
        private int caps;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getPermission() {
            return permission;
        }

        public void setPermission(int permission) {
            this.permission = permission;
        }

        public int getCaps() {
            return caps;
        }

        public void setCaps(int caps) {
            this.caps = caps;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.path);
            dest.writeInt(this.permission);
            dest.writeInt(this.caps);
        }

        public PathPermsBean() {
        }

        protected PathPermsBean(Parcel in) {
            this.path = in.readString();
            this.permission = in.readInt();
            this.caps = in.readInt();
        }

        public static final Parcelable.Creator<PathPermsBean> CREATOR = new Parcelable.Creator<PathPermsBean>() {
            @Override
            public PathPermsBean createFromParcel(Parcel source) {
                return new PathPermsBean(source);
            }

            @Override
            public PathPermsBean[] newArray(int size) {
                return new PathPermsBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeLong(this.sandboxId);
        dest.writeLong(this.magic);
        dest.writeString(this.owner);
        dest.writeString(this.ownerNick);
        dest.writeInt(this.permission);
        dest.writeByte(this.isDefault ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isOwner ? (byte) 1 : (byte) 0);
        dest.writeByte(this.exclusiveUser ? (byte) 1 : (byte) 0);
        dest.writeString(this.desc);
        dest.writeString(this.lanSyncCipher);
        dest.writeString(this.channelId);
        dest.writeLong(this.usedSpace);
        dest.writeByte(this.isPhotoBucket ? (byte) 1 : (byte) 0);
        dest.writeByte(this.canSync ? (byte) 1 : (byte) 0);
        dest.writeByte(this.doNotSync ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.pathPerms);
    }

    public Sandbox() {
    }

    protected Sandbox(Parcel in) {
        this.name = in.readString();
        this.sandboxId = in.readLong();
        this.magic = in.readLong();
        this.owner = in.readString();
        this.ownerNick = in.readString();
        this.permission = in.readInt();
        this.isDefault = in.readByte() != 0;
        this.isOwner = in.readByte() != 0;
        this.exclusiveUser = in.readByte() != 0;
        this.desc = in.readString();
        this.lanSyncCipher = in.readString();
        this.channelId = in.readString();
        this.usedSpace = in.readLong();
        this.isPhotoBucket = in.readByte() != 0;
        this.canSync = in.readByte() != 0;
        this.doNotSync = in.readByte() != 0;
        this.pathPerms = in.createTypedArrayList(PathPermsBean.CREATOR);
    }

    public static final Parcelable.Creator<Sandbox> CREATOR = new Parcelable.Creator<Sandbox>() {
        @Override
        public Sandbox createFromParcel(Parcel source) {
            return new Sandbox(source);
        }

        @Override
        public Sandbox[] newArray(int size) {
            return new Sandbox[size];
        }
    };
}