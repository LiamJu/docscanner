package nutstore.android.sdk.module;

/**
 * @author Zhu Liang
 */

public class VerifyResult {
    /**
     * token : NUTSTORE_TOKEN
     * userName : USER_NAME
     * unionid : UNION_ID
     */

    private String token;
    private String userName;
    private String unionid;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
}
