package nutstore.android.sdk.module;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static nutstore.android.sdk.util.Preconditions.checkNotNull;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/6 下午6:04
 */

public class CreateSandboxInfo {

    private String name;
    private String desc;
    private NSShareACLBean acl;
    private Boolean isPhotoBucket;
    private Boolean doNotSync;

    /**
     * 创建一个非云相册且没有描述信息的个人同步文件夹
     */
    public CreateSandboxInfo(String name, Boolean doNotSync) {
        this(name, false/*isPhotoBucket*/, doNotSync);
    }

    /**
     * 创建一个没有描述信息的个人同步文件夹
     */
    public CreateSandboxInfo(String name, Boolean isPhotoBucket, Boolean doNotSync) {
        this(name, null/*desc*/, new NSShareACLBean(), isPhotoBucket, doNotSync);
    }

    /**
     * 创建一个个人同步文件夹或者多人协同文件夹
     */
    public CreateSandboxInfo(@NonNull String name, @Nullable String desc,
                             @NonNull NSShareACLBean acl, @NonNull Boolean isPhotoBucket,
                             @NonNull Boolean doNotSync) {
        this.name = checkNotNull(name);
        this.desc = desc;
        this.acl = checkNotNull(acl);
        this.isPhotoBucket = checkNotNull(isPhotoBucket);
        this.doNotSync = checkNotNull(doNotSync);
    }
}
