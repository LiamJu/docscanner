package nutstore.android.sdk.module;

/**
 * @author Zhu Liang
 */

public final class ErrorDetail {

    /**
     * errorCode : UserExisted
     * detailMsg : User liamju@163.com already exists
     * payload :
     */

    private String errorCode;
    private String detailMsg;
    private String payload;

    public ErrorDetail(String errorCode, String detailMsg, String payload) {
        this.errorCode = errorCode;
        this.detailMsg = detailMsg;
        this.payload = payload;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDetailMsg() {
        return detailMsg;
    }

    public void setDetailMsg(String detailMsg) {
        this.detailMsg = detailMsg;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "ErrorDetail{" +
                "errorCode='" + errorCode + '\'' +
                ", detailMsg='" + detailMsg + '\'' +
                ", payload='" + payload + '\'' +
                '}';
    }
}
