package nutstore.android.sdk.module;

import java.util.List;

/**
 * @author Zhu Liang
 */

public class UserInfo {
    /**
     * sandboxes : [{"name":"","sandboxId":33,"magic":9092323561394054155,"owner":"liamju@163.com","ownerNick":"","permission":4,"caps":15,"pathPerms":[{"path":"/","permission":4,"caps":15}],"isDefault":true,"isOwner":true,"exclusiveUser":true,"desc":"","lanSyncCipher":"raUj3jpsWOF0nwQCLRmOPwuedJmotCa8RvE5T0FEF5k=","channelId":"d-79Db2BqIpaYwHlOo6wOw9Q","usedSpace":3387812,"isPhotoBucket":false,"canSync":false,"doNotSync":false}]
     * freeUpRate : 1073741824
     * phoneVerified : false
     * userChannel : u-zRHQOkzwImGhSh-6a1P0_w
     * accountExpireLeftTime : 0
     * totalStorageSize : 0
     * freeDownRate : 3221225472
     * cometSubServer : wxapi.jianguoyun.com
     * usedStorageSize : 3387812
     * nickName :
     * isInTeam : false
     * usedUpRate : 0
     * language : zh
     * rateResetLeftMills : 2578009441
     * usedDownRate : 0
     * tfAuthEnabled : false
     * isPaidUser : false
     */

    private long freeUpRate;
    private boolean phoneVerified;
    private String userChannel;
    private long accountExpireLeftTime;
    private long totalStorageSize;
    private long freeDownRate;
    private String cometSubServer;
    private long usedStorageSize;
    private String nickName;
    private boolean isInTeam;
    private long usedUpRate;
    private String language;
    private long rateResetLeftMills;
    private long usedDownRate;
    private boolean tfAuthEnabled;
    private boolean isPaidUser;
    private List<Sandbox> sandboxes;

    public long getFreeUpRate() {
        return freeUpRate;
    }

    public void setFreeUpRate(long freeUpRate) {
        this.freeUpRate = freeUpRate;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public String getUserChannel() {
        return userChannel;
    }

    public void setUserChannel(String userChannel) {
        this.userChannel = userChannel;
    }

    public long getAccountExpireLeftTime() {
        return accountExpireLeftTime;
    }

    public void setAccountExpireLeftTime(long accountExpireLeftTime) {
        this.accountExpireLeftTime = accountExpireLeftTime;
    }

    public long getTotalStorageSize() {
        return totalStorageSize;
    }

    public void setTotalStorageSize(long totalStorageSize) {
        this.totalStorageSize = totalStorageSize;
    }

    public long getFreeDownRate() {
        return freeDownRate;
    }

    public void setFreeDownRate(long freeDownRate) {
        this.freeDownRate = freeDownRate;
    }

    public String getCometSubServer() {
        return cometSubServer;
    }

    public void setCometSubServer(String cometSubServer) {
        this.cometSubServer = cometSubServer;
    }

    public long getUsedStorageSize() {
        return usedStorageSize;
    }

    public void setUsedStorageSize(long usedStorageSize) {
        this.usedStorageSize = usedStorageSize;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public boolean isIsInTeam() {
        return isInTeam;
    }

    public void setIsInTeam(boolean isInTeam) {
        this.isInTeam = isInTeam;
    }

    public long getUsedUpRate() {
        return usedUpRate;
    }

    public void setUsedUpRate(long usedUpRate) {
        this.usedUpRate = usedUpRate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public long getRateResetLeftMills() {
        return rateResetLeftMills;
    }

    public void setRateResetLeftMills(long rateResetLeftMills) {
        this.rateResetLeftMills = rateResetLeftMills;
    }

    public long getUsedDownRate() {
        return usedDownRate;
    }

    public void setUsedDownRate(long usedDownRate) {
        this.usedDownRate = usedDownRate;
    }

    public boolean isTfAuthEnabled() {
        return tfAuthEnabled;
    }

    public void setTfAuthEnabled(boolean tfAuthEnabled) {
        this.tfAuthEnabled = tfAuthEnabled;
    }

    public boolean isIsPaidUser() {
        return isPaidUser;
    }

    public void setIsPaidUser(boolean isPaidUser) {
        this.isPaidUser = isPaidUser;
    }

    public List<Sandbox> getSandboxes() {
        return sandboxes;
    }

    public void setSandboxes(List<Sandbox> sandboxes) {
        this.sandboxes = sandboxes;
    }
}
