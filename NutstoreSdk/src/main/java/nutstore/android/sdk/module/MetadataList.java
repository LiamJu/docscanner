package nutstore.android.sdk.module;

import java.util.List;

/**
 * @author Zhu Liang
 */

public class MetadataList {
    /**
     * etag : _DORdSDg6nwqMUgEYenJIQ
     * truncated : false
     * contents : [{"path":"/.foo","size":0,"mtime":1489112660,"tblId":null,"objectType":"file","version":1,"hash":"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=","isImage":false,"auxInfo":null},{"path":"/Debug-图片","size":0,"mtime":1484810780,"tblId":null,"objectType":"directory","version":1,"hash":null,"isImage":false,"auxInfo":null},{"path":"/Folder33","size":0,"mtime":1484193539,"tblId":null,"objectType":"directory","version":1,"hash":null,"isImage":false,"auxInfo":null},{"path":"/abcccc.txt","size":0,"mtime":1484209743,"tblId":null,"objectType":"file","version":1,"hash":"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=","isImage":false,"auxInfo":null},{"path":"/com.coolapk.market-6.10.5-1608192.apk","size":6279254,"mtime":1485155187,"tblId":null,"objectType":"file","version":1,"hash":"p/Ms/l5Zug1ZUYWEHY+YtWyonN6wKoPKpP6HJFSNO2I=","isImage":false,"auxInfo":null},{"path":"/example.txt","size":12,"mtime":1487231716,"tblId":null,"objectType":"file","version":1,"hash":"xKQUAchumeNWzT5yYeDK1d/Ft61/GNxef954qPrbX8E=","isImage":false,"auxInfo":null},{"path":"/gitbook.pdf","size":1349434,"mtime":1487144684,"tblId":null,"objectType":"file","version":1,"hash":"nGrIB96Y2oXuTYKUJ4BANLRGCTqXWZRDcEZ35DhNRuk=","isImage":false,"auxInfo":null},{"path":"/新建文件夹","size":0,"mtime":1484222687,"tblId":null,"objectType":"directory","version":11,"hash":null,"isImage":false,"auxInfo":null}]
     */

    private String etag;
    private boolean truncated;
    private List<Metadata> contents;

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public boolean isTruncated() {
        return truncated;
    }

    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
    }

    public List<Metadata> getContents() {
        return contents;
    }

    public void setContents(List<Metadata> contents) {
        this.contents = contents;
    }
}
