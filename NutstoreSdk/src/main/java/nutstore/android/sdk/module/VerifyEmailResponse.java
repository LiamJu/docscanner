package nutstore.android.sdk.module;

/**
 * @author Zhu Liang
 */

public class VerifyEmailResponse {

    private boolean isNotReg;
    private boolean needPhone;
    private String ticket;

    public boolean isNotReg() {
        return isNotReg;
    }

    public void setNotReg(boolean notReg) {
        isNotReg = notReg;
    }

    public boolean isNeedPhone() {
        return needPhone;
    }

    public void setNeedPhone(boolean needPhone) {
        this.needPhone = needPhone;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
}
