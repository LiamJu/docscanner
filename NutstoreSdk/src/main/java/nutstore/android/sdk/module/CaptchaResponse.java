package nutstore.android.sdk.module;

/**
 * @author Zhu Liang
 */

public class CaptchaResponse {
    /**
     * custom_ticket : CUSTOM_TICKET
     * exp : EXPIRATION
     * sig : SIGNATURE
     * reusable : REUSABLE
     */

    private String custom_ticket;
    private String exp;
    private String sig;
    private String reusable;

    public String getCustom_ticket() {
        return custom_ticket;
    }

    public void setCustom_ticket(String custom_ticket) {
        this.custom_ticket = custom_ticket;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    public String getReusable() {
        return reusable;
    }

    public void setReusable(String reusable) {
        this.reusable = reusable;
    }
}
