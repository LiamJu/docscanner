package nutstore.android.sdk.module;

import android.support.annotation.NonNull;

import nutstore.android.sdk.util.Preconditions;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/7 上午9:39
 */

public class PathInternal {
    private final String path;

    public PathInternal(@NonNull String path) {
        this.path = Preconditions.checkNotNull(path);
    }
}
