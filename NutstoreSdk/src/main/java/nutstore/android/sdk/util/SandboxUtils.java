package nutstore.android.sdk.util;

import android.support.annotation.NonNull;

import nutstore.android.sdk.module.Sandbox;

import static nutstore.android.sdk.util.Preconditions.checkNotNull;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/6 下午8:39
 */
public class SandboxUtils {

    private SandboxUtils() {
        throw new AssertionError();
    }

    public static String encodeSandboxId(@NonNull Sandbox sandbox) {
        checkNotNull(sandbox);
        return encodeSandboxId(sandbox.getSandboxId());
    }

    public static String encodeSandboxId(@NonNull Long sandboxId) {
        checkNotNull(sandboxId);
        return Long.toHexString(sandboxId);
    }

    public static String encodeMagic(@NonNull Sandbox sandbox) {
        checkNotNull(sandbox);
        return encodeMagic(sandbox.getMagic());
    }

    public static String encodeMagic(@NonNull Long magic) {
        checkNotNull(magic);
        return Long.toHexString(magic);
    }
}