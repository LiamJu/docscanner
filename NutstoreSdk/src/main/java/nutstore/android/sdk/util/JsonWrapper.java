package nutstore.android.sdk.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Zhu Liang
 * @see com.google.gson.Gson
 */
public class JsonWrapper {

    public static <T> T fromJson(String json, Class<T> classOfT) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(json));
        return new Gson().fromJson(json, classOfT);
    }

    public static <T> T fromJson(Reader json, Class<T> classOfT) throws Exception {
        return new Gson().fromJson(json, classOfT);
    }

    public static String toJson(Map<String, String> args) {
        Preconditions.checkNotNull(args);
        Iterator<Map.Entry<String, String>> it = args.entrySet().iterator();
        JsonObject jo = new JsonObject();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            jo.addProperty(entry.getKey(), entry.getValue());
        }
        return new Gson().toJson(jo);
    }

    public static String toJson(String key, String value) {
        Map<String, String> map = new HashMap<>();
        map.put(key, value);
        return toJson(map);
    }

    public static String toJson(Object src) {
        return new Gson().toJson(src);
    }
}
