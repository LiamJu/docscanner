package nutstore.android.sdk.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Zhu Liang
 */

public final class StringUtils {

    public static boolean isEmpty(@Nullable CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(@Nullable CharSequence str) {
        return !isEmpty(str);
    }

    /**
     * Format a file size in byte to readable string
     *
     * @param size file size in byte
     * @return readable file size
     */
    public static String readableFileSize(long size) {
        return humanReadableByteCount(size, false);
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        char pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1);
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}
