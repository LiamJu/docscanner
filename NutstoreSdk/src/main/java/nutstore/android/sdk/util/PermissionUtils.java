package nutstore.android.sdk.util;

import nutstore.android.sdk.consts.PermissionConsts;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/6 下午7:13
 */
public class PermissionUtils {

    private PermissionUtils() {
        throw new AssertionError();
    }

    public static Integer checkPermission(Integer permission) {
        Preconditions.checkNotNull(permission);
        Preconditions.checkArgument(PermissionConsts.NONE.equals(permission)
                || PermissionConsts.READ_ONLY.equals(permission)
                || PermissionConsts.WRITE_ONLY.equals(permission)
                || PermissionConsts.READ_WRITE.equals(permission)
                || PermissionConsts.MANAGE.equals(permission)
                || PermissionConsts.PREVIEW_ONLY.equals(permission)
                || PermissionConsts.PREVIEW_WRITE.equals(permission));
        return permission;
    }
}