package nutstore.android.sdk.util;

import android.support.annotation.NonNull;

import nutstore.android.sdk.consts.MetaDataTypeConsts;
import nutstore.android.sdk.consts.PathConsts;
import nutstore.android.sdk.module.Metadata;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/6/10 下午5:30
 */

public class MetadataUtils {

    @NonNull
    public static String getDisplayName(@NonNull Metadata metaData) {
        Preconditions.checkNotNull(metaData);
        return getDisplayName(metaData.getPath());
    }

    private static String getDisplayName(String absolutePath) {
        Preconditions.checkNotNull(absolutePath);
        return absolutePath.substring(absolutePath.lastIndexOf(PathConsts.PATH_SEPARATOR) + 1);
    }

    public static Boolean isFile(Metadata metaData) {
        Preconditions.checkNotNull(metaData);
        return MetaDataTypeConsts.FILE.equals(metaData.getObjectType());
    }

    public static Boolean isDirectory(Metadata metaData) {
        Preconditions.checkNotNull(metaData);
        return MetaDataTypeConsts.DIRECTORY.equals(metaData.getObjectType());
    }

    public static String getPath(@NonNull Metadata parent, @NonNull String name) {
        Preconditions.checkNotNull(parent);
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name is empty");

        String parentPath = parent.getPath();
        int lastIndex = parentPath.lastIndexOf(PathConsts.PATH_SEPARATOR);
        if (lastIndex != parentPath.length() - 1) {
            parentPath = parentPath + PathConsts.PATH_SEPARATOR;
        }
        return parentPath + name;
    }
}
