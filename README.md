# 坚果扫描V1.0.0

[TOC]

## 简介

这款产品是基于 [Scanbot SDK][1] 开发的。目前主要用到了这款 SDK 中的 “扫描文档”、“编辑文档页面”、“创建PDF文档”的功能。

Scanbot SDK 相关资料的链接如下：

1. [官网地址][1]
2. [GitHub地址][2]
3. [Scanbot SDK 示例代码地址][3]
4. [Scanbot SDK 示例文档][4]
5. [Android API Javadoc][5]
6. [Java API Core Javdoc][6]

由于是基于别人的SDK做开发，阅读示例代码、示例文档、Javadoc就显得尤为重要。开发过程中遇到的很多问题，参考这些官方资料就可以解决，但有些问题仅仅查看文档是解决不了问题的，必须读 SDK 的源码。SDK 做了混淆，阅读起来还是有些困难，通过能读懂的那部分 SDK 代码基本已经解决了具体的问题。如果实在不知道该如何解决，可以在 [slack](https://scanbotsdk.slack.com/messages/C06MYAUN9/) 跟 [Scanbot][1] 的研发直接沟通（如何使用 [Glide][https://github.com/bumptech/glide] 来加载用Scanbot编辑过的扫描页面，就是受他们研发的启发）。

坚果扫描的包名：`nutstore.android.docscanner`

Scanbot 发给我们的证书：

```java
private final String LICENSE_KEY =
            "RIdFhTubljbhRO2/uJ6z700DIcGoIU" +
                    "wex0kHXwgl/eSFQtpWXmYbHmwauDxT" +
                    "rDqN1TANnU6aysQ+v64kIgQ4PFb2Se" +
                    "yCi0ejeDiiuYW4JBXuq+FkZ4hvoZkO" +
                    "qLlW2c/BAPyEYecFdUU/WWUJ4n2+2x" +
                    "AD13pJ0qEjXqRiVTkhIfpE+uu/pd37" +
                    "It/UFCFtmWnFfTQ1AVDWHb0SeVni9A" +
                    "ozzxVIBhuqd9StmhPZqdg1vsfe8Gnq" +
                    "DNzn7d9u0Eu9ZLX4swIo7NLZDxo2qo" +
                    "c454GNldMBYOi3VXetKKNV8RLeTCrb" +
                    "Pj5j6UyY2SzJPXgCN14Wq9PylevVRt" +
                    "5nZz8IUUoWvg==\nU2NhbmJvdFNESw" +
                    "pudXRzdG9yZS5hbmRyb2lkLmRvY3Nj" +
                    "YW5uZXIKMTU2MDY0MzE5OQo1OTAKMg" +
                    "==\n";
```

如何使用请参考：[Getting-started](https://github.com/doo/Scanbot-SDK-Examples/wiki/Getting-started)。

## 创建Key Store

![](./images/Keystore.jpg)

Key Store 保存在 `坚果云扫描器APP文档/Android_开发/docscanner-app-key`目录下。

## 产品整体结构

![](./images/产品整体结构.png)

## 遇到的难点以及解决办法

### 运行时权限

运行时权限用的是 [easypermissions](https://github.com/googlesamples/easypermissions)。

### 拍照

拍照功能遇到的难点是如何将照片缓存下来，为接下来的编辑、创建文档做准备。

[Scanbot SDK 示例文档][4] 里详细介绍了如何 [Creating documents](https://github.com/doo/Scanbot-SDK-Examples/wiki/Creating-documents)。开发的过程中也是按照文档写的，但是创建文档后发现，给页面设置的滤镜并没有生效。阅读源码后发现，问题出在文档里描述的`pageFactory.buildPage`方法上面。想要使设置的滤镜生效，必须创建 `filtered`文件夹。

具体情况请参考：

1. `PageFactory.buildPage(java.lang.String pageId, byte[] image, int screenWidth, int screenHeight)`
2. `PageFactory.buildPage(android.graphics.Bitmap image, int screenWidth, int screenHeight)`

然而手动扫描`CaptureActivity` 后裁剪`EditPolygonActivity`的时候又遇到了问题，裁剪的时候有一定几率会出现图片的下半部分是“纯黑色”。上面这两个`buildPage`会生成名为 `original` 和 `preview` 的图片。项目中没有使用`original`的图片，原因是图片过大，加载时间过长。但 `preview` 的图片在手机上展示的时候跟`original` 没有太大的区别。经分析图片的下半部分显示`纯黑色`的原因可能是“保存preview是在子线程中完成的”，代码如下：

```java
public PageFactory.Result buildPage(String var1, byte[] var2, int var3, int var4) throws IOException {
    // ...
    a.execute(new Runnable() {
        public final void run() {
            FileOutputStream var1 = null;
            try {
                var1 = new FileOutputStream(var8);
                var7.compress(CompressFormat.JPEG, 90, var1);
                return;
            } catch (IOException var6) {
                PageFactory.this.d.logException(var6);
            } finally {
                IOUtils.closeQuietly(var1);
            }
        }
    });
    return new PageFactory.Result(var5, var7);
}
```

找到原因之后，目前的解决方案是仿照上面提到的那两个`PageFactory.buildPage()`方法，创建了`PageFactoryHelper`类，将保存`preview`图片的过程改为**同步**。**这仅仅是目前的处理方法，如果有更好的方法会修改**

### 日志留存

日志留存：将所有的 `Log` 信息和 Crash 信息缓存下来，方便分析用户在使用中遇到的问题。

用户可以 "设置>报告错误" 然后发送邮件，日志会被添加到附件中。

依赖：

```groovy
implementation 'com.jakewharton.timber:timber:4.6.0'
implementation 'log4j:log4j:1.2.17'
```

以及：`app/libs/android-logging-log4j-1.0.3.jar`

相关的类：`L.java` 、 `LogTrackerTree.java` 、`App.java`、`SettingFragment.kt`

### 崩溃统计

跟坚果云一样，使用的是 [Fabric][fabric] 的 [Crashlytics](https://docs.fabric.io/android/crashlytics/overview.html)

[Fabric][fabric] 还提供了 Android Studio 插件，方便在项目中**接入** [Fabric][fabric] SDK。

Fabric SDK 文档：[Fabric SDK][fabric-sdk]

### 埋点

为了方便统计用户的使用习惯，坚果扫描接入了 [Countly][countly]

Countly SDK 文档：[Countly SDK][countly-sdk]

需要添加埋点的位置：

![埋点](./images/坚果扫描v1.0.0埋点.jpg)



| 追加                                   | addEditScan             |
| -------------------------------------- | ----------------------- |
| 单页删除                               | deleteSingleEditScan    |
| 多选删除                               | deleteMultipageEditScan |
| 选择的页数统计                         | pagesNumberImportScan   |
| 手动                                   | manualScan              |
| 分享                                   | sharePreview            |
| 多页预览按键                           | multipagePreview        |
| 删除                                   | deleteSelect            |
| 分享                                   | shareSelect             |
| 下拉刷新                               | pullRefresh             |
| 登录                                   | logIn                   |
| 注册                                   | signUp                  |
| 整个扫描时间统计（从进入到保存成文档） | timeScan                |
| 编辑                                   | multipagePreviewScan    |
| 手动                                   | timeManualScan          |
| 自动                                   | timeAutoScan            |
| 微信登录                               | timeLoginWechat         |
| 微信注册                               | timeSignupWechat        |
| 账号登录                               | timeLoginAccount        |

### 删除页面（DSPage）的时机

1. 手动扫描（CaptureActivity）> 剪裁（EditPolygonActivity）> 点击“重拍”
2. 手动扫描（CaptureActivity）> 剪裁（EditPolygonActivity）> 点击“Back键”
3. 多页编辑（EditCapturesActivity）> 删除
4. 多页编辑（EditCapturesActivity）> 点击“Back键”
5. 多页编辑（EditCapturesActivity）> 点击“返回”
6. 预览（PreviewPagesActivity）> 删除

### 在线客服

坚果扫描接入了智齿客服 SDK。

appkey: c2a34ec272be4db0a78eeb3f418c8fac 

[V2.5.5-Android-SDK对接集成文档](https://shimo.im/doc/dG4WOqBZqUYCoYIv)

[1]: https://scanbot.io/en/sdk.html
[2]: https://github.com/doo
[3]: https://github.com/doo/Scanbot-SDK-Examples
[4]: https://github.com/doo/Scanbot-SDK-Examples/wiki
[5]: http://doo.github.io/Scanbot-SDK-Documentation/Android/
[6]: http://doo.github.io/Scanbot-SDK-Documentation/Android/Core/
[7]: http://doo.github.io/Scanbot-SDK-Documentation/Android/net/doo/snap/persistence/PageFactory.html
[fabric]: https://fabric.io/home
[fabric-sdk]: https://docs.fabric.io/android/fabric/overview.html
[countly]: http://countly.jianguoyun.com/
[countly-sdk]: https://resources.count.ly/docs/countly-sdk-for-android
