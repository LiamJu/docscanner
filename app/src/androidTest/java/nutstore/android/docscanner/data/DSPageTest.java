package nutstore.android.docscanner.data;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import nutstore.android.docscanner.util.L;

/**
 * @author Zhu Liang
 */
public class DSPageTest {

    @Test
    public void name() throws Exception {
        JSONArray ja = new JSONArray();
        for (int i = 0; i < 4; i++) {
            JSONObject jo = new JSONObject();
            jo.put("x", "1.0f");
            jo.put("y", "0.0f");
            ja.put(jo);
        }

        String json = ja.toString();

        L.d("", json);
    }
}