package nutstore.android.docscanner.data;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import net.doo.snap.entity.Document;
import net.doo.snap.entity.DocumentType;
import net.doo.snap.entity.Language;
import net.doo.snap.entity.OcrStatus;
import net.doo.snap.entity.OptimizationType;
import net.doo.snap.entity.Page;
import net.doo.snap.entity.RotationType;
import net.doo.snap.process.DocumentProcessingResult;
import net.doo.snap.ui.EditPolygonImageView;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.Injection;
import nutstore.android.sdk.util.schedulers.ImmediateSchedulerProvider;

/**
 * @author Zhu Liang
 */
@RunWith(AndroidJUnit4.class)
public class DocScannerRepositoryTest {

    private DocScannerRepository mRepository;

    @Before
    public void setUp() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();
        mRepository = Injection.provideDocumentRepository(context, new ImmediateSchedulerProvider());
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testDocumentResults() throws Exception {
        DSDocumentResult[] results = getDocumentResults();

        mRepository.saveDocumentResult(results);

        List<DSDocumentResult> resultList = mRepository.listDocumentResults();
        Assert.assertNotNull(resultList);
    }

    @Test
    public void deleteAll() throws Exception {
        mRepository.deleteAll();

        List<DSDocumentResult> resultList = mRepository.listDocumentResults();
        Assert.assertTrue(resultList.isEmpty());
    }

    private DSDocumentResult[] getDocumentResults() {
        DSDocumentResult[] results = new DSDocumentResult[5];
        for (int i = 0; i < 5; i++) {
            String documentName = "document-" + i;
            Random random = new Random();
            Document document = new Document();
            document.setId(UUID.randomUUID().toString());
            document.setName(documentName);
            document.setDate(new Date().getTime());
            document.setPagesCount(random.nextInt(20));
            document.setSize(random.nextLong());
            document.setThumbnailUri("thumbnail-uri-" + i);
            document.setOcrStatus(OcrStatus.PENDING);
            document.setLanguage(Language.AFR);
            document.setOcrText("ocr-" + i);
            document.setDocumentType(DocumentType.ACADEMIA);

            List<Page> pages = new ArrayList<>();
            for (int j = 0; j < 8; j++) {
                Page page = new Page(UUID.randomUUID().toString());
                page.getParameters().putString(
                        Constants.KEY_PREVIEW_PATH, documentName + "-path-" + j);
                page.setOptimizationType(OptimizationType.COLOR_DOCUMENT);
                page.setRotationType(RotationType.ROTATION_90);
                page.setPolygon(EditPolygonImageView.DEFAULT_POLYGON);
                page.setImageSize(Page.ImageType.COMBINED, random.nextInt(10), random.nextInt(20));
                page.setImageSize(Page.ImageType.OVERLAY_TMP, random.nextInt(10), random.nextInt(20));
                pages.add(page);
            }
            File file = new File("file-" + i);
            DocumentProcessingResult result = new DocumentProcessingResult(document, pages, file);
            results[i] = new DSDocumentResult(result);
        }
        return results;
    }
}