package nutstore.android.docscanner.service

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.content.ContextCompat
import net.doo.snap.ScanbotSDK
import net.doo.snap.entity.SnappingDraft
import net.doo.snap.process.DocumentProcessingResult
import net.doo.snap.process.DocumentProcessor
import net.doo.snap.process.draft.DocumentDraftExtractor
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.Injection
import nutstore.android.docscanner.R
import nutstore.android.docscanner.data.DSDocumentResult
import nutstore.android.docscanner.data.DSPage
import nutstore.android.docscanner.data.DocScannerRepository
import nutstore.android.docscanner.util.L
import nutstore.android.docscanner.util.NotificationHelper
import nutstore.android.sdk.exception.ServerException
import nutstore.android.sdk.util.Preconditions
import nutstore.android.sdk.util.schedulers.ImmediateSchedulerProvider
import org.greenrobot.eventbus.EventBus
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * 将 Pages 转换成 Document 的服务
 *
 * @author Zhu Liang
 */
class DocumentService : IntentService("DocumentService") {

    private lateinit var documentDraftExtractor: DocumentDraftExtractor
    private lateinit var documentProcessor: DocumentProcessor
    private lateinit var mDocScannerRepository: DocScannerRepository
    private lateinit var mNotificationHelper: NotificationHelper

    companion object {

        private val TAG = DocumentService::class.java.simpleName

        fun deletePages(context: Context?, pages: ArrayList<DSPage>) {
            if (context == null) return

            val intent = Intent(context, DocumentService::class.java)
            intent.action = Constants.ACTION_DELETE_PAGES
            intent.putParcelableArrayListExtra(Constants.EXTRA_PAGES, pages)
            ContextCompat.startForegroundService(context, intent)
        }

        fun deletePage(context: Context?, page: DSPage) {
            if (context == null) return

            val intent = Intent(context, DocumentService::class.java)
            intent.action = Constants.ACTION_DELETE_PAGE
            intent.putExtra(Constants.EXTRA_PAGE, page)
            ContextCompat.startForegroundService(context, intent)
        }

        fun createDocument(context: Context?, documentName: String, pages: ArrayList<DSPage>) {
            if (context == null) return

            val intent = Intent(context, DocumentService::class.java)
            intent.action = Constants.ACTION_CREATE_DOCUMENT
            intent.putExtra(Constants.EXTRA_DOCUMENT_NAME, documentName)
            intent.putParcelableArrayListExtra(Constants.EXTRA_PAGES, pages)
            ContextCompat.startForegroundService(context, intent)
        }

        fun syncDocuments(context: Context?, results: List<DSDocumentResult>) {
            if (context == null) return

            val intent = Intent(context, DocumentService::class.java)
            intent.action = Constants.ACTION_SYNC_DOCUMENTS
            intent.putParcelableArrayListExtra(Constants.EXTRA_DOCUMENT_RESULTS, ArrayList(results))
            ContextCompat.startForegroundService(context, intent)
        }
    }

    override fun onCreate() {
        super.onCreate()

        val scanbotSDK = ScanbotSDK(this)
        documentDraftExtractor = scanbotSDK.documentDraftExtractor()
        documentProcessor = scanbotSDK.documentProcessor()
        mDocScannerRepository = Injection.provideDocumentRepository(this, ImmediateSchedulerProvider())

        mNotificationHelper = NotificationHelper(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent == null) return
        val action = intent.action ?: return

        when (action) {
            Constants.ACTION_CREATE_DOCUMENT -> {
                val documentName = intent.getStringExtra(Constants.EXTRA_DOCUMENT_NAME)
                val pages = intent.getParcelableArrayListExtra<DSPage>(Constants.EXTRA_PAGES)
                handleActionCreateDocument(documentName, pages)
            }
            Constants.ACTION_SYNC_DOCUMENTS -> {
                val results = intent.getParcelableArrayListExtra<DSDocumentResult>(Constants.EXTRA_DOCUMENT_RESULTS)!!
                handleActionSyncDocuments(results)
            }
            Constants.ACTION_DELETE_PAGES -> {
                val pages = intent.getParcelableArrayListExtra<DSPage>(Constants.EXTRA_PAGES)
                handleActionDeletePages(pages)
            }
            Constants.ACTION_DELETE_PAGE -> {
                val page = intent.getParcelableExtra<DSPage>(Constants.EXTRA_PAGE)
                handleActionDeletePage(page)
            }
        }
    }

    private fun handleActionCreateDocument(documentName: String, dsPages: ArrayList<DSPage>) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationHelper = NotificationHelper(this)
            val builder = notificationHelper.getNotification1(R.string.app_name, R.string.module_document_service_create_document)
            startForeground(Constants.NOTI_ID_CREATE_DOCUMENT, builder.build())
        } else {
            val builder = mNotificationHelper.getNotification1(R.string.app_name, R.string.module_document_service_create_document)
                    .setSmallIcon(R.drawable.ic_stat_notify_syncing)
                    .setTicker(getText(R.string.module_document_service_create_document))
            mNotificationHelper.notify(Constants.NOTI_ID_CREATE_DOCUMENT, builder)
        }


        L.d(TAG, "handleActionCreateDocument: ")
        Preconditions.checkNotNull(dsPages)

        val snappingDraft = SnappingDraft()
        dsPages.forEach { it ->
            snappingDraft.addPage(it.page)
        }
        snappingDraft.documentName = documentName
        snappingDraft.isCombined = true
        val documentDraftExtractor = documentDraftExtractor
        val documentDrafts = documentDraftExtractor.extract(snappingDraft)

        val result: DocumentProcessingResult? = try {
            documentProcessor.processDocument(documentDrafts[0])
        } catch (e: Exception) {
            L.e(TAG, "handleActionCreateDocument: ", e)
            null
        }

        val status = if (result != null) {
            mDocScannerRepository.saveDocumentResult(DSDocumentResult(result))
            CreateDocEvent.STATUS_SUCCESS
        } else {
            CreateDocEvent.STATUS_FAILED
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            mNotificationHelper.cancel(Constants.NOTI_ID_CREATE_DOCUMENT)
        }
        EventBus.getDefault().post(CreateDocEvent(status))
    }

    private fun handleActionSyncDocuments(results: ArrayList<DSDocumentResult>) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationHelper = NotificationHelper(this)
            val builder = notificationHelper.getNotification1(R.string.app_name, R.string.module_document_service_sync_documents)
            startForeground(Constants.NOTI_ID_SYNC_DOCUMENTS, builder.build())
        } else {
            val builder = mNotificationHelper.getNotification1(R.string.app_name, R.string.module_document_service_sync_documents)
                    .setSmallIcon(R.drawable.ic_stat_notify_syncing)
                    .setTicker(getText(R.string.module_document_service_sync_documents))
            mNotificationHelper.notify(Constants.NOTI_ID_SYNC_DOCUMENTS, builder)
        }

        val nutstoreRepository = Injection.provideNutstoreRepository(this)
        var changed = false

        // 当 syncFailedMsg 的 ErrorCode 不为 NONE 时，将终止同步操作
        val syncFailedMsg = SyncFailedMsg()

        for (result in results) {

            if (syncFailedMsg.errorCode != SyncFailedMsg.NONE) {
                break
            }

            if (result.synced) {
                L.d(TAG, "Jump: ${result.document.name}")
                continue
            }

            nutstoreRepository
                    .uploadDocsToNutScan(result)
                    .subscribe(
                            {
                                if (changed.not()) {
                                    changed = true
                                }
                                result.synced = true
                            },
                            {
                                L.e(TAG, "Sync failed: ", it)

                                if (it is ServerException) {
                                    syncFailedMsg.errorCode = handleServerException(it)
                                } else if (it is UnknownHostException
                                        || it is ConnectException
                                        || it is SocketTimeoutException) {
                                    syncFailedMsg.errorCode = SyncFailedMsg.NO_NETWORK
                                }
                            })
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            mNotificationHelper.cancel(Constants.NOTI_ID_SYNC_DOCUMENTS)
        }

        if (changed) {
            // 所有文档同步完成之后
            EventBus.getDefault().post(results)
        }

        if (syncFailedMsg.errorCode != SyncFailedMsg.NONE) {
            EventBus.getDefault().post(syncFailedMsg)
        }
    }

    private fun handleActionDeletePages(pages: ArrayList<DSPage>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationHelper = NotificationHelper(this)
            val builder = notificationHelper.getNotification1(R.string.app_name, R.string.module_document_service_delete_pages)
            startForeground(Constants.NOTI_ID_DELETE_PAGES, builder.build())
        }

        mDocScannerRepository.deletePages(pages)
    }

    private fun handleActionDeletePage(page: DSPage) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationHelper = NotificationHelper(this)
            val builder = notificationHelper.getNotification1(R.string.app_name, R.string.module_document_service_delete_page)
            startForeground(Constants.NOTI_ID_DELETE_PAGE, builder.build())
        }

        mDocScannerRepository.deletePage(page)
    }

    private fun handleServerException(e: ServerException): String {
        when (e.errorCode) {
            ServerException.STORAGESPACE_EXHAUSTED -> {
                return SyncFailedMsg.STORAGE_SPACE_EXHAUSTED
            }
            ServerException.TRAFFIC_RATE_EXHAUSTED -> {
                return SyncFailedMsg.TRAFFIC_RATE_EXHAUSTED
            }
            else -> {
                return SyncFailedMsg.NONE
            }
        }
    }
}