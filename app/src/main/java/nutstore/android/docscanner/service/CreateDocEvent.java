package nutstore.android.docscanner.service;

/**
 * @author Zhu Liang
 */

public class CreateDocEvent {

    /**
     * 表示创建文档成功时的状态
     */
    public static final int STATUS_SUCCESS = 1;

    /**
     * 表示创建文档失败是的状态
     */
    public static final int STATUS_FAILED = 2;

    /**
     * 创建文档的状态
     *
     * @see #STATUS_SUCCESS
     * @see #STATUS_FAILED
     */
    public final int status;

    public CreateDocEvent(int status) {
        this.status = status;
    }
}
