package nutstore.android.docscanner.service;

import android.support.annotation.NonNull;

import nutstore.android.docscanner.Constants;

/**
 * @author Zhu Liang
 */

public class SyncFailedMsg {

    public static final String NONE = Constants.EMPTY_STRING;

    /**
     * 空间不足
     */
    public static final String STORAGE_SPACE_EXHAUSTED = "StorageSpaceExhausted";

    /**
     * 流量不足
     */
    public static final String TRAFFIC_RATE_EXHAUSTED = "TrafficRateExhausted";

    public static final String NO_NETWORK = "NoNetwork";

    @NonNull
    public String errorCode = NONE;
}
