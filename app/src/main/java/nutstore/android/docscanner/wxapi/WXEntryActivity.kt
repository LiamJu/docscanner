package nutstore.android.docscanner.wxapi

import android.content.Intent
import android.os.Bundle
import com.tencent.mm.opensdk.modelbase.BaseReq
import com.tencent.mm.opensdk.modelbase.BaseResp
import com.tencent.mm.opensdk.modelmsg.SendAuth
import com.tencent.mm.opensdk.openapi.IWXAPI
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.ui.base.BaseActivity
import org.greenrobot.eventbus.EventBus


class WXEntryActivity : BaseActivity(), IWXAPIEventHandler {

    private var api: IWXAPI? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        api = WXAPIFactory.createWXAPI(this, Constants.WX_APP_ID, false)
        api!!.handleIntent(intent, this)


    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        setIntent(intent)
        api!!.handleIntent(intent, this)
    }

    override fun onResp(p0: BaseResp?) {
        if (p0 == null) return

        when (p0.errCode) {
            BaseResp.ErrCode.ERR_OK -> {
                if (p0 is SendAuth.Resp) {
                    val code = p0.code
                    EventBus.getDefault().post(code)
                    finish()
                }
            }
        }
    }

    override fun onReq(p0: BaseReq?) {

    }
}
