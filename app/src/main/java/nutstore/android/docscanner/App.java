package nutstore.android.docscanner;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.sobot.chat.SobotApi;

import net.doo.snap.ScanbotSDKInitializer;

import org.greenrobot.greendao.database.Database;

import io.pigcasso.photopicker.PhotoLoader;
import io.pigcasso.photopicker.PhotoPicker;
import io.pigcasso.photopicker.ThemeConfig;
import nutstore.android.docscanner.data.DaoMaster;
import nutstore.android.docscanner.data.DaoMaster.DevOpenHelper;
import nutstore.android.docscanner.data.DaoSession;
import nutstore.android.docscanner.exception.DSUncaughtExceptionHandler;
import nutstore.android.docscanner.task.ImageLoader;
import nutstore.android.docscanner.ui.GlideApp;
import nutstore.android.docscanner.util.L;
import nutstore.android.sdk.util.Utils;

/**
 * @author Zhu Liang
 */

public class App extends MultiDexApplication {

    private static final String TAG = "App";

    private final String LICENSE_KEY =
            "RIdFhTubljbhRO2/uJ6z700DIcGoIU" +
                    "wex0kHXwgl/eSFQtpWXmYbHmwauDxT" +
                    "rDqN1TANnU6aysQ+v64kIgQ4PFb2Se" +
                    "yCi0ejeDiiuYW4JBXuq+FkZ4hvoZkO" +
                    "qLlW2c/BAPyEYecFdUU/WWUJ4n2+2x" +
                    "AD13pJ0qEjXqRiVTkhIfpE+uu/pd37" +
                    "It/UFCFtmWnFfTQ1AVDWHb0SeVni9A" +
                    "ozzxVIBhuqd9StmhPZqdg1vsfe8Gnq" +
                    "DNzn7d9u0Eu9ZLX4swIo7NLZDxo2qo" +
                    "c454GNldMBYOi3VXetKKNV8RLeTCrb" +
                    "Pj5j6UyY2SzJPXgCN14Wq9PylevVRt" +
                    "5nZz8IUUoWvg==\nU2NhbmJvdFNESw" +
                    "pudXRzdG9yZS5hbmRyb2lkLmRvY3Nj" +
                    "YW5uZXIKMTU2MDY0MzE5OQo1OTAKMg" +
                    "==\n";

    public static final boolean ENCRYPTED = false;

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        // 从Android N开始，通过其他应用打开文件时，默认不再支持文件绝对路径的Url（会抛出FileUriExposedException）。
        // 参考了 Amaze File Manager 的解决方案，添加了下面的代码。
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        //Register uncaught exception handler
        Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new DSUncaughtExceptionHandler(defaultHandler));

        new ScanbotSDKInitializer()
                .license(this, LICENSE_KEY)
                .initialize(this);


        DevOpenHelper helper = new DevOpenHelper(this, ENCRYPTED ? "doc-scanner-db-encrypted" : "doc-scanner-db");
        Database db = ENCRYPTED ? helper.getEncryptedWritableDb("super-secret") : helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();

        // 初始化图片选择器
        PhotoPicker.Companion.setPhotoLoader(new GlidePhotoLoader());
        ThemeConfig themeConfig = new ThemeConfig();
        themeConfig.setArrowDropColor(Color.WHITE);
        themeConfig.setBottomBarBackgroundColor(ContextCompat.getColor(this, R.color.primary));
        themeConfig.setRadioCheckedColor(ContextCompat.getColor(this, R.color.primary));
        PhotoPicker.Companion.setThemeConfig(themeConfig);

        // 初始化图片加载器
        ImageLoader.setPlaceHolderDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.white)));
        ImageLoader.setErrorDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.white)));

        // 初始化 Utils
        Utils.init(this);

        // 收集日志信息
        L.init(this);

        // 在线客服
        SobotApi.initSobotSDK(this, Constants.SOBOT_APP_KEY, null);
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    private class GlidePhotoLoader implements PhotoLoader {

        @Override
        public void loadPhoto(ImageView imageView, String s, int i, int i1) {
            GlideApp.with(imageView).load(s).into(imageView);
        }
    }
}
