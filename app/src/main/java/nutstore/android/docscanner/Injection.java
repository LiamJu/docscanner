package nutstore.android.docscanner;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import net.doo.snap.persistence.DocumentStoreStrategy;
import net.doo.snap.persistence.PageStoreStrategy;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import nutstore.android.docscanner.data.DaoSession;
import nutstore.android.docscanner.data.DocScannerRepository;
import nutstore.android.docscanner.data.NutstoreRepository;
import nutstore.android.docscanner.data.UserInfoRepository;
import nutstore.android.sdk.ApiManager;
import nutstore.android.sdk.NutstoreAPI;
import nutstore.android.sdk.NutstoreUploadFileApi;
import nutstore.android.sdk.internal.HttpConfig;
import nutstore.android.sdk.util.StringUtils;
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider;
import nutstore.android.sdk.util.schedulers.SchedulerProvider;

/**
 * @author Zhu Liang
 */

public class Injection {

    public static BaseSchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.instance();
    }

    public static DocScannerRepository provideDocumentRepository(Context context) {
        return new DocScannerRepository(provideDaoSession(context), provideDocumentStoreStrategy(context),
                providePageStoreStrategy(context), provideSchedulerProvider());
    }

    public static DocScannerRepository provideDocumentRepository(Context context, BaseSchedulerProvider schedulerProvider) {
        return new DocScannerRepository(provideDaoSession(context), provideDocumentStoreStrategy(context),
                providePageStoreStrategy(context), schedulerProvider);
    }

    private static DaoSession provideDaoSession(Context context) {
        return ((App) context.getApplicationContext()).getDaoSession();
    }

    @NotNull
    public static NutstoreAPI provideNutstoreAPI(Context context) {
        return ApiManager.getNutstoreAPI(Constants.BASE_URL,
                provideAuthToken(context),
                Locale.getDefault().equals(Locale.PRC) ? Locale.PRC.toString() : Locale.US.toString(),
                HttpConfig.getUserAgent(BuildConfig.VERSION_NAME));
    }

    @NonNull
    private static NutstoreUploadFileApi provideNutstoreUploadFileApi(Context context) {
        return ApiManager.getNutstoreUploadFileApi(Constants.BASE_URL,
                provideAuthToken(context),
                Locale.getDefault().equals(Locale.PRC) ? Locale.PRC.toString() : Locale.US.toString(),
                HttpConfig.getUserAgent(BuildConfig.VERSION_NAME));
    }

    @NotNull
    public static UserInfoRepository provideUserInfoRepository(@NotNull Context context) {
        return UserInfoRepository.getInstance(context.getApplicationContext());
    }

    @NotNull
    public static NutstoreRepository provideNutstoreRepository(@NotNull Context context) {
        return new NutstoreRepository(context, provideNutstoreAPI(context), provideNutstoreUploadFileApi(context));
    }

    private static String provideAuthToken(Context context) {
        UserInfoRepository repository = provideUserInfoRepository(context);
        String username = repository.getUsername();
        String machineName = repository.getMachineName();
        String token = repository.getToken();
        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(machineName) && StringUtils.isNotEmpty(token)) {
            return HttpConfig.getAuthorization(repository.getUsername(),
                    repository.getMachineName(),
                    repository.getToken());
        } else {
            return Constants.EMPTY_STRING;
        }
    }

    private static DocumentStoreStrategy provideDocumentStoreStrategy(Context context) {
//        return new DocumentStoreStrategy(context, context.getSharedPreferences("snapping", Context.MODE_PRIVATE));
        return new DocumentStoreStrategy(context, null);
    }

    private static PageStoreStrategy providePageStoreStrategy(Context context) {
        return new PageStoreStrategy((Application) context.getApplicationContext());

    }
}
