package nutstore.android.docscanner.data;

import android.support.annotation.VisibleForTesting;

import net.doo.snap.persistence.DocumentStoreStrategy;
import net.doo.snap.persistence.PageStoreStrategy;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import nutstore.android.docscanner.util.L;
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider;

/**
 * @author Zhu Liang
 */

public class DocScannerRepository {

    private static final String TAG = "DocScannerRepository";

    private DSDocumentResultDao mDocumentResultDao;
    private DSDocumentDao mDocumentDao;
    private DSPageDao mPageDao;
    private DocumentStoreStrategy mDocumentStoreStrategy;
    private PageStoreStrategy mPageStoreStrategy;
    private BaseSchedulerProvider mSchedulerProvider;

    public DocScannerRepository(DaoSession daoSession, DocumentStoreStrategy documentStoreStrategy,
                                PageStoreStrategy pageStoreStrategy, BaseSchedulerProvider schedulerProvider) {
        mDocumentResultDao = daoSession.getDSDocumentResultDao();
        mDocumentDao = daoSession.getDSDocumentDao();
        mPageDao = daoSession.getDSPageDao();

        mDocumentStoreStrategy = documentStoreStrategy;
        mPageStoreStrategy = pageStoreStrategy;

        mSchedulerProvider = schedulerProvider;
    }

    public void saveDocumentResult(DSDocumentResult... results) {
        mDocumentResultDao.insertInTx(results);
        for (DSDocumentResult result : results) {
            mDocumentDao.insert(result.getDocument());
            mPageDao.insertInTx(result.getPages());
        }
    }

    public Flowable<List<DSDocumentResult>> listDocumentResultsRx() {
        return Flowable
                .create(new FlowableOnSubscribe<List<DSDocumentResult>>() {
                    @Override
                    public void subscribe(FlowableEmitter<List<DSDocumentResult>> e) throws Exception {
                        e.onNext(listDocumentResults());
                        e.onComplete();
                    }
                }, BackpressureStrategy.LATEST)
                .subscribeOn(mSchedulerProvider.io());
    }

    @VisibleForTesting
    protected List<DSDocumentResult> listDocumentResults() throws Exception {
        List<DSDocumentResult> results = mDocumentResultDao.queryRaw("", new String[]{});

        for (DSDocumentResult result : results) {
            // 加载的文档有可能是从内存中读取的，所以要将所有的文档设置为"未同步"
            result.setSynced(false);
            String where = "WHERE " + DSDocumentDao.Properties.Id.columnName + "=?";
            String selectionArg = result.getDid();
            DSDocument document = mDocumentDao.queryRaw(where, selectionArg).get(0);
            result.setDocument(document);

            List<DSPage> pages = new ArrayList<>();
            JSONArray ja = new JSONArray(result.getPageIds());
            int size = ja.length();
            for (int i = 0; i < size; i++) {
                String pageId = ja.getString(i);

                where = "WHERE " + DSPageDao.Properties.Id.columnName + "=?";

                List<DSPage> temp = mPageDao.queryRaw(where, pageId);

                if (temp != null && !temp.isEmpty()) {
                    pages.addAll(temp);
                }
            }
            result.setPages(pages);
        }
        return results;
    }

    public void deleteDocumentResults(List<DSDocumentResult> results) {
        for (DSDocumentResult result : results) {
            // 从数据库删除 result 数据
            mDocumentResultDao.delete(result);
            // 从数据库删除 document 数据
            mDocumentDao.delete(result.getDocument());
            try {
                DSDocument document = result.getDocument();
                // 删除 document 的缓存文件
                File documentDir = mDocumentStoreStrategy.getDocumentDir(document.getId());
                if (documentDir != null) {
                    FileUtils.deleteDirectory(documentDir);
                    L.d(TAG, "Successfully delete document directory: " + documentDir.getPath());
                } else {
                    L.d(TAG, "documentDir is null");
                }
            } catch (IOException e) {
                L.e(TAG, "deleteDocument: ", e);
            }

            // 从数据库删除 pages 数据
            List<DSPage> pages = result.getPages();
            deletePages(pages);
        }
    }

    public void deletePages(List<DSPage> pages) {
        // 删除 pages 的缓存文件
        for (DSPage page : pages) {
            deletePage(page);
        }
    }

    public void deletePage(DSPage page) {
        mPageDao.delete(page);
        try {
            File pageDir = mPageStoreStrategy.getPageDir(page.getId());
            if (pageDir != null) {
                FileUtils.deleteDirectory(pageDir);
                L.d(TAG, "Successfully delete page directory: " + pageDir.getAbsolutePath());
            } else {
                L.d(TAG, "pageDir is null");
            }
        } catch (IOException e) {
            L.e(TAG, "deletePage: ", e);
        }
    }

    public void deleteAll() {
        mDocumentDao.deleteAll();
        mPageDao.deleteAll();
        mDocumentResultDao.deleteAll();
    }
}
