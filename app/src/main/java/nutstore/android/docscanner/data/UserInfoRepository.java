package nutstore.android.docscanner.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.doo.snap.entity.OptimizationType;

import nutstore.android.docscanner.Constants;
import nutstore.android.sdk.module.UserInfo;
import nutstore.android.sdk.util.JsonWrapper;
import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.StringUtils;

/**
 * 保存：Username, Token, UserInfo 以及偏好设置
 *
 * @author Zhu Liang
 */

public class UserInfoRepository {

    private static final String SP_NAME = "preference.USER_INFO";
    private static final String KEY_USERNAME = "key.USERNAME";
    private static final String KEY_TOKEN = "key.TOKEN";
    private static final String KEY_MACHINE_NAME = "key.MACHINE_NAME";
    private static final String KEY_USER_INFO = "key.USER_INFO";
    private static final String KEY_AUTO_UPLOAD = "key.AUTO_UPLOAD";
    private static final String KEY_ONLY_WIFI = "key.ONLY_WIFI";
    private static final String KEY_USE_FLASH = "key.FLASH_MODE";
    private static final String KEY_IMAGE_FILTER = "key.IMAGE_FILTER";

    private static UserInfoRepository instance;

    private SharedPreferences mSharedPreferences;


    public static UserInfoRepository getInstance(Context context) {
        if (instance == null) {
            synchronized (UserInfoRepository.class) {
                if (instance == null) {
                    instance = new UserInfoRepository(context);
                }
            }
        }
        return instance;
    }

    private UserInfoRepository(Context context) {
        mSharedPreferences = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }

    public void saveUsernameAndTokenAndMachineName(String username, String token, String machineName) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(username));
        Preconditions.checkArgument(StringUtils.isNotEmpty(token));
        Preconditions.checkArgument(StringUtils.isNotEmpty(machineName));

        saveUsername(username);
        saveToken(token);
        saveMachineName(machineName);
    }

    public void saveUsername(String username) {
        mSharedPreferences.edit().putString(KEY_USERNAME, username).apply();
    }

    public String getUsername() {
        return mSharedPreferences.getString(KEY_USERNAME, Constants.EMPTY_STRING);
    }

    public void saveToken(String token) {
        mSharedPreferences.edit().putString(KEY_TOKEN, token).apply();
    }

    public String getToken() {
        return mSharedPreferences.getString(KEY_TOKEN, Constants.EMPTY_STRING);
    }

    public void saveMachineName(String machine) {
        Preconditions.checkNotNull(machine);
        mSharedPreferences.edit().putString(KEY_MACHINE_NAME, machine).apply();
    }

    public String getMachineName() {
        return mSharedPreferences.getString(KEY_MACHINE_NAME, Constants.EMPTY_STRING);
    }

    public void saveUserInfo(@NonNull UserInfo userInfo) {
        Preconditions.checkNotNull(userInfo);
        String json = JsonWrapper.toJson(userInfo);
        mSharedPreferences.edit().putString(KEY_USER_INFO, json).apply();
    }

    @Nullable
    public UserInfo getUserInfo() {
        String json = mSharedPreferences.getString(KEY_USER_INFO, Constants.EMPTY_STRING);
        if (!json.equals(Constants.EMPTY_STRING)) {
            return JsonWrapper.fromJson(json, UserInfo.class);
        } else {
            return null;
        }
    }

    public void saveAutoUpload(boolean autoUpload) {
        mSharedPreferences.edit().putBoolean(KEY_AUTO_UPLOAD, autoUpload).apply();
    }

    public boolean isAutoUpload() {
        return mSharedPreferences.getBoolean(KEY_AUTO_UPLOAD, false);
    }

    /**
     * 设置是否仅 WiFi 下上传
     */
    public void saveOnlyWiFi(boolean onlyWiFi) {
        mSharedPreferences.edit().putBoolean(KEY_ONLY_WIFI, onlyWiFi).apply();
    }

    public boolean isOnlyWiFi() {
        return mSharedPreferences.getBoolean(KEY_ONLY_WIFI, false);
    }

    public void saveUseFlash(boolean useFlash) {
        mSharedPreferences.edit().putBoolean(KEY_USE_FLASH, useFlash).apply();
    }

    public boolean isUseFlash() {
        return mSharedPreferences.getBoolean(KEY_USE_FLASH, false);
    }

    public void saveImageFilter(OptimizationType type) {
        mSharedPreferences.edit().putInt(KEY_IMAGE_FILTER, type.getCode()).apply();
    }

    public OptimizationType getImageFilter() {
        return OptimizationType.getByCode(mSharedPreferences.getInt(KEY_IMAGE_FILTER,
                OptimizationType.BLACK_AND_WHITE.getCode()));
    }

    public void removeAll() {
        mSharedPreferences
                .edit()
                .remove(KEY_AUTO_UPLOAD)
                .remove(KEY_MACHINE_NAME)
                .remove(KEY_TOKEN)
                .remove(KEY_USERNAME)
                .remove(KEY_USER_INFO)
                .remove(KEY_ONLY_WIFI)
                .remove(KEY_USE_FLASH)
                .remove(KEY_IMAGE_FILTER)
                .apply();
    }
}
