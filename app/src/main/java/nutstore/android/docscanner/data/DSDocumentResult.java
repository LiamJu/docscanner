package nutstore.android.docscanner.data;

import android.os.Parcel;
import android.os.Parcelable;

import net.doo.snap.entity.Document;
import net.doo.snap.entity.Page;
import net.doo.snap.process.DocumentProcessingResult;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.StringUtils;

/**
 * 对应 {@link DocumentProcessingResult} 类
 *
 * @author Zhu Liang
 */

@Entity
public class DSDocumentResult implements Parcelable {

    @Id(autoincrement = true)
    private Long id;
    @Property
    private String did;
    @Property
    private String pageIds;

    /**
     * 文档的绝对路径
     */
    @Property
    private String path;

    /**
     * 文档缩略图的绝对路径
     */
    @Property
    private String thumbnailPath;

    @Transient
    private DSDocument document;
    @Transient
    private List<DSPage> pages;

    @Transient
    private Boolean synced = false;

    public DSDocumentResult(DocumentProcessingResult result) {
        Preconditions.checkNotNull(result);
        Document d = result.getDocument();
        did = d.getId();
        List<Page> pages = result.getPages();
        List<DSPage> dsPages = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        for (Page page : pages) {
            jsonArray.put(page.getId());
            dsPages.add(new DSPage(page));
        }
        pageIds = jsonArray.toString();
        path = result.getDocumentFile().getAbsolutePath();
        thumbnailPath = d.getThumbnailUri();
        Preconditions.checkArgument(StringUtils.isNotEmpty(thumbnailPath));
        document = new DSDocument(d);
        this.pages = dsPages;
    }

    @Generated(hash = 677646720)
    public DSDocumentResult(Long id, String did, String pageIds, String path, String thumbnailPath) {
        this.id = id;
        this.did = did;
        this.pageIds = pageIds;
        this.path = path;
        this.thumbnailPath = thumbnailPath;
    }

    @Generated(hash = 24353146)
    public DSDocumentResult() {
    }

    public Long getId() {
        return id;
    }

    public String getDid() {
        return this.did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getPageIds() {
        return this.pageIds;
    }

    public void setPageIds(String pageIds) {
        this.pageIds = pageIds;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getThumbnailPath() {
        return this.thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public DSDocument getDocument() {
        return document;
    }

    public void setDocument(DSDocument document) {
        this.document = document;
    }

    public List<DSPage> getPages() {
        return pages;
    }

    public void setPages(List<DSPage> pages) {
        this.pages = pages;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getSynced() {
        return synced;
    }

    public void setSynced(Boolean synced) {
        this.synced = synced;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.did);
        dest.writeString(this.pageIds);
        dest.writeString(this.path);
        dest.writeString(this.thumbnailPath);
        dest.writeParcelable(this.document, flags);
        dest.writeTypedList(this.pages);
        dest.writeValue(this.synced);
    }

    protected DSDocumentResult(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.did = in.readString();
        this.pageIds = in.readString();
        this.path = in.readString();
        this.thumbnailPath = in.readString();
        this.document = in.readParcelable(DSDocument.class.getClassLoader());
        this.pages = in.createTypedArrayList(DSPage.CREATOR);
        this.synced = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<DSDocumentResult> CREATOR = new Creator<DSDocumentResult>() {
        @Override
        public DSDocumentResult createFromParcel(Parcel source) {
            return new DSDocumentResult(source);
        }

        @Override
        public DSDocumentResult[] newArray(int size) {
            return new DSDocumentResult[size];
        }
    };
}
