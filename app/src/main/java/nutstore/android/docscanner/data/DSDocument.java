package nutstore.android.docscanner.data;

import android.os.Parcel;
import android.os.Parcelable;

import net.doo.snap.entity.Document;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * 对应 {@link net.doo.snap.entity.Document} 类
 *
 * @author Zhu Liang
 */

@Entity
public class DSDocument implements Parcelable {

    @Id
    private String id;
    @Property
    private String name;
    @Property
    private Long date;
    @Property
    private Integer pagesCount;
    @Property
    private Long size = -1L;
    @Property
    private String thumbnailUri;
    @Property
    private String ocrText;
    @Property
    private String documentType;

    public DSDocument(Document document) {
        this.id = document.getId();
        this.name = document.getName();
        this.date = document.getDate();
        this.pagesCount = document.getPagesCount();
        this.size = document.getSize();
        this.thumbnailUri = document.getThumbnailUri();
        this.ocrText = document.getOcrText();
        this.documentType = document.getDocumentType().getMimeType();
    }

    @Generated(hash = 965458259)
    public DSDocument(String id, String name, Long date, Integer pagesCount,
            Long size, String thumbnailUri, String ocrText, String documentType) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.pagesCount = pagesCount;
        this.size = size;
        this.thumbnailUri = thumbnailUri;
        this.ocrText = ocrText;
        this.documentType = documentType;
    }

    @Generated(hash = 1554559521)
    public DSDocument() {
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDate() {
        return this.date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getPagesCount() {
        return this.pagesCount;
    }

    public void setPagesCount(Integer pagesCount) {
        this.pagesCount = pagesCount;
    }

    public Long getSize() {
        return this.size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getThumbnailUri() {
        return this.thumbnailUri;
    }

    public void setThumbnailUri(String thumbnailUri) {
        this.thumbnailUri = thumbnailUri;
    }

    public String getOcrText() {
        return this.ocrText;
    }

    public void setOcrText(String ocrText) {
        this.ocrText = ocrText;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeValue(this.date);
        dest.writeValue(this.pagesCount);
        dest.writeValue(this.size);
        dest.writeString(this.thumbnailUri);
        dest.writeString(this.ocrText);
        dest.writeString(this.documentType);
    }

    protected DSDocument(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.date = (Long) in.readValue(Long.class.getClassLoader());
        this.pagesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.size = (Long) in.readValue(Long.class.getClassLoader());
        this.thumbnailUri = in.readString();
        this.ocrText = in.readString();
        this.documentType = in.readString();
    }

    public static final Parcelable.Creator<DSDocument> CREATOR = new Parcelable.Creator<DSDocument>() {
        @Override
        public DSDocument createFromParcel(Parcel source) {
            return new DSDocument(source);
        }

        @Override
        public DSDocument[] newArray(int size) {
            return new DSDocument[size];
        }
    };
}
