package nutstore.android.docscanner.data;

import android.content.Context;

import org.reactivestreams.Publisher;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import nutstore.android.docscanner.R;
import nutstore.android.sdk.NutstoreAPI;
import nutstore.android.sdk.NutstoreUploadFileApi;
import nutstore.android.sdk.consts.PathConsts;
import nutstore.android.sdk.module.Metadata;
import nutstore.android.sdk.module.MetadataList;
import nutstore.android.sdk.module.PathInternal;
import nutstore.android.sdk.module.Sandbox;
import nutstore.android.sdk.module.UserInfo;
import nutstore.android.sdk.util.MetadataUtils;
import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.SandboxUtils;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * @author Zhu Liang
 */

public class NutstoreRepository {

    private NutstoreAPI mNutstoreAPI;
    private NutstoreUploadFileApi mNutstoreUploadFileApi;
    private static boolean sDidGetUserInfo = false;
    private final String NUT_SCAN_DISPLAY_NAME;

    public NutstoreRepository(Context context, NutstoreAPI nutstoreAPI, NutstoreUploadFileApi nutstoreUploadFileApi) {
        this.mNutstoreAPI = Preconditions.checkNotNull(nutstoreAPI);
        this.mNutstoreUploadFileApi = Preconditions.checkNotNull(nutstoreUploadFileApi);
        NUT_SCAN_DISPLAY_NAME = context.getString(R.string.common_nut_scan_display_name);
    }

    /**
     * 获取坚果扫描同步文件夹下所有子文件（不包括文件夹）
     */
    public Flowable<List<Metadata>> listNutScanMetadatas() {
        Flowable<UserInfo> flowable;
        if (!sDidGetUserInfo) {
            flowable = mNutstoreAPI.getUserInfoV2(1);
            sDidGetUserInfo = true;
        } else {
            flowable = mNutstoreAPI.getUserInfoV2();
        }
        return flowable
                .flatMap(new Function<UserInfo, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(UserInfo userInfo) throws Exception {
                        return Flowable.fromIterable(userInfo.getSandboxes());
                    }
                })
                .filter(new Predicate<Sandbox>() {
                    @Override
                    public boolean test(Sandbox sandbox) throws Exception {
                        return sandbox.isIsDefault();
                    }
                })
                .firstOrError()
                .toFlowable()
                .map(new Function<Sandbox, List<Metadata>>() {
                    @Override
                    public List<Metadata> apply(Sandbox sandbox) throws Exception {
                        // 创建 "/我的坚果云/坚果云扫描/" 目录
                        Metadata metadata = mNutstoreAPI.mkdir(SandboxUtils.encodeSandboxId(sandbox),
                                SandboxUtils.encodeMagic(sandbox),
                                new PathInternal(PathConsts.PATH_SEPARATOR + NUT_SCAN_DISPLAY_NAME)).execute().body();
                        return listMetadatas(sandbox, metadata.getPath());
                    }
                });
    }

    /**
     * 上传文档到 {@link #NUT_SCAN_DISPLAY_NAME} 的同步文件夹下
     *
     * @throws FileNotFoundException
     * @throws nutstore.android.sdk.exception.ServerException
     */
    public Flowable<Metadata> uploadDocsToNutScan(final DSDocumentResult result) {
        return mNutstoreAPI
                .getUserInfoV2()
                .flatMap(new Function<UserInfo, Publisher<Sandbox>>() {
                    @Override
                    public Publisher<Sandbox> apply(UserInfo userInfo) throws Exception {
                        return Flowable.fromIterable(userInfo.getSandboxes());
                    }
                })
                .filter(new Predicate<Sandbox>() {
                    @Override
                    public boolean test(Sandbox sandbox) throws Exception {
                        return sandbox.isIsDefault();
                    }
                })
                .firstOrError()
                .toFlowable()
                .flatMap(new Function<Sandbox, Publisher<Metadata>>() {
                    @Override
                    public Publisher<Metadata> apply(Sandbox sandbox) throws Exception {
                        // 创建 "/我的坚果云/坚果云扫描/" 目录
                        Metadata metadata = mNutstoreAPI.mkdir(SandboxUtils.encodeSandboxId(sandbox),
                                SandboxUtils.encodeMagic(sandbox),
                                new PathInternal(PathConsts.PATH_SEPARATOR + NUT_SCAN_DISPLAY_NAME)).execute().body();
                        // 上传文件
                        File source = new File(result.getPath());
                        if (source.exists() && source.isFile()) {
                            RequestBody body = RequestBody.create(null, new File(result.getPath()));
                            return mNutstoreUploadFileApi.uploadFile(SandboxUtils.encodeSandboxId(sandbox),
                                    SandboxUtils.encodeMagic(sandbox), MetadataUtils.getPath(metadata, source.getName()), body);
                        } else {
                            throw new FileNotFoundException(source.getName() + " not found");
                        }
                    }
                });
    }

    /**
     * 遍历 sandbox 同步文件夹下路径为 path 的文件（夹）的所有直接子文件列表
     *
     * @param sandbox 同步文件夹
     * @param path    绝对路径
     * @return 所有直接子文件列表
     */
    private List<Metadata> listMetadatas(Sandbox sandbox, String path) throws IOException {
        String tag = null;
        String marker = null;
        List<Metadata> metadatas = new ArrayList<>();
        MetadataList metadataList;
        do {
            Call<MetadataList> metadataListCall = mNutstoreAPI.getMetadataList(SandboxUtils.encodeSandboxId(sandbox),
                    SandboxUtils.encodeMagic(sandbox), path, tag, marker);
            metadataList = metadataListCall.execute().body();

            tag = metadataList.getEtag();

            List<Metadata> contents = metadataList.getContents();
            if (contents != null && !contents.isEmpty()) {
                marker = MetadataUtils.getDisplayName(contents.get(contents.size() - 1));
            }

            metadatas.addAll(contents);

        } while (metadataList.isTruncated() && marker != null);

        return metadatas;
    }
}
