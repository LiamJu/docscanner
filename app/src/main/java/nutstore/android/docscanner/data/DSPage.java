package nutstore.android.docscanner.data;

import android.graphics.Point;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;

import net.doo.snap.entity.OptimizationType;
import net.doo.snap.entity.Page;
import net.doo.snap.entity.RotationType;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nutstore.android.docscanner.Constants;
import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.StringUtils;

/**
 * 对应 {@link Page} 类
 *
 * @author Zhu Liang
 */

@Entity
public class DSPage implements Parcelable {

    @Id
    private String id;
    @Property
    private String path;
    @Property
    private String originalPath;
    @Property
    private Integer optimizationType;
    @Property
    private Integer rotationType;
    @Property
    private String polygon;
    @Property
    private String imageSizes;
    @Property
    private Boolean processed;

    public DSPage(Page page) {
        id = page.getId();
        path = page.getParameters().getString(Constants.KEY_PREVIEW_PATH);
        Preconditions.checkArgument(path != null && !path.isEmpty());
        originalPath = page.getParameters().getString(Constants.KEY_ORIGINAL_PATH);
        Preconditions.checkArgument(StringUtils.isNotEmpty(originalPath));
        optimizationType = page.getOptimizationType().getCode();
        rotationType = page.getRotationType().getDegrees();
        setPolygon(page.getPolygon());
        processed = page.isProcessed();

        try {
            setImageSizes(page);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setImageSizes(Page page) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        Page.ImageType[] imageTypes = Page.ImageType.values();
        if (imageTypes == null || imageTypes.length == 0) {
            return;
        }
        for (Page.ImageType imageType : imageTypes) {
            Point point = page.getImageSize(imageType);
            if (point != null) {
                JSONObject temp = new JSONObject();
                temp.put("x", point.x);
                temp.put("y", point.y);

                jsonObject.put(imageType.getFileName(), temp);
            }
        }
        imageSizes = jsonObject.toString();
    }

    private Point getImageTypes(Page.ImageType imageType) {
        if (imageSizes != null && !imageSizes.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(imageSizes);
                if (jsonObject.has(imageType.getFileName())) {
                    JSONObject temp = jsonObject.getJSONObject(imageType.getFileName());
                    int x = temp.getInt("x");
                    int y = temp.getInt("y");
                    return new Point(x, y);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public String getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(String originalPath) {
        this.originalPath = originalPath;
    }

    public void setPolygon(List<PointF> polygon) {
        try {
            JSONArray ja = new JSONArray();
            List<PointF> pointFS = polygon;
            for (PointF pointF : pointFS) {
                JSONObject jo = new JSONObject();
                jo.put("x", Float.toString(pointF.x));
                jo.put("y", Float.toString(pointF.y));
                ja.put(jo);
            }
            this.polygon = ja.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<PointF> getPolygonF() {
        List<PointF> pointFS = new ArrayList<>();
        try {
            JSONArray ja = new JSONArray(polygon);
            int length = ja.length();
            for (int i = 0; i < length; i++) {
                JSONObject jo = ja.getJSONObject(i);
                float x = Float.valueOf(jo.getString("x"));
                float y = Float.valueOf(jo.getString("y"));
                pointFS.add(new PointF(x, y));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return pointFS;
    }

    public RotationType getRotationTypeEnum() {
        return RotationType.getByDegrees(rotationType);
    }

    public void setRotationType(RotationType type) {
        rotationType = type.getDegrees();
    }

    public void setOptimizationType(OptimizationType optimizationType) {
        this.optimizationType = optimizationType.getCode();
    }

    public OptimizationType getOptimizationTypeEnum() {
        return OptimizationType.getByCode(optimizationType);
    }

    public Page getPage() {
        Page page = new Page(id);
        page.getParameters().putString(Constants.KEY_PREVIEW_PATH, path);
        page.getParameters().putString(Constants.KEY_ORIGINAL_PATH, originalPath);
        page.setOptimizationType(getOptimizationTypeEnum());
        page.setRotationType(getRotationTypeEnum());
        page.setPolygon(getPolygonF());
        page.setProcessed(processed);
        Page.ImageType[] imageTypes = Page.ImageType.values();
        for (Page.ImageType imageType : imageTypes) {
            Point point = getImageTypes(imageType);
            if (point != null) {
                page.setImageSize(imageType, point.x, point.y);
            }
        }
        return page;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.path);
        dest.writeString(this.originalPath);
        dest.writeInt(this.optimizationType);
        dest.writeInt(this.rotationType);
        dest.writeString(this.polygon);
        dest.writeByte(this.processed ? (byte) 1 : (byte) 0);
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOptimizationType(Integer optimizationType) {
        this.optimizationType = optimizationType;
    }

    public void setRotationType(Integer rotationType) {
        this.rotationType = rotationType;
    }

    public String getPolygon() {
        return this.polygon;
    }

    public void setPolygon(String polygon) {
        this.polygon = polygon;
    }

    public boolean getProcessed() {
        return this.processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public String getImageSizes() {
        return this.imageSizes;
    }

    public void setImageSizes(String imageSizes) {
        this.imageSizes = imageSizes;
    }

    public Integer getOptimizationType() {
        return this.optimizationType;
    }

    public Integer getRotationType() {
        return this.rotationType;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    protected DSPage(Parcel in) {
        this.id = in.readString();
        this.path = in.readString();
        this.originalPath = in.readString();
        this.optimizationType = in.readInt();
        this.rotationType = in.readInt();
        this.polygon = in.readString();
        this.processed = in.readByte() != 0;
    }

    @Generated(hash = 1632676208)
    public DSPage(String id, String path, String originalPath, Integer optimizationType,
            Integer rotationType, String polygon, String imageSizes, Boolean processed) {
        this.id = id;
        this.path = path;
        this.originalPath = originalPath;
        this.optimizationType = optimizationType;
        this.rotationType = rotationType;
        this.polygon = polygon;
        this.imageSizes = imageSizes;
        this.processed = processed;
    }

    @Generated(hash = 1322198132)
    public DSPage() {
    }

    public static final Parcelable.Creator<DSPage> CREATOR = new Parcelable.Creator<DSPage>() {
        @Override
        public DSPage createFromParcel(Parcel source) {
            return new DSPage(source);
        }

        @Override
        public DSPage[] newArray(int size) {
            return new DSPage[size];
        }
    };
}
