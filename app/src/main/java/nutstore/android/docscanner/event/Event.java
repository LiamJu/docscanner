package nutstore.android.docscanner.event;

/**
 * @author Zhu Liang
 */

public enum Event {

    /**
     * 扫描-编辑-追加
     */
    addEditScan,
    /**
     * 扫描-编辑-单页删除
     */
    deleteSingleEditScan,
    /**
     * 扫描-预览方式-多选删除
     */
    deleteMultipageEditScan,
    /**
     * 扫描-导入-选择的页数统计
     */
    pagesNumberImportScan,
    /**
     * 扫描-扫描-手动
     */
    manualScan,
    /**
     * 浏览-分享
     */
    sharePreview,
    /**
     * 浏览-多页预览按钮
     */
    multipagePreview,
    /**
     * 选择-删除
     */
    deleteSelect,
    /**
     * 选择-分享
     */
    shareSelect,
    /**
     * 同步-下拉刷新
     */
    pullRefresh,
    /**
     * 设置-登录
     */
    login,
    /**
     * 设置-注册
     */
    signUp,

    /**
     * 停留事件-扫描-整个扫描时间统计（从进入到保存文档）
     */
    timeScan,
    /**
     * 停留事件-扫描-编辑
     */
    multiPagePreviewScan,
    /**
     * 停留事件-扫描-扫描时间-手动
     */
    timeManualScan,
    /**
     * 停留事件-扫描-扫描时间-自动
     */
    timeAutoScan,
    /**
     * 停留事件-设置-登录时间统计-微信登录
     */
    timeLoginWechat,
    /**
     * 停留事件-设置-登录时间统计-微信注册
     */
    timeSignupWechat,
    /**
     * 停留事件-设置-登录时间统计-账号登录
     */
    timeLoginAccount,

}
