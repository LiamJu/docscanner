package nutstore.android.docscanner.event;

import ly.count.android.sdk.Countly;

import static java.lang.System.currentTimeMillis;

/**
 * @author Zhu Liang
 */

public class RecordEvent {

    private static RecordEvent instance;

    /**
     * 记录跳转到 {@link nutstore.android.docscanner.ui.capture.CaptureActivity} 的时刻
     */
    private long startScanMillisTime;

    /**
     * 记录跳转到 {@link nutstore.android.docscanner.ui.editcapture.EditCapturesActivity} 的时刻
     */
    private long startEditCapturesTime;

    public static RecordEvent getInstance() {
        if (instance == null) {
            synchronized (RecordEvent.class) {
                if (instance == null) {
                    instance = new RecordEvent();
                }
            }
        }
        return instance;
    }

    public void recordEvent(final EventParams params) {
        Countly.sharedInstance().recordEvent(params.event.name(), params.segmentation,
                params.count, params.sum, params.dur);
    }

    /**
     * 开始扫描
     */
    public void startScan() {
        startScanMillisTime = currentTimeMillis();
    }

    /**
     * 结束扫描（保存文档）
     */
    public void endScan() {
        long dur = currentTimeMillis() - startScanMillisTime;
        startScanMillisTime = 0;
        recordEvent(new EventParams.Builder(Event.timeScan)
                .dur(dur)
                .build());
    }

    /**
     * 开始编辑多页
     */
    public void startEditCaptures() {
        startEditCapturesTime = currentTimeMillis();
    }

    /**
     * 退出编辑多页
     */
    public void endEditCaptures() {
        long dur = currentTimeMillis() - startEditCapturesTime;
        startEditCapturesTime = 0;
        recordEvent(new EventParams.Builder(Event.multiPagePreviewScan)
                .dur(dur).build());
    }
}
