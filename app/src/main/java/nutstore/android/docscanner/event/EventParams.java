package nutstore.android.docscanner.event;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import nutstore.android.sdk.util.Preconditions;

/**
 * @author Zhu Liang
 */

public class EventParams {
    @NonNull
    final Event event;
    @Nullable
    final Map<String, String> segmentation;
    final int count;
    final double sum;
    final double dur;

    private EventParams(@NonNull Event event, Map<String, String> segmentation, int count, double sum, double dur) {
        this.event = Preconditions.checkNotNull(event);
        this.segmentation = segmentation;
        this.count = count;
        this.sum = sum;
        this.dur = dur;
    }

    public static class Builder {
        @NonNull
        private final Event event;
        private Map<String, String> segmentation;
        private int count = 1;
        private double sum;
        private double dur;

        public Builder(@NonNull Event event) {
            this.event = Preconditions.checkNotNull(event);
        }

        public Builder addSegmentation(String key, String value) {
            if (segmentation == null) {
                segmentation = new HashMap<>();
            }
            segmentation.put(key, value);
            return this;
        }

        public Builder count(int count) {
            this.count = count;
            return this;
        }

        public Builder sum(double sum) {
            this.sum = sum;
            return this;
        }

        public Builder dur(double dur) {
            this.dur = dur;
            return this;
        }

        public EventParams build() {
            return new EventParams(event, segmentation, count, sum, dur);
        }
    }
}
