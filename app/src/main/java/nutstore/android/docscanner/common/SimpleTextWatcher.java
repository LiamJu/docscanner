package nutstore.android.docscanner.common;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * @author Zhu Liang
 * @version 1.0
 * @since 2017/4/18 下午9:01
 */

public class SimpleTextWatcher implements TextWatcher {
    private static final String TAG = SimpleTextWatcher.class.getSimpleName();

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
