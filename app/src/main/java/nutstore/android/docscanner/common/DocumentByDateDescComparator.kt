package nutstore.android.docscanner.common

import nutstore.android.docscanner.data.DSDocumentResult
import java.io.File

/**
 * @author Zhu Liang
 */
class DocumentByDateDescComparator : Comparator<DSDocumentResult> {
    override fun compare(o1: DSDocumentResult, o2: DSDocumentResult): Int {
        val doc1 = File(o1.path)
        val doc2 = File(o2.path)

        return doc2.lastModified().compareTo(doc1.lastModified())
    }

}