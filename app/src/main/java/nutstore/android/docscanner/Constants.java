package nutstore.android.docscanner;

/**
 * @author Zhu Liang
 */

public interface Constants {

    String EXTRA_AUTO_SNAPPING = "docscanner.extra.AUTO_SNAPPING";
    String EXTRA_MANUAL_SNAP = "docscanner.extra.MANUAL_SNAP";
    String EXTRA_RETURN_RESULT = "docscanner.extra.RETURN_RESULT";
    String EXTRA_EDITED_PAGE = "docscanner.extra.EDITED_DATA";
    String EXTRA_EDITED_PAGES = "docscanner.extra.EDITED_DATAS";
    String EXTRA_CROPPED_PAGE = "docscanner.extra.CROPPED_PAGE";
    String EXTRA_CAPTURE_PAGES = "docscanner.extra.CAPTURE_PAGES";
    String EXTRA_EDIT_POLYGON = "docscanner.extra.EDIT_POLYGON";
    String EXTRA_PREVIEW_PAGES = "docscanner.extra.PREVIEW_PAGES";
    String EXTRA_PAGES = "docscanner.extra.PAGES";
    String EXTRA_PAGE = "docscanner.extra.PAGE";
    String EXTRA_DOCUMENT_NAME = "docscanner.extra.DOCUMENT_NAME";
    String EXTRA_DOCUMENT_RESULT = "docscanner.extra.DOCUMENT_RESULT";
    String EXTRA_POSITION = "docscanner.extra.POSITION";
    String EXTRA_AUTH_CODE = "docscanner.extra.CODE";
    String EXTRA_SANDBOX = "docscanner.extra.SANDBOX";
    String EXTRA_DOCUMENT_RESULTS = "docscanner.extra.EXTRA_DOCUMENT_RESULTS";

    String KEY_PREVIEW_PATH = "docscanner.key.PREVIEW_PATH";
    String KEY_ORIGINAL_PATH = "docscanner.key.ORIGINAL_PATH";
    String KEY_FLASH_ENABLED = "docscanner.key.FLASH_ENABLED";

    int REQUEST_CODE_CREATE_DOCUMENT = 0x110;
    int REQUEST_CODE_EDIT_POLYGON = 0x111;
    int REQUEST_CODE_EDIT_CAPTURE = 0x112;
    int REQUEST_CODE_PREVIEW_PAGES = 0x113;
    int REQUEST_CODE_CAPTURE = 0x114;
    int REQUEST_CODE_PHOTO_PICKER = 0x115;
    int REQUEST_CODE_WECHAT_AUTH = 0x116;

    int RC_PERMISSION_CAMERA = 0X120;

    /**
     * @see nutstore.android.docscanner.service.DocumentService
     */
    int NOTI_ID_CREATE_DOCUMENT = 1;

    /**
     * @see nutstore.android.docscanner.service.DocumentService
     */
    int NOTI_ID_SYNC_DOCUMENTS = 2;

    /**
     * @see nutstore.android.docscanner.service.DocumentService
     */
    int NOTI_ID_DELETE_PAGES = 3;

    /**
     * @see nutstore.android.docscanner.service.DocumentService
     */
    int NOTI_ID_DELETE_PAGE = 4;

    /**
     * 自动扫描的延迟时间
     */
    long CAPTURE_AUTO_SNAPPING_DELAY_MILLIS = 700L;

    String ACTION_CREATE_DOCUMENT = "docscanner.action.CREATE_DOCUMENT";
    String ACTION_SYNC_DOCUMENTS = "docscanner.action.SYNC_DOCUMENTS";
    String ACTION_DELETE_PAGES = "docscanner.action.DELETE_PAGES";
    String ACTION_DELETE_PAGE = "docscanner.action.DELETE_PAGE";

    /**
     * 微信 APP_ID
     */
    String WX_APP_ID = "wx33a85b3381c18747";

    /**
     * 在线客服 app key
     */
    String SOBOT_APP_KEY = "c2a34ec272be4db0a78eeb3f418c8fac";

    String EMPTY_STRING = "";

    String BASE_URL = "https://app.jianguoyun.com";
    String PKG_NAME_NUTSTORE = "nutstore.android";

    long MILLIS_IN_DAY = 24L * 60 * 60 * 1000;

    // 接收不到验证码
    String URL_NOT_RECEIVE_CODE = "http://www.jianguoyun.com/s/help/?p=2936";

    String NUTSTORE_WEB_SITE_HOME = "https://www.jianguoyun.com/";
    String URL_PRICING_PAGE = NUTSTORE_WEB_SITE_HOME + "s/pricing";
    String URL_RESET_PASSWORD = NUTSTORE_WEB_SITE_HOME + "d/forget_pwd";

    String AUTHORITY_FILE_PROVIDER = "nutstore.android.docscanner.fileprovider";

    String FEEDBACK_EMAIL_ADDRESS = "android.feedback@nutstore.net";

    /**
     * 保存文档的格式
     */
    String DOCUMENT_NAME_FORMAT = "yyyyMMdd-kkMMss";
}
