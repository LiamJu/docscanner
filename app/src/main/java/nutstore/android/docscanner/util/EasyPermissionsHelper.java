package nutstore.android.docscanner.util;

import android.Manifest;
import android.app.Activity;

import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.R;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

/**
 * @author Zhu Liang
 */

public class EasyPermissionsHelper {

    public static void requestCameraPermissions(Activity activity) {
        EasyPermissions.requestPermissions(new PermissionRequest.Builder(activity, Constants.RC_PERMISSION_CAMERA, Manifest.permission.CAMERA)
                .setRationale(R.string.common_camera_rationale)
                .setPositiveButtonText(R.string.common_ok)
                .setNegativeButtonText(R.string.common_cancel)
                .build());
    }

    public static void showCameraDeniedDialog(Activity activity) {
        new AppSettingsDialog.Builder(activity).setTitle(R.string.common_camera_title)
                .setRationale(R.string.common_camera_rationale)
                .setPositiveButton(R.string.common_ok)
                .setNegativeButton(R.string.common_cancel)
                .build()
                .show();
    }
}
