package nutstore.android.docscanner.util;

import android.app.Application;
import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import nutstore.android.docscanner.BuildConfig;
import timber.log.Timber;

/**
 * @author Zhu Liang
 */

public class L {

    public static void init(Application application) {

        File logFile = null;

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File externalFileDir = application.getExternalFilesDir(null);
            File logDir;
            try {
                FileUtils.forceMkdir(logDir = new File(externalFileDir, ".logs"));
            } catch (IOException e) {
                logDir = null;
            }
            if (logDir != null) {
                logFile = new File(logDir, "nut-scan.log");
            }
        }

        // 收集日志信息
        if (BuildConfig.DEBUG) {
            if (logFile != null) {
                Timber.plant(new Timber.DebugTree(), LogTrackerTree.getInstance(logFile));
            } else {
                Timber.plant(new Timber.DebugTree());
            }
        } else {
            if (logFile != null) {
                Timber.plant(LogTrackerTree.getInstance(logFile));
            }
        }
    }

    @NonNull
    public static File[] getLogFiles(Context context) {
        File logDir = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File externalFileDir = context.getExternalFilesDir(null);
            logDir = new File(externalFileDir, ".logs");
        }
        if (logDir != null && logDir.exists() && logDir.isDirectory()) {
            return logDir.listFiles();
        } else {
            return new File[]{};
        }
    }

    public static void d(String tag, String msg) {
        Timber.tag(tag).d(msg);
    }

    public static void e(String tag, String msg, Throwable tr) {
        Timber.tag(tag).e(tr, msg);
    }

    public static void i(String tag, String msg) {
        Timber.tag(tag).i(msg);
    }

    public static String getStackTraceString(Throwable tr) {
        return Log.getStackTraceString(tr);
    }
}
