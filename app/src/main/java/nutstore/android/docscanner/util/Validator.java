package nutstore.android.docscanner.util;

import android.support.annotation.NonNull;

import java.util.regex.Pattern;

/**
 * @author Zhu Liang
 */

public final class Validator {

    public static int EMAIL_MIN_LENGTH = 4;
    public static int EMAIL_MAX_LENGTH = 54;

    public static int PASSWORD_MIN_LENGTH = 6;
    public static int PASSWORD_MAX_LENGTH = 54;

    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$");
    private static final Pattern INVALID_EMAIL_PATTERN = Pattern.compile(
            "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.con$");

    public static boolean validateEmail(@NonNull String email) {
        if (isNullOrEmpty(email) || email.length() < EMAIL_MIN_LENGTH || email.length() > EMAIL_MAX_LENGTH)
            return false;
        if (INVALID_EMAIL_PATTERN.matcher(email).matches()) {
            return false;
        }
        return EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean validatePassword(String password) {
        if (password.length() < PASSWORD_MIN_LENGTH && password.length() > PASSWORD_MAX_LENGTH) {
            return false;
        }
        return true;
    }

    public static boolean isPasscode(@NonNull String passcode) {
        return passcode.matches("^[0-9]{6}$");
    }

    private static boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }
}
