package nutstore.android.docscanner.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;

import net.doo.snap.entity.OptimizationType;
import net.doo.snap.entity.Page;
import net.doo.snap.lib.detector.ContourDetector;
import net.doo.snap.persistence.PageFactory;
import net.doo.snap.persistence.PageStoreStrategy;
import net.doo.snap.ui.EditPolygonImageView;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.data.DSPage;

/**
 * @author Zhu Liang
 */

public class PageFactoryHelper {

    private static final Bitmap.CompressFormat COMPRESS_FORMAT = Bitmap.CompressFormat.WEBP;
    private static final int QUALITY = 100;


    /**
     * 构建 {@link DSPage}
     *
     * @param pageStoreStrategy 页面存储策略实例
     * @param images            图片文件字节数组
     * @param reqWidth          请求图片的宽
     * @param reqHeight         请求图片的高
     * @param detectorContour   是否需要探测轮廓
     * @return 返回 DSPage
     */
    public static DSPage buildDSPage(PageStoreStrategy pageStoreStrategy, byte[] images, int imageOrientation, int reqWidth, int reqHeight, boolean detectorContour, OptimizationType type) throws IOException {
        PageFactory.Result result = buildPage(pageStoreStrategy, images, imageOrientation, reqWidth, reqHeight);
        return buildDSPage(result, pageStoreStrategy, detectorContour, type);
    }

    /**
     * 构建 {@link DSPage}
     *
     * @param pageStoreStrategy 页面存储策略实例
     * @param pathname          图片的绝对路径
     * @param reqWidth          请求图片的宽
     * @param reqHeight         请求图片的高
     * @param detectorContour   是否需要探测轮廓
     * @return 返回 DSPage
     */
    public static DSPage buildDSPage(PageStoreStrategy pageStoreStrategy, String pathname, int reqWidth, int reqHeight, boolean detectorContour, OptimizationType type) throws IOException {
        PageFactory.Result result = buildPage(pageStoreStrategy, pathname, reqWidth, reqHeight);
        return buildDSPage(result, pageStoreStrategy, detectorContour, type);
    }

    private static DSPage buildDSPage(PageFactory.Result result, PageStoreStrategy pageStoreStrategy, boolean detectorContour, OptimizationType type) throws IOException {
        List<PointF> polygon = EditPolygonImageView.DEFAULT_POLYGON;
        if (detectorContour) {
            ContourDetector detector = new ContourDetector();
            detector.detect(result.preview);
            polygon = detector.getPolygonF();
        }

        Page page = result.page;
        page.setOptimizationType(type);
        File originalFile = pageStoreStrategy.getImageFile(page.getId(), Page.ImageType.ORIGINAL);
        page.getParameters().putString(Constants.KEY_ORIGINAL_PATH, originalFile.getAbsolutePath());
        File previewFile = pageStoreStrategy.getImageFile(page.getId(), Page.ImageType.PREVIEW);
        page.getParameters().putString(Constants.KEY_PREVIEW_PATH, previewFile.getAbsolutePath());
        page.setPolygon(polygon);
        return new DSPage(page);
    }

    /**
     * 这个方法对应 {@link PageFactory#buildPage(byte[], int, int)}，区别在于这个方法去除了添加
     * Preview Bitmap 到 LruCache 的步骤，以及将 Preview Bitmap 持久化到文件的过程从异步执行改为了同步执行。
     * 如此修改，最主要的原因是调用 {@link PageFactory#buildPage(byte[], int, int)} 将 {@link Page.ImageType#PREVIEW} 类型的文件在
     * {@link nutstore.android.docscanner.ui.editpolygon.EditPolygonActivity} 页面展示的时候一定几率图片的一部分显示成纯黑色。
     * <p>
     * 目前我们的项目中实际编辑、预览的图片都是{@link Page.ImageType#PREVIEW}类型的图片。
     * 虽说{@link Page.ImageType#ORIGINAL}类型的图片是同步执行的，然而在加载{@link Page.ImageType#ORIGINAL}类型图片的时候明显有延迟的情况。
     * 另外从观感上来看两种类型的图片差别不大。
     * <p/>
     * 如果图片的真实宽高大于请求的宽高，将对图片进行压缩。
     *
     * @param pageStoreStrategy {@link Page} 对应的文件缓存存储策略
     * @param images            图片的字节流
     * @param imageOrientation  图片的方向
     * @param reqWidth          请求的图片宽度
     * @param reqHeight         请求的图片高度
     */
    private static PageFactory.Result buildPage(PageStoreStrategy pageStoreStrategy, byte[] images, int imageOrientation, int reqWidth, int reqHeight) throws IOException {
        Page page = new Page();
        File pageDir = pageStoreStrategy.getPageDir(page.getId());
        FileUtils.forceMkdir(pageDir);
        FileUtils.forceMkdir(FileUtils.getFile(pageDir, "filtered"));

        // 保存 original 图片到本地
        File originalDest = new File(pageDir, Page.ImageType.ORIGINAL.getFileName());
        if (imageOrientation > 0) {
            Bitmap originalBitmap = BitmapFactory.decodeByteArray(images, 0, images.length);
            originalBitmap = rotateBitmap(originalBitmap, imageOrientation);
            writeBitmap(originalBitmap, originalDest);
        } else {
            FileUtils.writeByteArrayToFile(originalDest, images);
        }

        // 保存 preview 图片到本地
        Bitmap previewBitmap = ImageSizer.decodeSampledBitmapFromBytes(images, reqWidth, reqHeight);
        if (imageOrientation > 0) {
            previewBitmap = rotateBitmap(previewBitmap, imageOrientation);
        }
        page.setImageSize(Page.ImageType.OPTIMIZED_PREVIEW, previewBitmap.getWidth(), previewBitmap.getHeight());
        final String previewImagePath = pageStoreStrategy.getImageFile(page.getId(), Page.ImageType.PREVIEW).getPath();
        writeBitmap(previewBitmap, new File(previewImagePath));

        return new PageFactory.Result(page, previewBitmap);
    }

    private static PageFactory.Result buildPage(PageStoreStrategy pageStoreStrategy, String pathname, int reqWidth, int reqHeight) throws IOException {
        Page page = new Page();
        File pageDir = pageStoreStrategy.getPageDir(page.getId());
        FileUtils.forceMkdir(pageDir);
        FileUtils.forceMkdir(FileUtils.getFile(pageDir, "filtered"));

        // 保存 original 图片到本地
        FileUtils.copyFile(new File(pathname), new File(pageDir, Page.ImageType.ORIGINAL.getFileName()));

        // 保存 preview 图片到本地
        final Bitmap previewBitmap = ImageSizer.decodeSampledBitmapFromFile(pathname, reqWidth, reqHeight);
        page.setImageSize(Page.ImageType.OPTIMIZED_PREVIEW, previewBitmap.getWidth(), previewBitmap.getHeight());
        final String previewImagePath = pageStoreStrategy.getImageFile(page.getId(), Page.ImageType.PREVIEW).getPath();
        writeBitmap(previewBitmap, new File(previewImagePath));

        return new PageFactory.Result(page, previewBitmap);
    }

    private static Bitmap rotateBitmap(Bitmap bitmap, int imageOrientation) {
        final Matrix matrix = new Matrix();
        matrix.setRotate(imageOrientation, bitmap.getWidth() / 2f, bitmap.getHeight() / 2f);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
    }

    private static void writeBitmap(Bitmap source, File dest) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(dest);
            source.compress(COMPRESS_FORMAT, QUALITY, fos);
        } finally {
            IOUtils.closeQuietly(fos);
        }
    }
}
