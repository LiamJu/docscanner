package nutstore.android.docscanner.util;

import android.content.Context;
import android.content.Intent;

/**
 * @author Zhu Liang
 */

public class ContextUtils {

    public static void startActivity(Context context, Intent intent, String activityNotFoundMsg) {
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        } else {
            ToastUtils.showShort(context, activityNotFoundMsg);
        }
    }
}
