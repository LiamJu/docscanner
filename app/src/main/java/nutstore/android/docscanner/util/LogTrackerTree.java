package nutstore.android.docscanner.util;

import android.support.annotation.NonNull;
import android.util.Log;

import org.apache.log4j.Logger;

import java.io.File;

import de.mindpipe.android.logging.log4j.LogConfigurator;
import timber.log.Timber;

/**
 * @author Zhu Liang
 */

public class LogTrackerTree extends Timber.Tree {
    private static LogTrackerTree sInstance;
    private LogConfigurator mLogConfigurator = new LogConfigurator();

    public static LogTrackerTree getInstance(File logFile) {
        if (sInstance == null) {
            synchronized (LogTrackerTree.class) {
                if (sInstance == null) {
                    sInstance = new LogTrackerTree(logFile);
                }
            }
        }
        return sInstance;
    }

    private LogTrackerTree(File logFile) {
        String filePattern = "%d - [%c] - %p : %m%n";
        int maxBackupSize = 5;
        long maxFileSize = 1024 * 1024;

        configure(logFile.getAbsolutePath(), filePattern, maxBackupSize, maxFileSize);
    }

    private void configure(String fileName, String filePattern, int maxBackupSize, long maxFileSize) {
        mLogConfigurator.setFileName(fileName);
        mLogConfigurator.setMaxFileSize(maxFileSize);
        mLogConfigurator.setFilePattern(filePattern);
        mLogConfigurator.setMaxBackupSize(maxBackupSize);
        mLogConfigurator.setUseLogCatAppender(true);
        mLogConfigurator.configure();
    }

    @Override
    protected void log(int priority, String tag, @NonNull String message, Throwable t) {
        switch (priority) {
            case Log.VERBOSE:
                logVerbose();
                break;
            case Log.DEBUG:
                logDebug(tag, message, t);
                break;
            case Log.INFO:
                logInfo(tag, message, t);
                break;
            case Log.WARN:
                logWarning(tag, message, t);
                break;
            case Log.ERROR:
                logError(tag, message, t);
                break;
            case Log.ASSERT:
                logAssert();
                break;
            default:
                throw new IllegalStateException("Invalid priority: " + priority);
        }
    }

    private void logVerbose() {
        // nothing to do
    }

    private void logDebug(String tag, String msg, Throwable t) {
        Logger.getLogger(tag).debug(msg, t);
    }

    private void logInfo(String tag, String msg, Throwable t) {
        Logger.getLogger(tag).info(msg, t);
    }

    private void logWarning(String tag, String msg, Throwable t) {
        Logger.getLogger(tag).warn(msg, t);
    }

    private void logError(String tag, String msg, Throwable t) {
        Logger.getLogger(tag).error(msg, t);
    }

    private void logAssert() {
        // nothing to do
    }
}
