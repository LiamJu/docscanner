package nutstore.android.docscanner.util;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.Toast;

public final class ToastUtils {

    public static void showShort(@Nullable Context context, CharSequence text) {
        if (context != null) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        }
    }

    public static void showShort(@Nullable Context context, int resId) {
        if (context != null) {
            Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
        }
    }
}
