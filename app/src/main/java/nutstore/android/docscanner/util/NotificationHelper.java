package nutstore.android.docscanner.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import nutstore.android.docscanner.R;

/**
 * Helper class to manage notification channels, and create notifications.
 */

public class NotificationHelper extends ContextWrapper {

    private NotificationManager manager;
    public static final String PRIMARY_CHANNEL = "nutstore.android.docscanner.service.PRIMARY_CHANNEL";

    /**
     * Registers notification channels, which can be used later by individual notifications.
     *
     * @param ctx The application context
     */
    public NotificationHelper(Context ctx) {
        super(ctx);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel chan1 = new NotificationChannel(PRIMARY_CHANNEL,
                    getString(R.string.noti_channel_default), NotificationManager.IMPORTANCE_LOW);
            chan1.setLightColor(ContextCompat.getColor(this, R.color.primary_light));
            chan1.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(chan1);
        }
    }

    public NotificationCompat.Builder getNotification1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new NotificationCompat.Builder(getApplicationContext(), PRIMARY_CHANNEL);
        } else {
            return new NotificationCompat.Builder(getApplicationContext());
        }
    }

    public NotificationCompat.Builder getNotification1(int titleId, int bodyId) {
        return getNotification1(getString(titleId), getString(bodyId));
    }

    public NotificationCompat.Builder getNotification1(String title, String body) {
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(getApplicationContext(), PRIMARY_CHANNEL);
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }
        builder.setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_stat_notify_sync)
                .setColor(ContextCompat.getColor(this, R.color.primary_light))
                .setAutoCancel(true);
        return builder;
    }

    /**
     * Send a NotificationCompat.
     *
     * @param id           The ID of the notification
     * @param notification The notification object
     */
    public void notify(int id, NotificationCompat.Builder notification) {
        getManager().notify(id, notification.build());
    }

    public void cancel(int id) {
        getManager().cancel(id);
    }

    /**
     * Get the notification manager.
     * <p>
     * Utility method as this helper works with it a lot.
     *
     * @return The system service NotificationManager
     */
    private NotificationManager getManager() {
        if (manager == null) {
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }
}
