package nutstore.android.docscanner.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;

import nutstore.android.sdk.util.StringUtils;

/**
 * @author Zhu Liang
 */

public final class IntentUtils {

    public static Intent makeShareIntent(Context context, File file) {

        // 经过测试使用下面屏蔽掉的这种方法会导致分享到一些 APP 失败

        /*Uri uri = FileProvider.getUriForFile(context, Constants.AUTHORITY_FILE_PROVIDER, file);

        String extension = FilenameUtils.getExtension(file.getName());
        String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType(type);
        return shareIntent;*/

        String extension = FilenameUtils.getExtension(file.getName());
        String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        shareIntent.setType(type);
        return shareIntent;
    }

    public static Intent makeSendMailIntent(String address, String title, String text, File[] attachment) {
        Intent intent;
        if (attachment == null || attachment.length <= 1) {
            intent = new Intent(Intent.ACTION_SEND);
            if (attachment.length == 1) {
                Uri uri = Uri.fromFile(attachment[0]);
                intent.putExtra(Intent.EXTRA_STREAM, uri);
            }
        } else {
            intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            ArrayList<Uri> uris = new ArrayList<>();
            for (File file : attachment) {
                uris.add(Uri.fromFile(file));
                intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            }
        }

        if (StringUtils.isNotEmpty(address)) {
            String[] addresses = new String[1];
            addresses[0] = address;
            intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        }

        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("message/rfc822");

        return intent;
    }
}
