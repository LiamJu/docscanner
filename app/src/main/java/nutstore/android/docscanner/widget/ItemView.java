package nutstore.android.docscanner.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import nutstore.android.docscanner.R;

/**
 * @author Zhu Liang
 */

public class ItemView extends FrameLayout {

    public ItemView(Context context) {
        this(context, null);
    }

    public ItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflate(context, R.layout.item_view, this);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ItemView, defStyleAttr, R.style.ItemView_Widget);

        String startText = ta.getString(R.styleable.ItemView_startText);
        setStartText(startText);

        String endText = ta.getString(R.styleable.ItemView_endText);
        setEndText(endText);

        Drawable startDrawable = ta.getDrawable(R.styleable.ItemView_startDrawable);
        setStartDrawable(startDrawable);

        Drawable endDrawable = ta.getDrawable(R.styleable.ItemView_endDrawable);
        int endDrawableTint = ta.getColor(R.styleable.ItemView_endDrawableTint, Color.WHITE);
        setEndDrawable(endDrawable, endDrawableTint);

        ta.recycle();
    }

    public void setStartText(String text) {
        TextView startAction = findViewById(R.id.start_action);
        startAction.setText(text);
    }

    public void setEndText(CharSequence text) {
        TextView endAction = findViewById(R.id.end_action);
        endAction.setText(text);
    }

    public void setStartDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, Color.WHITE);
            DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
            TextView startAction = findViewById(R.id.start_action);
            startAction.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null);
        }
    }

    public void setEndDrawable(Drawable drawable, int drawableTint) {
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, drawableTint);
            DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
            TextView endAction = findViewById(R.id.end_action);
            endAction.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawable, null);
        }
    }
}
