package nutstore.android.docscanner.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import nutstore.android.docscanner.R;

/**
 * @author Zhu Liang
 */

public class NavigationView extends FrameLayout {

    private OnNavigationViewListener mOnNavigationViewListener;

    public NavigationView(Context context) {
        this(context, null);
    }

    public NavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setId(R.id.navigation_view);

        inflate(context, R.layout.navigation_view, this);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.NavigationView, defStyleAttr, R.style.NavigationView);

        CharSequence startText = ta.getText(R.styleable.NavigationView_startText);
        setStartText(startText);

        CharSequence endText = ta.getText(R.styleable.NavigationView_endText);
        setEndText(endText);

        Drawable startDrawable = ta.getDrawable(R.styleable.NavigationView_startDrawable);
        setStartDrawable(startDrawable);

        Drawable endDrawable = ta.getDrawable(R.styleable.NavigationView_endDrawable);
        setEndDrawable(endDrawable);

        CharSequence title = ta.getText(R.styleable.NavigationView_title);
        setTitle(title);

        ta.recycle();

        findViewById(R.id.start_action).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnNavigationViewListener != null) {
                    mOnNavigationViewListener.onStartClicked(v);
                }
            }
        });

        findViewById(R.id.end_action).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnNavigationViewListener != null) {
                    mOnNavigationViewListener.onEndClicked(v);
                }
            }
        });
    }

    public void setStartText(CharSequence text) {
        TextView startAction = findViewById(R.id.start_action);
        startAction.setText(text);
    }

    public void setEndText(CharSequence text) {
        TextView endAction = findViewById(R.id.end_action);
        endAction.setText(text);
    }

    public void setStartDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, Color.WHITE);
            DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
            TextView startAction = findViewById(R.id.start_action);
            startAction.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null);
        }
    }

    public void setEndDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, Color.WHITE);
            DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
            TextView endAction = findViewById(R.id.end_action);
            endAction.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawable, null);
        }
    }

    public void setTitle(CharSequence text) {
        TextView titleView = findViewById(R.id.title);
        titleView.setText(text);
    }

    public void setOnNavigationViewListener(OnNavigationViewListener onNavigationViewListener) {
        mOnNavigationViewListener = onNavigationViewListener;
    }

    public interface OnNavigationViewListener {
        void onStartClicked(View view);

        void onEndClicked(View view);
    }

    public static class SimpleOnNavigationViewListener implements OnNavigationViewListener {

        @Override
        public void onStartClicked(View view) {

        }

        @Override
        public void onEndClicked(View view) {

        }
    }

}
