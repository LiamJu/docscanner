package nutstore.android.docscanner.task;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.util.Util;

import net.doo.snap.entity.OptimizationType;
import net.doo.snap.entity.RotationType;
import net.doo.snap.lib.detector.ContourDetector;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.List;

import nutstore.android.docscanner.ui.GlideApp;
import nutstore.android.docscanner.util.L;
import nutstore.android.sdk.util.Preconditions;

/**
 * @author Zhu Liang
 */

public class ImageLoader {

    private static final String TAG = "ImageLoader";

    private static Drawable sErrorDrawable = new ColorDrawable(Color.parseColor("#FF2B2B2B"));
    private static Drawable sPlaceHolderDrawable = new ColorDrawable(Color.parseColor("#FF2B2B2B"));

    private static ImageLoader mInstance;

    public static ImageLoader getInstance() {
        if (mInstance == null) {
            synchronized (ImageLoader.class) {
                if (mInstance == null) {
                    mInstance = new ImageLoader();
                }
            }
        }
        return mInstance;
    }

    public static void setPlaceHolderDrawable(Drawable drawable) {
        if (drawable != null) {
            sPlaceHolderDrawable = drawable;
        }
    }

    public static void setErrorDrawable(Drawable drawable) {
        if (drawable != null) {
            sErrorDrawable = drawable;
        }
    }

    public void loadImage(ImageView imageView, String imagePath, RotationType rotationType, OptimizationType optimizationType, List<PointF> polygon) {
        Preconditions.checkNotNull(imageView);
        Preconditions.checkNotNull(imagePath);
        GlideApp
                .with(imageView)
                .load(new File(imagePath))
                .transform(new FillSpace(new Ops(rotationType.getDegrees(), optimizationType.getCode(), polygon)))
                .placeholder(sPlaceHolderDrawable)
                .error(sErrorDrawable)
                .into(imageView);
    }

    public void loadImage(ImageView imageView, String imagePath, RotationType rotationType, OptimizationType optimizationType, List<PointF> polygon, int width, int height) {
        Preconditions.checkNotNull(imageView);
        Preconditions.checkNotNull(imagePath);

        L.d(TAG, "loadImage: " + imagePath);

        GlideApp
                .with(imageView)
                .load(new File(imagePath))
                .override(width, height)
                .transform(new FillSpace(new Ops(rotationType.getDegrees(), optimizationType.getCode(), polygon)))
                .placeholder(sPlaceHolderDrawable)
                .error(sErrorDrawable)
                .into(imageView);
    }

    public void loadImage(ImageView imageView, String imagePath) {
        Preconditions.checkNotNull(imageView);
        Preconditions.checkNotNull(imagePath);
        GlideApp
                .with(imageView)
                .load(new File(imagePath))
                .placeholder(sPlaceHolderDrawable)
                .error(sErrorDrawable)
                .into(imageView);
    }

    public static class FillSpace extends BitmapTransformation {
        private static final String ID = "nutstore.android.docscanner.transformation.FillSpace";
        private static final byte[] ID_BYTES = ID.getBytes(Charset.forName("UTF-8"));

        private Ops ops;

        private FillSpace(Ops ops) {
            Preconditions.checkNotNull(ops);
            this.ops = ops;
        }

        @Override
        public Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {
            ContourDetector detector = new ContourDetector();
            Bitmap result = detector.processImageF(toTransform, ops.polygon, ops.imageFilter);

            Matrix matrix = new Matrix();
            matrix.postRotate(ops.degrees);
            return Bitmap.createBitmap(result, 0, 0, result.getWidth(), result.getHeight(), matrix, true);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            FillSpace fillSpace = (FillSpace) o;

            return ops.equals(fillSpace.ops);
        }

        @Override
        public int hashCode() {
            return Util.hashCode(ID.hashCode(),
                    Util.hashCode(ops.hashCode()));
        }

        @Override
        public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
            messageDigest.update(ID_BYTES);

            // int:   4字节
            // float: 4字节
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4 + 4 + 4 * 2 * 4)
                    .putInt(ops.degrees) // 旋转的角度
                    .putInt(ops.imageFilter); //滤镜

            // 剪裁边界坐标
            for (PointF pointF : ops.polygon) {
                byteBuffer
                        .putFloat(pointF.x)
                        .putFloat(pointF.y);
            }
            messageDigest.update(byteBuffer.array());
        }
    }

}
