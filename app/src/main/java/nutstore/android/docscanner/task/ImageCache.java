package nutstore.android.docscanner.task;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.StringUtils;

/**
 * @author Zhu Liang
 */

public class ImageCache {

    private static ImageCache sInstance;

    private LruCache<String, Bitmap> mLruCache;

    public static ImageCache getInstance() {
        if (sInstance == null) {
            synchronized (ImageCache.class) {
                if (sInstance == null) {
                    sInstance = new ImageCache();
                }
            }
        }
        return sInstance;
    }

    private ImageCache() {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 4);
        mLruCache = new LruCache<String, Bitmap>(maxMemory) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() * value.getHeight();
            }
        };
    }

    public void addImageToCache(String imagePath, Bitmap bitmap, int width, int height) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(imagePath), "pathname is empty");
        Preconditions.checkNotNull(bitmap);
        addImageToCache(imagePath, bitmap, width, height, new Ops());
    }

    public Bitmap getImageFromCache(String imagePath, int width, int height) {
        return getImageFromCache(imagePath, width, height, new Ops());
    }

    public void addImageToCache(String imagePath, Bitmap bitmap, int width, int height, Ops ops) {
        if (getImageFromCache(imagePath, width, height, ops) == null && bitmap != null) {
            synchronized (mLruCache) {
                mLruCache.put(imagePath + width + height + ops.toString(), bitmap);
            }
        }
    }

    public Bitmap getImageFromCache(String imagePath, int width, int height, Ops ops) {
        synchronized (mLruCache) {
            return mLruCache.get(imagePath + width + height + ops.toString());
        }
    }
}
