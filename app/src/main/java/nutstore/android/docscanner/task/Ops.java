package nutstore.android.docscanner.task;

import android.graphics.PointF;

import net.doo.snap.entity.OptimizationType;
import net.doo.snap.entity.RotationType;
import net.doo.snap.ui.EditPolygonImageView;

import java.util.List;

class Ops {
    final int degrees;
    final int imageFilter;
    final List<PointF> polygon;

    Ops() {
        this(RotationType.ROTATION_0.getDegrees(), OptimizationType.NONE.getCode(), EditPolygonImageView.DEFAULT_POLYGON);
    }

    Ops(int degrees, int imageFilter, List<PointF> polygon) {
        this.degrees = degrees;
        this.imageFilter = imageFilter;
        this.polygon = polygon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ops ops = (Ops) o;

        if (degrees != ops.degrees) return false;
        if (imageFilter != ops.imageFilter) return false;
        return polygon.equals(ops.polygon);
    }

    @Override
    public int hashCode() {
        int result = degrees;
        result = 31 * result + imageFilter;
        result = 31 * result + polygon.hashCode();
        return result;
    }
}