package nutstore.android.docscanner.ui.editcapture

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.annotation.MainThread
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.ListPopupWindow
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.target.Target
import com.github.chrisbanes.photoview.PhotoView
import net.doo.snap.entity.OptimizationType
import net.doo.snap.entity.RotationType
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.Injection
import nutstore.android.docscanner.R
import nutstore.android.docscanner.data.DSPage
import nutstore.android.docscanner.data.UserInfoRepository
import nutstore.android.docscanner.event.Event
import nutstore.android.docscanner.event.EventParams
import nutstore.android.docscanner.event.RecordEvent
import nutstore.android.docscanner.service.DocumentService
import nutstore.android.docscanner.task.ImageLoader
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.ui.base.ListAdapter
import nutstore.android.docscanner.ui.capture.CaptureActivity
import nutstore.android.docscanner.ui.common.OKCancelDialogFragment
import nutstore.android.docscanner.ui.editpolygon.EditPolygonActivity
import nutstore.android.docscanner.ui.previewpages.PreviewPagesActivity
import nutstore.android.docscanner.widget.NavigationView
import nutstore.android.sdk.util.Preconditions
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Zhu Liang
 */
class EditCapturesActivity : BaseActivity() {

    private lateinit var mEditedPages: ArrayList<DSPage>
    private lateinit var mViewPager: ViewPager
    private lateinit var mImageFiltersAdapter: ImageFiltersAdapter
    private lateinit var mUserInfoRepository: UserInfoRepository

    companion object {

        fun makeIntent(ctx: Context, pages: ArrayList<DSPage>): Intent {
            Preconditions.checkArgument(pages.isNotEmpty(), "pages is empty")
            val intent = Intent(ctx, EditCapturesActivity::class.java)
            intent.putParcelableArrayListExtra(Constants.EXTRA_CAPTURE_PAGES, pages)
            return intent
        }

        private const val FRAGMENT_DIALOG_ID_DELETE_PAGE = 1
        private const val FRAGMENT_DIALOG_ID_BACK_PRESSED = 2

        private const val FRAGMENT_DIALOG_TAG_DELETE_PAGE = "fragment.tag.DELETE_PAGE"
        private const val FRAGMENT_DIALOG_TAG_BACK_PRESSED = "fragment.tag.BACK_PRESSED"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_captures)

        mUserInfoRepository = Injection.provideUserInfoRepository(this)

        mEditedPages = intent.getParcelableArrayListExtra(Constants.EXTRA_CAPTURE_PAGES)
        Preconditions.checkArgument(mEditedPages.isNotEmpty(), "pages is empty")

        mViewPager = findViewById(R.id.viewPager_editCapture_gallery)
        mViewPager.adapter = EditCapturesAdapter(this)

        setNavigationTitle()

        mImageFiltersAdapter = ImageFiltersAdapter(this, listOf(ImageFilter(getString(R.string.filter_title_none), OptimizationType.NONE),
                ImageFilter(getString(R.string.filter_title_enchanced), OptimizationType.COLOR_ENHANCED),
                ImageFilter(getString(R.string.filter_title_grayscale), OptimizationType.GRAYSCALE),
                ImageFilter(getString(R.string.filter_title_black_and_white), OptimizationType.BLACK_AND_WHITE),
                ImageFilter(getString(R.string.filter_title_color_document), OptimizationType.COLOR_DOCUMENT)))

        // 添加
        findViewById<View>(R.id.tv_edit_capture_append).setOnClickListener({
            // 统计追加页面
            RecordEvent.getInstance().recordEvent(EventParams.Builder(Event.addEditScan).build())

            startActivityForResult(CaptureActivity.makeIntent(this@EditCapturesActivity),
                    Constants.REQUEST_CODE_CAPTURE)
        })

        // 旋转
        findViewById<View>(R.id.tv_edit_capture_rotate).setOnClickListener({
            rotatePage()
        })

        // 显示滤镜选择器
        findViewById<View>(R.id.tv_edit_capture_image_filters).setOnClickListener({
            showImageFiltersUi()
        })

        // 预览方式
        findViewById<View>(R.id.tv_edit_capture_preview).setOnClickListener({
            showPreviewUi()
        })

        // 剪裁
        findViewById<View>(R.id.tv_edit_capture_crop).setOnClickListener({
            showEditPolygonUi()
        })

        // 删除
        findViewById<View>(R.id.tv_edit_capture_delete).setOnClickListener({

            val msg = if (mEditedPages.size == 1) {
                getString(R.string.module_edit_capture_delete_document_msg)
            } else {
                getString(R.string.module_edit_capture_delete_page_msg)
            }
            OKCancelDialogFragment.newInstance("", msg,
                    getString(R.string.common_ok), getString(R.string.common_cancel),
                    FRAGMENT_DIALOG_ID_DELETE_PAGE, null).show(supportFragmentManager, FRAGMENT_DIALOG_TAG_DELETE_PAGE)
        })

        findViewById<NavigationView>(R.id.navigation_view).setOnNavigationViewListener(object : NavigationView.OnNavigationViewListener {
            override fun onStartClicked(view: View?) {
                showBackPressedDialog()
            }

            override fun onEndClicked(view: View?) {
                // 统计整个扫描时间，保存文档
                RecordEvent.getInstance().endScan()
                // 统计整个编辑多页时间
                RecordEvent.getInstance().endEditCaptures()

                val sdf = SimpleDateFormat(Constants.DOCUMENT_NAME_FORMAT, Locale.SIMPLIFIED_CHINESE)
                DocumentService.createDocument(this@EditCapturesActivity, sdf.format(Date()), mEditedPages)
                finish()
            }
        })

        mViewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                setNavigationTitle()
            }
        })
    }

    override fun onStart() {
        super.onStart()

        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onBackPressed() {
        showBackPressedDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Constants.REQUEST_CODE_PREVIEW_PAGES -> {
                if (Activity.RESULT_OK == resultCode) {
                    val pages = data!!.getParcelableArrayListExtra<DSPage>(Constants.EXTRA_PREVIEW_PAGES)
                    if (pages.isEmpty()) {
                        // 页面已被全部删除
                        finish()
                    } else if (mEditedPages.size != pages.size) {
                        // 如果 mEditedPages 的页数跟返回的 pages 的页数不相等时才重置适配器

                        mEditedPages = pages
                        mViewPager.adapter = EditCapturesAdapter(this)

                        setNavigationTitle()
                    }
                }
            }

            Constants.REQUEST_CODE_EDIT_POLYGON -> {
                if (Activity.RESULT_OK == resultCode) {
                    val page = data!!.getParcelableExtra<DSPage>(Constants.EXTRA_EDITED_PAGE)
                    var position = -1
                    for (i in mEditedPages.indices) {
                        val editedPage = mEditedPages[i]
                        // 如果两者的 id 相等，说明剪裁的是同一个页面
                        if (editedPage.id == page.id) {
                            position = i
                            break
                        }
                    }
                    if (position != -1) {
                        mEditedPages[position] = page
                        mViewPager.adapter?.notifyDataSetChanged()
                    }
                }
            }

            Constants.REQUEST_CODE_CAPTURE -> {
                if (Activity.RESULT_OK == resultCode) {
                    val pages = data!!.getParcelableArrayListExtra<DSPage>(Constants.EXTRA_CAPTURE_PAGES)
                    if (pages != null && pages.isNotEmpty()) {
                        // 添加图片后，跳转到新添加的第一张图片
                        val oldSize = mEditedPages.size
                        mEditedPages.addAll(pages)
                        mViewPager.adapter?.notifyDataSetChanged()
                        mViewPager.currentItem = oldSize
                    }
                }
            }
        }
    }

    /**
     * 更新导航条标题
     */
    private fun setNavigationTitle() {
        val navigationView = findViewById<NavigationView>(R.id.navigation_view)
        val title = getString(R.string.module_edit_capture_title, mViewPager.currentItem + 1, mEditedPages.size)
        navigationView.setTitle(title)
    }

    /**
     * 用户点击 Back 按钮或者最上角的返回按钮，显示对话框提醒用户。
     */
    private fun showBackPressedDialog() {
        OKCancelDialogFragment
                .newInstance(null,
                        getString(R.string.module_edit_capture_back_pressed_dialog_message),
                        getString(R.string.module_edit_capture_back_pressed_dialog_positive),
                        getString(R.string.common_cancel), FRAGMENT_DIALOG_ID_BACK_PRESSED,
                        null)
                .show(supportFragmentManager, FRAGMENT_DIALOG_TAG_BACK_PRESSED)
    }

    /**
     * 显示滤镜选择器
     */
    private fun showImageFiltersUi() {

        val popupWindow = ListPopupWindow(this)
        popupWindow.width = ViewGroup.LayoutParams.MATCH_PARENT
        popupWindow.height = ViewGroup.LayoutParams.WRAP_CONTENT
        popupWindow.anchorView = findViewById(R.id.popup_anchor)
        popupWindow.isModal = true
        popupWindow.setAdapter(mImageFiltersAdapter)
        popupWindow.setOnItemClickListener { _, _, position, _ ->
            val filter = mImageFiltersAdapter.getItem(position)
            imageFilterPage(filter.optimizationType)
            popupWindow.dismiss()
        }
        popupWindow.show()
    }

    private fun rotatePage() {
        val currentItem = mViewPager.currentItem

        val page = getItem(currentItem)
        page.setRotationType(RotationType.rotateClockwise(page.rotationTypeEnum))
        mViewPager.adapter?.notifyDataSetChanged()
    }

    @MainThread
    private fun deletePage() {
        // 统计单页删除
        RecordEvent.getInstance().recordEvent(EventParams.Builder(Event.deleteSingleEditScan).build())

        // 如果就剩一页了，直接退出编辑页面
        if (mEditedPages.size == 0) {
            finish()
        } else if (mEditedPages.size == 1) {
            val page = mEditedPages[0]
            DocumentService.deletePage(this, page)
            finish()
        } else {
            val currentItem = mViewPager.currentItem
            if (currentItem < mEditedPages.size) {
                val page = mEditedPages[currentItem]
                mEditedPages.remove(page)
                mViewPager.adapter?.notifyDataSetChanged()

                DocumentService.deletePage(this, page)
            }
        }
    }

    private fun showEditPolygonUi() {
        val currentItem = mViewPager.currentItem

        val page = getItem(currentItem)
        val intent = EditPolygonActivity.makeIntent(this, page, EditPolygonActivity.TYPE_CANCEL)
        startActivityForResult(intent, Constants.REQUEST_CODE_EDIT_POLYGON)
    }

    private fun showPreviewUi() {
        val intent = PreviewPagesActivity.makeIntent(this, mEditedPages)
        startActivityForResult(intent, Constants.REQUEST_CODE_PREVIEW_PAGES)
    }

    private fun imageFilterPage(optimizationType: OptimizationType) {
        val currentItem = mViewPager.currentItem

        val page = getItem(currentItem)
        page.setOptimizationType(optimizationType)
        mUserInfoRepository.saveImageFilter(optimizationType)
        mViewPager.adapter?.notifyDataSetChanged()
    }

    private fun getCount(): Int {
        return mEditedPages.size
    }

    private fun getItem(position: Int): DSPage {
        check(position < mEditedPages.size)
        return mEditedPages[position]
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onOKCancelMsg(okCancelMsg: OKCancelDialogFragment.OKCancelMsg) {
        when (okCancelMsg.dialogId) {
            FRAGMENT_DIALOG_ID_DELETE_PAGE -> {
                if (DialogInterface.BUTTON_POSITIVE == okCancelMsg.which) {
                    deletePage()
                }
            }
            FRAGMENT_DIALOG_ID_BACK_PRESSED -> {
                if (DialogInterface.BUTTON_POSITIVE == okCancelMsg.which) {
                    // 点击返回，说明用户放弃了这次编辑，则删除所有生成的页面
                    DocumentService.deletePages(this@EditCapturesActivity, mEditedPages)
                    finish()
                }
            }
        }
    }

    private inner class EditCapturesAdapter(private val mActivity: EditCapturesActivity) : PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun getCount(): Int {
            return mActivity.getCount()
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val imageView = PhotoView(mActivity)

            val page = mActivity.getItem(position)

            ImageLoader.getInstance().loadImage(imageView, page.path, page.rotationTypeEnum,
                    page.optimizationTypeEnum, page.polygonF, Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)

            container.addView(imageView)

            return imageView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

        override fun getItemPosition(`object`: Any): Int {
            return POSITION_NONE
        }
    }

    private class ImageFiltersAdapter constructor(context: Context, items: List<ImageFilter>) : ListAdapter<ImageFilter>(context, items, R.layout.recycler_item_image_filter) {

        override fun bindView(holder: ListAdapter.ViewHolder, value: ImageFilter) {
            holder.setText(R.id.tv_image_filter_display_name, value.displayName)
        }
    }

    private class ImageFilter constructor(val displayName: String, val optimizationType: OptimizationType)
}