package nutstore.android.docscanner.ui.documentgallery

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.target.Target
import com.github.chrisbanes.photoview.PhotoView
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.R
import nutstore.android.docscanner.data.DSDocumentResult
import nutstore.android.docscanner.data.DSPage
import nutstore.android.docscanner.event.Event
import nutstore.android.docscanner.event.EventParams
import nutstore.android.docscanner.event.RecordEvent
import nutstore.android.docscanner.task.ImageLoader
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.util.ContextUtils
import nutstore.android.docscanner.util.IntentUtils
import nutstore.android.docscanner.widget.NavigationView
import java.io.File

/**
 * ViewPager 的方式预览文档
 *
 * @author Zhu Liang
 */
class DocumentGalleryActivity : BaseActivity() {

    private lateinit var mDocumentResult: DSDocumentResult
    private lateinit var mNavigationView: NavigationView
    private var mPosition: Int = INVALID_POSITION

    companion object {

        const val INVALID_POSITION = -1

        fun makeIntent(context: Context, documentResult: DSDocumentResult, position: Int): Intent {
            checkNotNull(documentResult)
            check(position >= 0, { "Invalid position: $position" })

            val intent = Intent(context, DocumentGalleryActivity::class.java)
            intent.putExtra(Constants.EXTRA_DOCUMENT_RESULT, documentResult)
            intent.putExtra(Constants.EXTRA_POSITION, position)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_document_gallery)

        mDocumentResult = intent.getParcelableExtra(Constants.EXTRA_DOCUMENT_RESULT)

        val position = if (savedInstanceState != null) {
            savedInstanceState.getInt(Constants.EXTRA_POSITION, INVALID_POSITION)
        } else {
            intent.getIntExtra(Constants.EXTRA_POSITION, INVALID_POSITION)
        }
        check(position != INVALID_POSITION, { "Invalid position: $position" })
        mPosition = position

        mNavigationView = findViewById(R.id.navigation_view)
        mNavigationView.setTitle(getString(R.string.module_document_gallery_page_selected,
                position + 1, mDocumentResult.pages.size))

        val viewPager = findViewById<ViewPager>(R.id.view_pager_document_gallery)
        viewPager.adapter = DSPagesAdapter(this, mDocumentResult.pages)
        viewPager.currentItem = mPosition
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                mPosition = position
                mNavigationView.setTitle(getString(R.string.module_document_gallery_page_selected,
                        position + 1, mDocumentResult.pages.size))
            }
        })

        mNavigationView.setOnNavigationViewListener(object : NavigationView.SimpleOnNavigationViewListener() {
            override fun onEndClicked(view: View?) {
                // 统计分享
                RecordEvent.getInstance()
                        .recordEvent(EventParams.Builder(Event.sharePreview)
                                .addSegmentation("activity", "gallery")
                                .build())
                val intent = IntentUtils.makeShareIntent(this@DocumentGalleryActivity,
                        File(mDocumentResult.path))
                ContextUtils.startActivity(this@DocumentGalleryActivity,
                        intent, getString(R.string.common_not_found_activity_to_share))
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putInt(Constants.EXTRA_POSITION, mPosition)
    }

    private class DSPagesAdapter(private val mContext: Context, val mPages: List<DSPage>) : PagerAdapter() {

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun getCount(): Int {
            return mPages.size
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val imageView = PhotoView(mContext)
            container.addView(imageView)

            val page = mPages[position]

            ImageLoader.getInstance().loadImage(imageView, page.path, page.rotationTypeEnum,
                    page.optimizationTypeEnum, page.polygonF, Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)

            return imageView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View?)
        }
    }
}