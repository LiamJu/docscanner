package nutstore.android.docscanner.ui.base

import android.util.Log
import nutstore.android.sdk.exception.ServerException
import nutstore.android.sdk.ui.base.AbstractPresenter
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * @author Zhu Liang
 */
abstract class DSAbstractPresenter<V : DSBaseView<out DSBasePresenter>>(view: V, schedulerProvider: BaseSchedulerProvider) : AbstractPresenter<V>(view, schedulerProvider), DSBasePresenter {

    override fun handleError(throwable: Throwable) {
        if (throwable is UnknownHostException
                || throwable is ConnectException
                || throwable is SocketTimeoutException) {
            mView.showNetworkError()
        } else if (throwable is ServerException) {
            mView.showUnknownError(throwable.detailMsg)
        } else {
            mView.showUnknownError(Log.getStackTraceString(throwable))
        }
    }
}