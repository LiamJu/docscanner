package nutstore.android.docscanner.ui.common

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nutstore.android.docscanner.R

/**
 * @author Zhu Liang
 */
class ProgressDialogFragment : DialogFragment() {

    companion object {

        fun newInstance() = ProgressDialogFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (dialog != null) {
            dialog.setCanceledOnTouchOutside(false)
        }
        return inflater.inflate(R.layout.fragment_progress_dialog, container, false)
    }
}