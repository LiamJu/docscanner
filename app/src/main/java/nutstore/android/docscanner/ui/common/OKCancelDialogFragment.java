package nutstore.android.docscanner.ui.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import org.greenrobot.eventbus.EventBus;


/**
 * 显示标题、解释信息、确定按钮、取消按钮的对话框
 *
 * @author Zhu Liang
 */

public class OKCancelDialogFragment extends DialogFragment {

    private static final String KEY_TITLE = "key.TITLE";
    private static final String KEY_MESSAGE = "key.MESSAGE";
    private static final String KEY_POSITIVE_TEXT = "key.POSITIVE_TEXT";
    private static final String KEY_NEGATIVE_TEXT = "key.NEGATIVE_TEXT";
    private static final String KEY_DIALOG_ID = "key.DIALOG_ID";
    private static final String KEY_OPTIONS = "key.OPTIONS";

    private int mDialogId;
    private Bundle mOptions;

    public static OKCancelDialogFragment newInstance(String title, String message,
                                                     String positiveText, String negativeText, int dialogId, @Nullable Bundle options) {

        Bundle args = new Bundle();
        args.putString(KEY_TITLE, title);
        args.putString(KEY_MESSAGE, message);
        args.putString(KEY_POSITIVE_TEXT, positiveText);
        args.putString(KEY_NEGATIVE_TEXT, negativeText);
        args.putInt(KEY_DIALOG_ID, dialogId);
        args.putBundle(KEY_OPTIONS, options);

        OKCancelDialogFragment fragment = new OKCancelDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mDialogId = getArguments().getInt(KEY_DIALOG_ID);
            mOptions = getArguments().getBundle(KEY_OPTIONS);
        } else {
            throw new NullPointerException();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(getArguments().getString(KEY_TITLE))
                .setMessage(getArguments().getString(KEY_MESSAGE))
                .setPositiveButton(getArguments().getString(KEY_POSITIVE_TEXT), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EventBus.getDefault().post(new OKCancelMsg(which, mDialogId, mOptions));
                    }
                })
                .setNegativeButton(getArguments().getString(KEY_NEGATIVE_TEXT), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EventBus.getDefault().post(new OKCancelMsg(which, mDialogId, mOptions));
                    }
                })
                .create();
        return dialog;
    }

    public static class OKCancelMsg {
        public final int which;
        public final int dialogId;
        public final Bundle options;

        private OKCancelMsg(int which, int dialogId, Bundle options) {
            this.which = which;
            this.dialogId = dialogId;
            this.options = options;
        }
    }
}
