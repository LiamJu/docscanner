package nutstore.android.docscanner.ui.splash

import android.os.AsyncTask
import android.os.Bundle
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import nutstore.android.docscanner.R
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.ui.documents.DocumentsActivity
import java.lang.ref.WeakReference


/**
 * @author Zhu Liang
 */
class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())

        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()

        SplashTask(this).execute()
    }

    private fun showMainUi() {
        startActivity(DocumentsActivity.makeIntent(this))
        finish()
    }

    private class SplashTask(activity: SplashActivity) : AsyncTask<Void, Void, Void>() {

        private val mReference: WeakReference<SplashActivity> = WeakReference(activity)

        override fun doInBackground(vararg params: Void?): Void? {
            try {
                Thread.sleep(1000L)
            } catch (e: Exception) {

            }
            return null
        }

        override fun onPostExecute(result: Void?) {

            mReference.get()?.showMainUi()
        }
    }
}