package nutstore.android.docscanner.ui.base

import nutstore.android.sdk.ui.base.BaseView

/**
 * @author Zhu Liang
 */

interface DSBaseView<P : DSBasePresenter> : BaseView<P> {
    fun showUnknownError(message: String?)

    fun showNetworkError()
}