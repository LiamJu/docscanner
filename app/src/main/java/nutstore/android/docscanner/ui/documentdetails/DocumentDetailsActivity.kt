package nutstore.android.docscanner.ui.documentdetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.MainThread
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.R
import nutstore.android.docscanner.data.DSDocumentResult
import nutstore.android.docscanner.data.DSPage
import nutstore.android.docscanner.event.Event
import nutstore.android.docscanner.event.EventParams
import nutstore.android.docscanner.event.RecordEvent
import nutstore.android.docscanner.task.ImageLoader
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.ui.base.CommonGridAdapter
import nutstore.android.docscanner.ui.documentgallery.DocumentGalleryActivity
import nutstore.android.docscanner.util.IntentUtils
import nutstore.android.docscanner.widget.NavigationView
import java.io.File

/**
 * 网格视图的方式预览文档
 *
 * @author Zhu Liang
 */
class DocumentDetailsActivity : BaseActivity() {

    companion object {
        fun makeIntent(context: Context, documentResult: DSDocumentResult): Intent {
            checkNotNull(documentResult)
            val intent = Intent(context, DocumentDetailsActivity::class.java)
            intent.putExtra(Constants.EXTRA_DOCUMENT_RESULT, documentResult)
            return intent
        }
    }

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mDocumentResult: DSDocumentResult

    @MainThread
    private fun showGalleryUi(position: Int) {
        val intent = DocumentGalleryActivity.makeIntent(this, mDocumentResult, position)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_document_details)

        mDocumentResult = intent.getParcelableExtra<DSDocumentResult>(Constants.EXTRA_DOCUMENT_RESULT)

        mRecyclerView = findViewById(R.id.rv_document_details_content)
        val spanCount = resources.getInteger(R.integer.module_document_details_num_column)
        mRecyclerView.layoutManager = GridLayoutManager(this, spanCount)
        mRecyclerView.adapter = DSPagesAdapter(this, mDocumentResult.pages)

        val navigationView = findViewById<NavigationView>(R.id.navigation_view)
        navigationView.setTitle(mDocumentResult.document.name)

        navigationView.setOnNavigationViewListener(object : NavigationView.OnNavigationViewListener {
            override fun onStartClicked(view: View?) {
                // nothing to do
            }

            override fun onEndClicked(view: View?) {
                // 统计分享
                RecordEvent.getInstance()
                        .recordEvent(EventParams.Builder(Event.sharePreview)
                                .addSegmentation("activity", "details")
                                .build())
                val intent = IntentUtils.makeShareIntent(this@DocumentDetailsActivity,
                        File(mDocumentResult.path))
                startActivity(intent)
            }
        })
    }

    private class DSPagesAdapter(private val activity: DocumentDetailsActivity, pages: List<DSPage>) : CommonGridAdapter<DSPage>(activity, pages, R.layout.recycler_item_document_details) {
        override fun bindView(holder: ViewHolder, value: DSPage) {
            val imageView = holder.findViewById<ImageView>(R.id.iv_document_details_page_thumbnail)!!
            ImageLoader.getInstance().loadImage(imageView, value.path, value.rotationTypeEnum, value.optimizationTypeEnum, value.polygonF)

            holder.itemView.setOnClickListener({
                activity.showGalleryUi(mItems.indexOf(value))
            })
        }
    }
}