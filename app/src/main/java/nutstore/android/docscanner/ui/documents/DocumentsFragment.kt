package nutstore.android.docscanner.ui.documents

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.SystemClock
import android.support.annotation.MainThread
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.pigcasso.photopicker.findViewById
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.R
import nutstore.android.docscanner.common.DocumentByDateDescComparator
import nutstore.android.docscanner.data.DSDocumentResult
import nutstore.android.docscanner.data.DSPage
import nutstore.android.docscanner.event.Event
import nutstore.android.docscanner.event.EventParams
import nutstore.android.docscanner.event.RecordEvent
import nutstore.android.docscanner.service.CreateDocEvent
import nutstore.android.docscanner.service.DocumentService
import nutstore.android.docscanner.service.SyncFailedMsg
import nutstore.android.docscanner.task.ImageLoader
import nutstore.android.docscanner.ui.base.CommonGridAdapter
import nutstore.android.docscanner.ui.base.DSBaseFragment
import nutstore.android.docscanner.ui.capture.CaptureActivity
import nutstore.android.docscanner.ui.common.OKCancelDialogFragment
import nutstore.android.docscanner.ui.documentdetails.DocumentDetailsActivity
import nutstore.android.docscanner.ui.documents.DocumentsContract.View.Companion.MODE_CHOICE
import nutstore.android.docscanner.ui.documents.DocumentsContract.View.Companion.MODE_NONE
import nutstore.android.docscanner.ui.documents.DocumentsContract.View.Companion.MODE_NORMAL
import nutstore.android.docscanner.ui.editcapture.EditCapturesActivity
import nutstore.android.docscanner.ui.settings.SettingsActivity
import nutstore.android.docscanner.util.IntentUtils
import nutstore.android.docscanner.util.ToastUtils
import nutstore.android.docscanner.widget.NavigationView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.util.*

class DocumentsFragment : DSBaseFragment<DocumentsContract.Presenter>(), DocumentsContract.View {

    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var navigationView: NavigationView
    private lateinit var mFab: FloatingActionButton

    /**
     * 选择模式下显示删除、分享操作
     */
    private lateinit var mBottomBar: View
    private lateinit var mDeleteText: TextView
    private lateinit var mShareText: TextView

    private lateinit var mDocumentsAdapter: DocumentsAdapter

    private var mMode = DocumentsContract.View.MODE_NONE
    private lateinit var mCheckedItems: ArrayList<Int>

    /**
     * 记录上次用户点击 [NavigationView] 的时间，用来判断用户是否想快速滚动到 [RecyclerView] 的顶部
     */
    private var mLastClickTime: Long = 0

    companion object {
        const val KEY_MODE = "key.MODE"
        const val KEY_CHECKED_ITEMS = "key.CHECKED_ITEMS"

        const val FRAGMENT_DIALOG_ID_DELETE = 1

        const val FRAGMENT_TAG_DELETE = "fragment.tag.DELETE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDocumentsAdapter = DocumentsAdapter(this)

        mCheckedItems = savedInstanceState?.getIntegerArrayList(KEY_CHECKED_ITEMS) ?: ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_documents, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = findViewById(R.id.rv_documents_docs)!!
        val spanCount = resources.getInteger(R.integer.module_preview_pages_num_column)
        recyclerView.layoutManager = GridLayoutManager(context, spanCount)
        recyclerView.adapter = mDocumentsAdapter

        swipeRefreshLayout = findViewById(R.id.srl_documents_manual_upload)!!

        navigationView = findViewById(R.id.navigation_view)!!

        mFab = findViewById(R.id.fab_documents_camera)!!

        mBottomBar = findViewById(R.id.ll_documents_bottom_bar)!!

        mDeleteText = findViewById(R.id.tv_documents_delete)!!

        mShareText = findViewById(R.id.tv_documents_share)!!

        mFab.setOnClickListener({
            if (activity != null) {
                // 统计整个扫描时间，开始扫描
                RecordEvent.getInstance().startScan()

                val intent = CaptureActivity.makeIntent(activity!!)
                startActivityForResult(intent, Constants.REQUEST_CODE_CREATE_DOCUMENT)
            }
        })

        navigationView.setOnNavigationViewListener(object : NavigationView.OnNavigationViewListener {
            override fun onStartClicked(view: View?) {
                showSettingsUi()
            }

            override fun onEndClicked(view: View?) {
                when (mMode) {
                    MODE_NORMAL -> {
                        switchDocumentsMode(MODE_CHOICE)
                    }
                    MODE_CHOICE -> {
                        switchDocumentsMode(MODE_NORMAL)
                    }
                }
            }
        })

        navigationView.setOnClickListener({

            // 双击 NavigationView 滚动到 RecyclerView 的顶端
            if (SystemClock.elapsedRealtime() - mLastClickTime < 200) {
                smoothScrollToTop()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

        })

        swipeRefreshLayout.setOnRefreshListener {
            // 统计下拉刷新
            RecordEvent.getInstance()
                    .recordEvent(EventParams.Builder(Event.pullRefresh).build())
            mPresenter.syncDocuments(manual = true, results = mDocumentsAdapter.items)
        }

        mBottomBar.setOnClickListener({
            // 仅仅是为了拦截点击事件
        })

        mDeleteText.setOnClickListener({
            check(MODE_CHOICE == mMode)
            check(mCheckedItems.size > 0)

            val textId = if (mCheckedItems.size == 1) {
                R.string.module_documents_delete_doc_msg
            } else {
                R.string.module_documents_delete_docs_msg
            }

            OKCancelDialogFragment.newInstance("", getString(textId), getString(R.string.common_ok),
                    getString(R.string.common_cancel), FRAGMENT_DIALOG_ID_DELETE, null).show(fragmentManager, FRAGMENT_TAG_DELETE)
        })

        mShareText.setOnClickListener({
            check(MODE_CHOICE == mMode)
            check(mCheckedItems.size == 1)

            val item = mDocumentsAdapter.getItem(mCheckedItems[0])

            /**
             * 统计选择分享
             */
            RecordEvent.getInstance().recordEvent(EventParams.Builder(Event.shareSelect).build())

            val intent = IntentUtils.makeShareIntent(context, File(item.path))
            startActivity(intent)
        })

        val mode = savedInstanceState?.getInt(KEY_MODE, MODE_NORMAL) ?: MODE_NORMAL

        switchDocumentsMode(mode)
    }

    override fun onStart() {
        super.onStart()

        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun showSettingsUi() {
        if (context != null) {
            startActivity(SettingsActivity.makeIntent(context!!))
        }
    }

    @MainThread
    override fun showDocuments(docs: List<DSDocumentResult>) {
        if (view == null) {
            return
        }

        Collections.sort(docs, DocumentByDateDescComparator())

        recyclerView.visibility = View.VISIBLE
        val noDocsTextView = findViewById<View>(R.id.tv_documents_no_docs)!!
        noDocsTextView.visibility = View.INVISIBLE

        mDocumentsAdapter.replaceItems(docs)
    }

    override fun showNoDocuments() {
        if (view == null) {
            return
        }
        recyclerView.visibility = View.INVISIBLE
        val noDocsTextView = findViewById<View>(R.id.tv_documents_no_docs)!!
        noDocsTextView.visibility = View.VISIBLE
    }

    @MainThread
    override fun showDocumentDetails(doc: DSDocumentResult) {
        if (context != null) {
            val intent = DocumentDetailsActivity.makeIntent(context!!, doc)
            startActivity(intent)
        }
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (view == null) return

        swipeRefreshLayout.post({
            swipeRefreshLayout.isRefreshing = active
        })
    }

    override fun setLoadingIndicatorEnabled(enabled: Boolean) {
        if (view == null) return
        swipeRefreshLayout.post({
            swipeRefreshLayout.isEnabled = enabled
        })
    }

    private fun isDocumentChecked(item: DSDocumentResult): Boolean {
        check(MODE_CHOICE == mMode, { "Invalid mode: $mMode" })
        val index = mDocumentsAdapter.indexOf(item = item)
        return index != -1 && mCheckedItems.contains(index)
    }

    private fun setItemChecked(position: Int, value: Boolean) {
        check(position >= 0)
        if (value) {
            if (!mCheckedItems.contains(position)) {
                mCheckedItems.add(position)
            }
        } else {
            if (mCheckedItems.contains(position)) {
                mCheckedItems.remove(position)
            }
        }
    }

    private fun updateBottomBar() {
        check(MODE_CHOICE == mMode, { "Invalid mode: $mMode" })
        val checkedCount = mCheckedItems.size
        when (checkedCount) {
            0 -> {
                // 禁用删除、分享
                mDeleteText.isEnabled = false
                mShareText.isEnabled = false
            }
            1 -> {
                // 启用删除、分享
                mDeleteText.isEnabled = true
                mShareText.isEnabled = true
            }
            else -> {
                // 启用删除，禁用分享
                mDeleteText.isEnabled = true
                mShareText.isEnabled = false
            }
        }
    }

    private class DocumentsAdapter(private val fragment: DocumentsFragment) : CommonGridAdapter<DSDocumentResult>(fragment.context, R.layout.recycler_item_document) {

        private var mCheckboxDrawable: Drawable
        private val mCheckboxOutlineDrawable: Drawable

        init {
            val context = fragment.context!!
            var checkboxDrawable = ContextCompat.getDrawable(context, R.drawable.ic_check_box_black_24dp)
            checkboxDrawable = DrawableCompat.wrap(checkboxDrawable!!)
            DrawableCompat.setTint(checkboxDrawable!!, ContextCompat.getColor(context, R.color.accent))
            mCheckboxDrawable = checkboxDrawable

            var checkboxOutlineDrawable = ContextCompat.getDrawable(context, R.drawable.ic_check_box_outline_blank_black_24dp)
            checkboxOutlineDrawable = DrawableCompat.wrap(checkboxOutlineDrawable!!)
            DrawableCompat.setTint(checkboxOutlineDrawable!!, ContextCompat.getColor(context, R.color.gray))
            mCheckboxOutlineDrawable = checkboxOutlineDrawable
        }

        override fun bindView(holder: ViewHolder, value: DSDocumentResult) {
            val imageView = holder.findViewById<ImageView>(R.id.iv_document_thumbnail)!!
            val thumbnailPath = value.thumbnailPath

            ImageLoader.getInstance().loadImage(imageView, thumbnailPath)

            holder.itemView.setOnClickListener({
                when (fragment.mMode) {
                    MODE_NORMAL -> {
                        fragment.showDocumentDetails(value)
                    }
                    MODE_CHOICE -> {
                        if (fragment.isDocumentChecked(value)) {
                            fragment.setItemChecked(indexOf(value), false)
                            holder.setImageDrawable(R.id.iv_document_checkbox, mCheckboxOutlineDrawable)
                        } else {
                            fragment.setItemChecked(indexOf(value), true)
                            holder.setImageDrawable(R.id.iv_document_checkbox, mCheckboxDrawable)
                        }
                        fragment.updateBottomBar()
                    }
                }
            })

            if (value.synced) {
                holder.setImageResource(R.id.iv_document_sync, R.drawable.ic_sync)
            } else {
                holder.setImageResource(R.id.iv_document_sync, R.drawable.ic_not_synced)
            }

            when (fragment.mMode) {
                MODE_NORMAL -> {
                    holder.setVisibility(R.id.iv_document_checkbox, View.INVISIBLE)
                }
                MODE_CHOICE -> {
                    holder.setVisibility(R.id.iv_document_checkbox, View.VISIBLE)
                    if (fragment.isDocumentChecked(value)) {
                        holder.setImageDrawable(R.id.iv_document_checkbox, mCheckboxDrawable)
                    } else {
                        holder.setImageDrawable(R.id.iv_document_checkbox, mCheckboxOutlineDrawable)
                    }
                }
            }
        }

        fun indexOf(item: DSDocumentResult): Int {
            return mItems.indexOf(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(KEY_MODE, mMode)
        outState.putIntegerArrayList(KEY_CHECKED_ITEMS, mCheckedItems)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (Constants.REQUEST_CODE_CREATE_DOCUMENT == requestCode) {
            if (Activity.RESULT_OK == resultCode) {
                val pages = data!!.getParcelableArrayListExtra<DSPage>(Constants.EXTRA_CAPTURE_PAGES)
                showEditCapturesUi(pages)
            }
        }
    }

    @MainThread
    private fun showEditCapturesUi(pages: ArrayList<DSPage>) {
        checkNotNull(pages)
        if (context != null) {
            // 统计扫描-编辑
            RecordEvent.getInstance().startEditCaptures()
            startActivity(EditCapturesActivity.makeIntent(context!!, pages))
        }
    }

    override fun smoothScrollToTop() {
        if (view == null) {
            return
        }
        recyclerView.post {
            recyclerView.smoothScrollToPosition(0)
        }
    }

    /**
     * 切换模式
     *
     * 详情请查看：[MODE_NORMAL], [MODE_CHOICE]
     */
    @MainThread
    override fun switchDocumentsMode(mode: Int) {

        if (mMode != mode) {

            mMode = mode

            when (mode) {
                MODE_NORMAL -> {
                    setNormalMode()
                }

                MODE_CHOICE -> {
                    setChoiceMode()
                }
                MODE_NONE -> {
                    // nothing to do
                }

                else -> {
                    throw IllegalArgumentException("Invalid mode: $mode")
                }
            }
        }
    }

    /**
     * 设备没有可用网络
     */
    override fun showNetworkConnectedError() {
        ToastUtils.showShort(context, R.string.common_this_operation_needs_network)
    }

    /**
     * 设备没有连接 WiFi
     */
    override fun showWiFiConnectedError() {
        ToastUtils.showShort(context, R.string.module_documents_sync_only_wifi)
    }

    /**
     * 文档创建失败
     */
    override fun showFailedToCreateDocumentError() {
        ToastUtils.showShort(context, R.string.module_documents_failed_to_create_document)
    }

    /**
     * 切换到正常模式，允许上传，允许查看文档详情
     *
     * @see MODE_NORMAL
     * @see MODE_CHOICE
     */
    @MainThread
    private fun setNormalMode() {
        mPresenter.loadToken()

        navigationView.setEndText(getString(R.string.module_documents_choose))
        mFab.visibility = View.VISIBLE
        mBottomBar.visibility = View.GONE

        mCheckedItems.clear()

        mDocumentsAdapter.notifyDataSetChanged()
    }

    /**
     * 切换到选择模式
     *
     * 禁用上传，禁用查看文档详情
     *
     * 0. 如果用户没有选择任一文档，禁用删除和分享
     * 1. 如果用户选择了一个文档，允许用户删除和分享
     * 2. 如果用户选择了多个文档，允许用户删除，禁用分享
     *
     * @see MODE_NORMAL
     * @see MODE_CHOICE
     */
    @MainThread
    private fun setChoiceMode() {
        mPresenter.loadToken()

        navigationView.setEndText(getString(R.string.common_cancel))
        mFab.visibility = View.GONE
        mBottomBar.visibility = View.VISIBLE

        mCheckedItems.clear()

        updateBottomBar()

        mDocumentsAdapter.notifyDataSetChanged()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCreateDocEvent(event: CreateDocEvent) {
        when (event.status) {
            CreateDocEvent.STATUS_SUCCESS -> {
                mPresenter.loadDocuments()
            }
            CreateDocEvent.STATUS_FAILED -> {
                showFailedToCreateDocumentError()
            }
        }
    }

    /**
     * 同步完成的消息
     *
     * @see DocumentService
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSyncDocMsg(results: List<DSDocumentResult>) {
        showDocuments(results)
    }

    /**
     * 同步失败的消息
     *
     * @see DocumentService
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSyncFailedMsg(syncFailedMsg: SyncFailedMsg) {
        when (syncFailedMsg.errorCode) {
            SyncFailedMsg.TRAFFIC_RATE_EXHAUSTED -> {
                ToastUtils.showShort(context, getString(R.string.common_error_traffic_rate_exhausted))
            }
            SyncFailedMsg.STORAGE_SPACE_EXHAUSTED -> {
                ToastUtils.showShort(context, getString(R.string.common_error_storage_space_exhausted))
            }
            SyncFailedMsg.NO_NETWORK -> {
                showNetworkConnectedError()
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onOKCancelMsg(okCancelMsg: OKCancelDialogFragment.OKCancelMsg) {

        when (okCancelMsg.dialogId) {
            FRAGMENT_DIALOG_ID_DELETE -> {

                if (MODE_CHOICE == mMode && mCheckedItems.size > 0) {
                    // 删除被选中的选项
                    if (DialogInterface.BUTTON_POSITIVE == okCancelMsg.which) {
                        val items = ArrayList<DSDocumentResult>()
                        for (position in mCheckedItems) {
                            val item = mDocumentsAdapter.getItem(position)
                            if (item != null) {
                                items.add(item)
                            }
                        }
                        // 删除完成之后，恢复正常模式
                        switchDocumentsMode(MODE_NORMAL)

                        // 统计选择删除
                        RecordEvent.getInstance().recordEvent(EventParams.Builder(Event.deleteSelect).build())

                        mPresenter.deleteDocuments(items)
                    }
                }
            }
        }
    }
}