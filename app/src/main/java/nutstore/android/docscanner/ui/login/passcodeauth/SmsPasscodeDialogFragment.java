package nutstore.android.docscanner.ui.login.passcodeauth;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import nutstore.android.docscanner.R;

/**
 * @author Zhu Liang
 */

public class SmsPasscodeDialogFragment extends PasscodeDialogFragment {

    private static final String EXTRA_PHONE =  "fragment.extra.PHONE";
    private View mContentView;

    public static SmsPasscodeDialogFragment newInstance(@NonNull String phone) {

        Bundle args = new Bundle();
        args.putString(EXTRA_PHONE, phone);
        SmsPasscodeDialogFragment fragment = new SmsPasscodeDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        TextView phoneText = (TextView) mContentView.findViewById(R.id.text_sms_auth_phone);
        phoneText.setText(getArguments().getString(EXTRA_PHONE));

        return dialog;
    }

    @Override
    protected View getContentView() {
        mContentView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_dialog_sms_auth, null);
        return mContentView;
    }

    @Override
    protected String providerDialogTitle() {
        return getString(R.string.module_passcode_already_send_code_to_your_phone);
    }
}
