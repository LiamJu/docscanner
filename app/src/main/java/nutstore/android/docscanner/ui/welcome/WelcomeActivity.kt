package nutstore.android.docscanner.ui.welcome

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.support.v4.app.NavUtils
import android.support.v4.app.TaskStackBuilder
import nutstore.android.docscanner.Injection
import nutstore.android.docscanner.R
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.ui.common.NetworkErrorDialogFragment
import nutstore.android.docscanner.ui.common.OKCancelDialogFragment
import nutstore.android.docscanner.ui.login.passcodeauth.*
import nutstore.android.docscanner.ui.welcome.wechat.EmailFragment
import nutstore.android.docscanner.ui.welcome.wechat.PasswordFragment
import nutstore.android.docscanner.util.L
import nutstore.android.sdk.ApiManager
import nutstore.android.sdk.util.Preconditions
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*


/**
 * 欢迎页面：集成邮箱登录、微信授权登录
 *
 * @author Zhu Liang
 */
class WelcomeActivity : BaseActivity(), WelcomeContract.View, EmailFragment.OnEmailListener, PasswordFragment.OnPasswordListener, WelcomeFragment.OnWelcomeListener {

    private lateinit var mPresenter: WelcomeContract.Presenter

    private var mLastTag: String? = null

    private var mUnionId: String? = null
    private var mEmail: String? = null
    private var mPassword: String? = null
    private var mPasscodeTitle: String? = null

    companion object {

        private val TAG = WelcomeActivity::class.java.simpleName


        private const val KEY_TAG = "key.TAG"
        private const val KEY_UNION_ID = "key.UNION_ID"
        private const val KEY_EMAIL = "key.EMAIL"
        private const val KEY_PASSWORD = "key.PASSWORD"
        /**
         * 跳转到网页，执行二次验证
         */
        private const val KEY_URL = "key.URL"

        private const val TAG_WELCOME = "welcome"
        private const val TAG_WECHAT_EMAIL = "email"
        private const val TAG_WECHAT_PASSWORD = "password"

        private const val OK_CANCEL_DIALOG_ID_AUTH_FAILED = 1
        private const val OK_CANCEL_DIALOG_ID_NEED_SETUP_AUTH = 2

        private const val FRAGMENT_TAG_TWO_FACTORS_AUTH = "fragment_tag_two_factors_auth"
        private const val FRAGMENT_TAG_SETUP_TWO_FACTORS_AUTH = "fragment_tag_setup_two_factors_auth"
        private const val FRAGMENT_TAG_WE_CHAT_AUTH = "fragment_tag_we_chat_auth"
        private const val FRAGMENT_TAG_SMS_AUTH = "fragment_tag_sms_auth"
        private const val FRAGMENT_TAG_DISABLED_BY_TEAM_ADMIN = "fragment_tag_disabled_by_team_admin"
        private const val FRAGMENT_TAG_AUTH_FAILED = "fragment_tag_auth_failed"
        private const val FRAGMENT_TAG_NETWORK_ERROR = "fragment_tag_network_error"


        fun makeIntent(context: Context): Intent {
            return Intent(context, WelcomeActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_welcome)


        val tag = if (savedInstanceState == null) {
            TAG_WELCOME
        } else {
            savedInstanceState.getString(KEY_TAG)
        }

        if (savedInstanceState != null) {
            mUnionId = savedInstanceState.getString(KEY_UNION_ID)
            mEmail = savedInstanceState.getString(KEY_EMAIL)
            mPassword = savedInstanceState.getString(KEY_PASSWORD)
        }

        WelcomePresenter(this, Injection.provideSchedulerProvider(), Injection.provideNutstoreAPI(this),
                Injection.provideUserInfoRepository(this))

        show(null, tag).commit()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putString(KEY_TAG, mLastTag)
        outState?.putString(KEY_UNION_ID, mUnionId)
        outState?.putString(KEY_EMAIL, mEmail)
        outState?.putString(KEY_PASSWORD, mPassword)
    }

    override fun onResume() {
        super.onResume()

        mPresenter.subscribe()
    }

    override fun onPause() {
        super.onPause()

        mPresenter.unsubscribe()
    }

    override fun onStart() {
        super.onStart()

        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }

    override fun setPresenter(presenter: WelcomeContract.Presenter) {
        Preconditions.checkNotNull(presenter)
        mPresenter = presenter
    }

    override fun showLoginSuccessUI() {

        // 强制重置 NutstoreAPI
        ApiManager.forceReleaseNutstoreAPI()
        ApiManager.forceReleaseNutstoreUploadFileApi()

        val upIntent = NavUtils.getParentActivityIntent(this)
        if (NavUtils.shouldUpRecreateTask(this, upIntent!!)) {
            // This activity is NOT part of this app's task, so create a new task
            // when navigating up, with a synthesized back stack.
            TaskStackBuilder.create(this)
                    // Add all of this activity's parents to the back stack
                    .addNextIntentWithParentStack(upIntent)
                    // Navigate up to the closest parent
                    .startActivities()
        } else {
            // This activity is part of this app's task, so simply
            // navigate up to the logical parent activity.
            NavUtils.navigateUpTo(this, upIntent)
        }
    }

    override fun showWechatEmailUi(unionId: String) {
        mUnionId = unionId
        show(null, TAG_WECHAT_EMAIL).commit()
    }

    override fun showWechatPasswordUi() {
        show(null, TAG_WECHAT_PASSWORD).commit()
    }

    /**
     * 启用手机身份验证
     *
     * @param uri 启用手机身份验证 Url
     */
    override fun showSetUpTwoFactorsAuthUI(uri: Uri) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = uri
        startActivity(intent)
    }

    override fun showUnknownError(message: String?) {
        showToast(message)
    }

    /**
     * [EmailFragment.OnEmailListener]
     */
    override fun onEmail(email: String) {
        mEmail = email
        mPresenter.verifyEmail(email, mUnionId!!)
    }

    /**
     * [PasswordFragment.OnPasswordListener]
     */
    override fun onPassword(password: String) {
        mPresenter.bindingEmail(mEmail!!, password, mUnionId!!)
    }

    /**
     * @see [WelcomeFragment.OnWelcomeListener]
     */
    override fun onSignIn(email: String, password: String) {
        mEmail = email
        mPassword = password
        mPresenter.attemptSignIn(email, password, null)
    }

    /**
     * 用户开启了二次验证需要输入动态验证码
     *
     *
     * 参考：LoginFragment#loginFailsWithNeedTwoFactorsAuth
     */
    override fun showLoginNeedTwoFactorsAuthError() {
        TwoFactorsAuthDialogFragment.newInstance()
                .show(supportFragmentManager, FRAGMENT_TAG_TWO_FACTORS_AUTH)
    }

    /**
     * 您的账户被要求启用手机身份验证，否则无法登录。请通过网页登录账户，启用手机身份验证后重试。
     *
     *
     * 参考：LoginFragment#loginFailsWithNeedSetUpTwoFactorsAuth
     *
     * @param url 手机身份验证的网页
     */
    override fun showLoginNeedSetUpTwoFactorsAuthError(url: String) {
        val options = Bundle()
        options.putString(KEY_URL, url)
        OKCancelDialogFragment.newInstance(getString(R.string.module_welcome_need_setup_auth_title),
                getString(R.string.module_welcome_need_setup_auth_msg),
                getString(R.string.module_welcome_need_setup_auth_btn),
                null, OK_CANCEL_DIALOG_ID_NEED_SETUP_AUTH, options)
                .show(supportFragmentManager, FRAGMENT_TAG_SETUP_TWO_FACTORS_AUTH)
    }

    /**
     * 微信身份验证
     *
     * 参考：LoginFragment#loginFailsWithNeedSmsAuth
     */
    override fun showLoginNeedWechatAuthError() {
        var fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_WE_CHAT_AUTH)
        if (fragment == null) {
            fragment = PasscodeFragment()
        }
        mPasscodeTitle = getString(R.string.module_welcome_passcode_wechat_label)
        PasscodeAuthPresenter(fragment as PasscodeFragment, mPasscodeTitle.toString())
        supportFragmentManager.beginTransaction().replace(R.id.contentFrame, fragment, FRAGMENT_TAG_WE_CHAT_AUTH)
                .addToBackStack(null)
                .commit()
    }

    /**
     * 手机身份验证
     *
     *
     * 参考：LoginFragment#loginFailsWithNeedSmsAuth
     *
     * @param phone 手机号
     */
    override fun showLoginNeedSmsAuthError(phone: String) {
        var fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_SMS_AUTH)
        if (fragment == null) {
            fragment = PasscodeFragment()
        }
        mPasscodeTitle = getString(R.string.module_welcome_passcode_sms_label, phone)
        PasscodeAuthPresenter(fragment as PasscodeFragment, mPasscodeTitle.toString())
        supportFragmentManager.beginTransaction().replace(R.id.contentFrame, fragment, FRAGMENT_TAG_SMS_AUTH)
                .addToBackStack(null)
                .commit()
    }

    /**
     * 验证错误，请确认手机时间与当前时间（%1$s）误差小于十分钟
     *
     *
     * 参考：LoginFragment#loginFailsWithTwoFactorAuthFailed
     */
    override fun showLoginTwoFactorAuthError(originPayload: String) {
        val time = java.lang.Long.parseLong(originPayload)
        val sdf = SimpleDateFormat("HH:mm",
                Locale.getDefault())
        val srvTime = sdf.format(Date(time))
        AlertDialog.Builder(this)
                .setTitle(R.string.module_welcome_login_failed_title)
                .setMessage(String.format(getString(R.string.module_welcome_two_factor_passcode_invalid_text), srvTime))
                .setPositiveButton(R.string.common_ok, { _, _ -> showLoginNeedTwoFactorsAuthError() }).show()
    }

    /**
     * 你的账号已被管理员禁用，如需启用，请联系您的管理员
     *
     *
     * 参考：LoginFragment#loginFailsWithDisabledByTeamAdmin
     */
    override fun showLoginDisabledByTeamAdminError() {
        OKCancelDialogFragment.newInstance(getString(R.string.module_welcome_login_failed_title),
                getString(R.string.module_welcome_login_failed_disabled_by_team_admin),
                getString(R.string.common_ok), null, -1, null)
                .show(supportFragmentManager, FRAGMENT_TAG_DISABLED_BY_TEAM_ADMIN)
    }

    /**
     * 账户或密码有误，请重新输入\r\n如果您是企业版用户，请点击“登录企业版”
     *
     *
     * 参考：LoginFragment#loginFailsWithAuthFailed
     */
    override fun showLoginAuthFailedError() {
        OKCancelDialogFragment.newInstance(
                getString(R.string.module_welcome_login_failed_title),
                getString(R.string.module_welcome_login_failed_text),
                getString(R.string.module_welcome_retype_username_or_password),
                null,
                OK_CANCEL_DIALOG_ID_AUTH_FAILED, null)
                .show(supportFragmentManager, FRAGMENT_TAG_AUTH_FAILED)
    }

    /**
     * 显示网络异常
     */
    override fun showNetworkError() {
        NetworkErrorDialogFragment.newInstance().show(supportFragmentManager, FRAGMENT_TAG_NETWORK_ERROR)
    }

    /**
     * 请求短信的次数过多
     */
    override fun showRequestSmsTooManyError() {
        showToast(R.string.module_welcome_resend_blocked_msg)
    }

    private fun show(fragmentTransaction: FragmentTransaction?, tag: String): FragmentTransaction {

        val ft = fragmentTransaction ?: supportFragmentManager.beginTransaction()

        if (mLastTag != tag) {

            if (mLastTag != null) {
                val fragment = supportFragmentManager.findFragmentByTag(mLastTag)
                if (fragment != null) {
                    ft.hide(fragment)
                }
            }

            var fragment = supportFragmentManager.findFragmentByTag(tag)
            if (fragment == null) {
                fragment = when (tag) {
                    TAG_WELCOME -> {
                        WelcomeFragment()
                    }
                    TAG_WECHAT_EMAIL -> {
                        EmailFragment()
                    }
                    TAG_WECHAT_PASSWORD -> {
                        PasswordFragment()
                    }
                    else -> {
                        throw IllegalArgumentException("Invalid tag: $tag")
                    }
                }
                ft.add(R.id.contentFrame, fragment, tag)
            } else {
                ft.show(fragment)
            }
            mLastTag = tag
        }
        return ft
    }

    /**
     * 接收微信授权返回的 code
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onAuthCode(code: String) {
        L.d(TAG, "onAuthCode: $code")
        mPresenter.verifyCode(code)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSendPasscodeMsg(sendPasscodeMsg: SendPasscodeMsg) {
        mPresenter.attemptSendTfSms(mEmail.toString(), mPassword.toString())
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onPasscodeMsg(passcodeMsg: PasscodeMsg) {
        mPresenter.attemptSignIn(mEmail.toString(), mPassword.toString(), passcodeMsg.getPasscode())
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onOKCancelMsg(okCancelMsg: OKCancelDialogFragment.OKCancelMsg) {
        when (okCancelMsg.dialogId) {
            OK_CANCEL_DIALOG_ID_NEED_SETUP_AUTH -> {
                if (DialogInterface.BUTTON_POSITIVE == okCancelMsg.which) {
                    showSetUpTwoFactorsAuthUI(Uri.parse(okCancelMsg.options!!.getString(KEY_URL)))
                }
            }
        }
    }
}