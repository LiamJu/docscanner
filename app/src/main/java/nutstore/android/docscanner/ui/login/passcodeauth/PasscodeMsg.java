package nutstore.android.docscanner.ui.login.passcodeauth;

public class PasscodeMsg {
    private final String passcode;

    PasscodeMsg(String passcode) {
        this.passcode = passcode;
    }

    public String getPasscode() {
        return passcode;
    }
}