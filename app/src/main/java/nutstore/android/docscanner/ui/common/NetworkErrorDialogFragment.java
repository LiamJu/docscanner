package nutstore.android.docscanner.ui.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import nutstore.android.docscanner.R;
import nutstore.android.sdk.util.NetworkUtils;

/**
 * @author Zhu Liang
 */

public class NetworkErrorDialogFragment extends DialogFragment {

    public static NetworkErrorDialogFragment newInstance() {

        Bundle args = new Bundle();

        NetworkErrorDialogFragment fragment = new NetworkErrorDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (NetworkUtils.isWifiConnected()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.common_connection_error);
            builder.setMessage(R.string.common_operation_failed_can_not_connect_to_server);
            builder.setPositiveButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            return builder.create();
        } else {
            //Pop up an alert window telling user that the network is not available,
            //and let user to set his wireless network by the way.
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.common_wireless_network_setting_wrong);
            //Lead user to wireless setting page
            builder.setMessage(R.string.common_this_operation_needs_network);
            builder.setPositiveButton(R.string.common_setting, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    NetworkUtils.openWirelessSettings();
                }
            });
            builder.setNegativeButton(R.string.common_cancel, null);
            return builder.create();
        }
    }
}
