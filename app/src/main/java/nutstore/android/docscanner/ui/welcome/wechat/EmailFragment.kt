package nutstore.android.docscanner.ui.welcome.wechat

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.pigcasso.photopicker.findViewById
import nutstore.android.docscanner.R

/**
 * 微信授权-输入邮箱
 *
 * @author Zhu Liang
 */
class EmailFragment : Fragment() {

    private var mOnEmailListener: OnEmailListener? = null

    companion object {
        fun newInstance() = EmailFragment()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnEmailListener) {
            mOnEmailListener = context
        } else {
            throw ClassCastException()
        }
    }

    override fun onDetach() {
        super.onDetach()

        mOnEmailListener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_wechat_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findViewById<View>(R.id.btn_wechat_email_submit)!!.setOnClickListener({
            val textInputLayout = findViewById<TextInputLayout>(R.id.inputlayout_wechat_email)
            val editText = textInputLayout!!.editText!!
            if (mOnEmailListener != null) {
                mOnEmailListener!!.onEmail(editText.text.toString())
            }
        })
    }

    interface OnEmailListener {
        fun onEmail(email: String)
    }
}