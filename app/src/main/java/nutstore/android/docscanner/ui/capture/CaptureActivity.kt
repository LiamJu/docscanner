package nutstore.android.docscanner.ui.capture

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import android.widget.Toast
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.disposables.CompositeDisposable
import net.doo.snap.ScanbotSDK
import net.doo.snap.camera.AutoSnappingController
import net.doo.snap.camera.ContourDetectorFrameHandler
import net.doo.snap.camera.PictureCallback
import net.doo.snap.camera.ScanbotCameraView
import net.doo.snap.lib.detector.DetectionResult
import net.doo.snap.persistence.PageFactory
import net.doo.snap.persistence.PageStoreStrategy
import net.doo.snap.ui.PolygonView
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.Injection
import nutstore.android.docscanner.R
import nutstore.android.docscanner.data.DSPage
import nutstore.android.docscanner.data.UserInfoRepository
import nutstore.android.docscanner.event.Event
import nutstore.android.docscanner.event.EventParams
import nutstore.android.docscanner.event.RecordEvent
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.ui.common.ProgressDialogFragment
import nutstore.android.docscanner.ui.editpolygon.EditPolygonActivity
import nutstore.android.docscanner.ui.photopicker.MyPhotoPickerActivity
import nutstore.android.docscanner.ui.photopicker.MyPhotoPickerActivity.Companion.EXTRA_RESULT_SELECTION
import nutstore.android.docscanner.util.EasyPermissionsHelper
import nutstore.android.docscanner.util.L
import nutstore.android.docscanner.util.PageFactoryHelper
import nutstore.android.docscanner.widget.NavigationView
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider
import pub.devrel.easypermissions.EasyPermissions
import java.io.File

/**
 * 扫描页面：自动扫描、导入图片、开关闪光灯、关闭当前页面
 */
class CaptureActivity : BaseActivity(), PictureCallback, ContourDetectorFrameHandler.ResultHandler, EasyPermissions.PermissionCallbacks {

    private lateinit var cameraView: ScanbotCameraView
    private lateinit var navigationView: NavigationView

    private var mFlashEnabled = false
    private var mManualSnap = false

    private var mCameraEnabled = true

    private lateinit var mDisposable: CompositeDisposable
    private lateinit var mSchedulerProvider: BaseSchedulerProvider

    private lateinit var mPageFactory: PageFactory
    private lateinit var mPageStoreStrategy: PageStoreStrategy

    private lateinit var userGuidanceToast: Toast

    private lateinit var mUserInfoRepository: UserInfoRepository

    companion object {

        private val TAG = CaptureActivity::class.java.simpleName

        const val FRAGMENT_TAG_PROGRESS = "fragment.tag.PROGRESS"
        fun makeIntent(context: Context) = Intent(context, CaptureActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_capture)

        mUserInfoRepository = Injection.provideUserInfoRepository(this)

        userGuidanceToast = Toast.makeText(this, "", Toast.LENGTH_SHORT)
        userGuidanceToast.setGravity(Gravity.CENTER, 0, 0)

        mDisposable = CompositeDisposable()
        mSchedulerProvider = Injection.provideSchedulerProvider()

        val scanbotSDK = ScanbotSDK(this)
        mPageFactory = scanbotSDK.pageFactory()
        mPageStoreStrategy = PageStoreStrategy(this.application)

        cameraView = findViewById(R.id.scanbot_camera_capture_camera)!!
        cameraView.continuousFocus()
        val polygonView = findViewById<PolygonView>(R.id.polygon_capture_polygon)
        navigationView = findViewById(R.id.navigation_view)!!


        val contourDetectorFrameHandler = ContourDetectorFrameHandler.attach(cameraView)
        contourDetectorFrameHandler.addResultHandler(polygonView) // optional
        contourDetectorFrameHandler.addResultHandler(this)
        contourDetectorFrameHandler.setAcceptedAngleScore(50.0)
        AutoSnappingController.attach(cameraView, contourDetectorFrameHandler)

        mFlashEnabled = mUserInfoRepository.isUseFlash
        setFlashEnabled(mFlashEnabled)

        cameraView.setCameraOpenCallback {
            cameraView.postDelayed({
                cameraView.continuousFocus()

                // disable auto-focus event system
                cameraView.setAutoFocusSound(false)
                // 禁用快门声音
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    cameraView.setShutterSound(false)
                }
                // 开关闪光灯
                setFlashEnabled(mFlashEnabled)
            }, Constants.CAPTURE_AUTO_SNAPPING_DELAY_MILLIS)
        }

        cameraView.addPictureCallback(this)

        navigationView.setOnNavigationViewListener(object : NavigationView.SimpleOnNavigationViewListener() {
            override fun onEndClicked(view: View?) {
                super.onEndClicked(view)

                setFlashEnabled(!mFlashEnabled)
            }
        })

        findViewById<View>(R.id.iv_capture_snap).setOnClickListener({
            // 统计扫描-手动扫描
            RecordEvent.getInstance().recordEvent(EventParams.Builder(Event.manualScan).build())
            mManualSnap = true
            cameraView.takePicture(false)
        })

        findViewById<View>(R.id.iv_capture_photo_picker).setOnClickListener({
            showPhotoPickerUi()
        })

        findViewById<View>(R.id.iv_capture_close).setOnClickListener({
            finish()
        })

        if (!EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
            EasyPermissionsHelper.requestCameraPermissions(this)
        }
    }

    override fun onResume() {
        super.onResume()
        if (mCameraEnabled) {
            cameraView.onResume()
        }
    }

    override fun onPause() {
        super.onPause()
        mDisposable.clear()
        if (mCameraEnabled) {
            cameraView.onPause()
        }

        dismissProgressDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Constants.REQUEST_CODE_EDIT_POLYGON -> {
                if (Activity.RESULT_OK == resultCode) {
                    val editedPage: DSPage = data!!.getParcelableExtra(Constants.EXTRA_EDITED_PAGE)
                    showEditCaptures(arrayListOf(editedPage))
                }
            }
            Constants.REQUEST_CODE_PHOTO_PICKER -> {
                if (Activity.RESULT_OK == resultCode) {
                    val result: List<String> = data!!.getStringArrayListExtra(EXTRA_RESULT_SELECTION)
                    // 统计导入-选择的页数统计
                    RecordEvent.getInstance()
                            .recordEvent(EventParams
                                    .Builder(Event.pagesNumberImportScan)
                                    .addSegmentation("number", result.size.toString()) // 用户选择的相片数
                                    .build())
                    if (!result.isEmpty()) {
                        // 选择图片后，需要将图片转成DSPage，这是一个耗时操作，由于Camera绑定了 onResume 和 onPause
                        // 声明周期，如果不禁用相机，图片转DSPage的时候，有可能会扫描成功，为了避免这个问题，在onResume和onPause里做了判断
                        mCameraEnabled = false
                        convertToDSPages(result)
                    }
                }
            }
        }
    }

    override fun handleResult(detectedFrame: ContourDetectorFrameHandler.DetectedFrame): Boolean {
        // Here you are continuously notified about contour detection results.
        // For example, you can show a user guidance text depending on the current detection status.
        this.runOnUiThread { showUserGuidance(detectedFrame.detectionResult) }

        return false // typically you need to return false
    }

    override fun onPictureTaken(bytes: ByteArray, imageOrientation: Int) {
        convertToDSPage(bytes, imageOrientation)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {

    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        EasyPermissionsHelper.showCameraDeniedDialog(this)
    }

    private fun setFlashEnabled(enabled: Boolean) {
        mFlashEnabled = enabled
        mUserInfoRepository.saveUseFlash(enabled)
        val resId = if (enabled) {
            R.drawable.ic_flash_off_black_24dp
        } else {
            R.drawable.ic_flash_on_black_24dp
        }
        navigationView.setEndDrawable(ContextCompat.getDrawable(this, resId))
        cameraView.useFlash(enabled)
    }

    private fun showUserGuidance(result: DetectionResult) {
        // 在弹出 Toast 的时候偶尔会抛出如下异常：Unable to add window -- token android.os.BinderProxy@ce8f6c0 is not valid; is your activity running?
        // 参考：http://stackoverflow.com/questions/7811993/error-binderproxy45d459c0-is-not-valid-is-your-activity-running
        if (!isFinishing) {
            when (result) {
                DetectionResult.OK -> {
                    userGuidanceToast.setText(R.string.module_capture_hint_do_not_move)
                    userGuidanceToast.show()
                }
                DetectionResult.OK_BUT_TOO_SMALL -> {
                    userGuidanceToast.setText(R.string.module_capture_hint_move_closer)
                    userGuidanceToast.show()
                }
                DetectionResult.OK_BUT_BAD_ANGLES -> {
                    userGuidanceToast.setText(R.string.module_capture_hint_bad_angles)
                    userGuidanceToast.show()
                }
                DetectionResult.ERROR_NOTHING_DETECTED -> {
                    userGuidanceToast.setText(R.string.module_capture_hint_nothing_detected)
                    userGuidanceToast.show()
                }
                DetectionResult.ERROR_TOO_NOISY -> {
                    userGuidanceToast.setText(R.string.module_capture_hint_too_noisy)
                    userGuidanceToast.show()
                }
                else -> {
                    userGuidanceToast.cancel()
                }
            }
        }
    }

    private fun convertToDSPage(bytes: ByteArray, imageOrientation: Int) {
        mDisposable.clear()

        val disposable = Flowable
                .create(({ emitter: FlowableEmitter<DSPage> ->

                    try {
                        val dm = resources.displayMetrics
                        val dsPage = PageFactoryHelper.buildDSPage(mPageStoreStrategy, bytes,
                                imageOrientation, dm.widthPixels, dm.heightPixels, !mManualSnap,
                                mUserInfoRepository.imageFilter)
                        emitter.onNext(dsPage)
                        emitter.onComplete()
                    } catch (e: Exception) {
                        emitter.onError(e)
                    }
                }), BackpressureStrategy.LATEST)
                .doOnSubscribe {
                    showProgressDialog()
                }
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            if (mManualSnap) {
                                mManualSnap = false
                                showEditPolygonUi(dsPage = it)
                            } else {
                                showEditCaptures(arrayListOf(it))
                            }
                        },
                        {
                            dismissProgressDialog()
                            L.e(TAG, "convertToPage: ", it)
                            handleError(it)
                        },
                        {
                            dismissProgressDialog()
                        })

        mDisposable.add(disposable)
    }

    private fun convertToDSPages(paths: List<String>) {
        mDisposable.clear()

        val disposable = Flowable
                .create({ emitter: FlowableEmitter<ArrayList<DSPage>> ->

                    try {
                        val dm = resources.displayMetrics
                        val width = dm.widthPixels
                        val height = dm.heightPixels
                        val pages = ArrayList<DSPage>()
                        for (pathname in paths) {
                            val file = File(pathname)
                            if (file.isFile && file.exists()) {
                                val dsPage = PageFactoryHelper.buildDSPage(mPageStoreStrategy, pathname, width, height, false, mUserInfoRepository.imageFilter)
                                pages.add(dsPage)
                            }
                        }

                        emitter.onNext(pages)
                        emitter.onComplete()
                    } catch (e: Exception) {
                        emitter.onError(e)
                    }

                }, BackpressureStrategy.LATEST)
                .doOnSubscribe {
                    showProgressDialog()
                }
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            showEditCaptures(it)
                        },
                        {
                            dismissProgressDialog()
                            L.e(TAG, "convertToPage: ", it)
                            handleError(it)
                        },
                        {
                            dismissProgressDialog()
                        })

        mDisposable.add(disposable)

    }

    private fun showEditPolygonUi(dsPage: DSPage) {
        startActivityForResult(EditPolygonActivity.makeIntent(this, dsPage, EditPolygonActivity.TYPE_RETAKE),
                Constants.REQUEST_CODE_EDIT_POLYGON)
    }

    private fun showEditCaptures(pages: ArrayList<DSPage>) {
        val intent = Intent()
        intent.putParcelableArrayListExtra(Constants.EXTRA_CAPTURE_PAGES, pages)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun showPhotoPickerUi() {
        val intent = MyPhotoPickerActivity.Companion.makeIntent(this)
        startActivityForResult(intent, Constants.REQUEST_CODE_PHOTO_PICKER)
    }

    private fun showProgressDialog() {
        ProgressDialogFragment.newInstance().show(supportFragmentManager, FRAGMENT_TAG_PROGRESS)
    }

    private fun dismissProgressDialog() {
        val f = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_PROGRESS)
        if (f != null && f is DialogFragment) {
            f.dismiss()
        }
    }

    /**
     * 处理异常
     */
    private fun handleError(throwable: Throwable) {
        if (throwable.message != null) {
            showToast(throwable.message!!)
        } else {
            showToast(L.getStackTraceString(throwable))
        }
        // TODO("可能出现存储空间不足的情况")
    }
}