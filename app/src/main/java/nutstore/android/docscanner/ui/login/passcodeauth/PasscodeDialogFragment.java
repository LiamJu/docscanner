package nutstore.android.docscanner.ui.login.passcodeauth;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import nutstore.android.docscanner.R;
import nutstore.android.docscanner.util.L;
import nutstore.android.sdk.ui.base.BaseDialogFragment;

/**
 * 输入验证码对话框
 *
 * @author Zhu Liang
 */

public abstract class PasscodeDialogFragment extends BaseDialogFragment<PasscodeAuthContract.Presenter>
        implements PasscodeAuthContract.View, View.OnClickListener {

    private static final String TAG = "CountDownDialogFragment";
    private static final int BUTTON_AUTHENTICATE = android.R.id.button1;
    private static final int BUTTON_RESEND = android.R.id.button2;

    private EditText mPasscodeEdit;
    private Button mResendButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new PasscodeAuthPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.recyclerCountDownTimer();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        View contentView = getContentView();

        Button authenticateButton = (Button) contentView.findViewById(BUTTON_AUTHENTICATE);
        authenticateButton.setOnClickListener(this);
        authenticateButton.setText(R.string.module_passcode_authenticate);
        mResendButton = (Button) contentView.findViewById(BUTTON_RESEND);
        mResendButton.setOnClickListener(this);
        mPasscodeEdit = (EditText) contentView.findViewById(R.id.edit_passcode);

        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setView(contentView)
                .setTitle(providerDialogTitle());

        final AlertDialog alertDialog = builder.create();
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        return alertDialog;
    }

    protected abstract View getContentView();

    protected abstract String providerDialogTitle();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case BUTTON_AUTHENTICATE:
                mPresenter.validatePasscode(mPasscodeEdit.getText().toString());
                break;
            case BUTTON_RESEND:
                mPresenter.restartCountDownTimer();
                break;
        }
    }

    @Override
    public void setResendUi(boolean enabled) {
        if (mResendButton != null) {
            mResendButton.setEnabled(enabled);
            if (enabled) {
                mResendButton.setText(R.string.module_passcode_resend);
            } else {
                // 当"重新发送"按钮不可点时，发送消息让接收者去发送验证码
                EventBus.getDefault().post(new SendPasscodeMsg());
            }
        }
    }

    /**
     * 更新剩余秒数
     *
     * @param leftSeconds 剩余秒数
     */
    @Override
    public void setLeftSecondsUi(long leftSeconds) {
        L.d(TAG, "setLeftSecondsUi: " + leftSeconds);
        if (mResendButton != null) {
            mResendButton.setText(getString(R.string.module_passcode_resend_in_seconds, leftSeconds));
        }
    }

    /**
     * 显示有效的验证码
     *
     * @param passcode 验证码
     */
    @Override
    public void showValidPasscodeUi(String passcode) {
        EventBus.getDefault().post(new PasscodeMsg(passcode));
    }

    @Override
    public void showInvalidPasscodeUi() {
        if (getContext() != null) {
            Toast.makeText(getContext(), R.string.module_passcode_should_be_six_numbers, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setPasscodeTitle(String title) {
        // nothing to do
    }
}
