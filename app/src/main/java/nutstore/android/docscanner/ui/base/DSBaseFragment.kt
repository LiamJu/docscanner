package nutstore.android.docscanner.ui.base

import nutstore.android.docscanner.R
import nutstore.android.docscanner.util.ToastUtils
import nutstore.android.sdk.ui.base.BaseFragment

/**
 * @author Zhu Liang
 */
open class DSBaseFragment<P : DSBasePresenter> : BaseFragment<P>(), DSBaseView<P> {
    override fun showNetworkError() {
        ToastUtils.showShort(context, getString(R.string.common_connection_error))
    }

    override fun showUnknownError(message: String?) {
        ToastUtils.showShort(context, message)
    }
}