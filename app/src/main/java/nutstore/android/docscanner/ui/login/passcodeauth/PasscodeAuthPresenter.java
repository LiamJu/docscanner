package nutstore.android.docscanner.ui.login.passcodeauth;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import nutstore.android.docscanner.util.Validator;
import nutstore.android.sdk.util.StringUtils;

/**
 * @author Zhu Liang
 */

public class PasscodeAuthPresenter implements PasscodeAuthContract.Presenter {

    @NonNull
    private final PasscodeAuthContract.View mView;
    private final String mTitle;
    private boolean mIsFirst = true;
    private CountDownTimer mCountDownTimer;

    public PasscodeAuthPresenter(@NonNull PasscodeAuthContract.View view) {
        this(view, null);
    }

    public PasscodeAuthPresenter(@NonNull PasscodeAuthContract.View view, String title) {
        mView = view;
        mTitle = title;

        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (mIsFirst) {
            mView.setResendUi(false);
            initOrStartCountDownTimer();
            mIsFirst = false;
        }

        if (mTitle != null) {
            mView.setPasscodeTitle(mTitle);
        }
    }

    @Override
    public void unsubscribe() {
    }

    @Override
    public void validatePasscode(@NonNull String passcode) {
        if (Validator.isPasscode(passcode)) {
            mView.showValidPasscodeUi(passcode);
            mView.dismissDialog();
        } else {
            mView.showInvalidPasscodeUi();
        }
    }

    /**
     * 重新开始倒计时
     */
    @Override
    public void restartCountDownTimer() {
        mView.setResendUi(false);
        recyclerCountDownTimer();
        initOrStartCountDownTimer();
    }

    private void initOrStartCountDownTimer() {
        if (mCountDownTimer == null) {
            mCountDownTimer = new CountDownTimer(TimeUnit.MINUTES.toMillis(1), TimeUnit.SECONDS.toMillis(1)) {
                @Override
                public void onTick(long millisUntilFinished) {
                    final long leftSeconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);
                    mView.setLeftSecondsUi(leftSeconds);
                }

                @Override
                public void onFinish() {
                    mView.setResendUi(true);
                }
            };
        }
        mCountDownTimer.start();
    }

    @Override
    public void recyclerCountDownTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }
}
