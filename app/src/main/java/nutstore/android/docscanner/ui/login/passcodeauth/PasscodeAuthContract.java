package nutstore.android.docscanner.ui.login.passcodeauth;

import android.support.annotation.NonNull;

import nutstore.android.sdk.ui.base.BaseDialogView;
import nutstore.android.sdk.ui.base.BasePresenter;

/**
 * @author Zhu Liang
 */

interface PasscodeAuthContract {

    interface View extends BaseDialogView<Presenter> {

        /**
         * 显示重新发送
         */
        void setResendUi(boolean enabled);

        /**
         * 更新剩余秒数
         *
         * @param leftSeconds 剩余秒数
         */
        void setLeftSecondsUi(long leftSeconds);

        /**
         * 显示有效的验证码
         *
         * @param passcode 验证码
         */
        void showValidPasscodeUi(String passcode);

        void showInvalidPasscodeUi();

        void setPasscodeTitle(String title);
    }

    interface Presenter extends BasePresenter {
        void validatePasscode(@NonNull String passcode);

        /**
         * 重新开始倒计时
         */
        void restartCountDownTimer();

        void recyclerCountDownTimer();
    }
}
