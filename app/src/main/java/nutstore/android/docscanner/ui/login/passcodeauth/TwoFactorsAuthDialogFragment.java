package nutstore.android.docscanner.ui.login.passcodeauth;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.R;
import nutstore.android.sdk.ui.base.BaseDialogFragment;

/**
 * @author Zhu Liang
 */

public class TwoFactorsAuthDialogFragment extends BaseDialogFragment<TwoFactorsContract.Presenter>
        implements TwoFactorsContract.View {

    public static TwoFactorsAuthDialogFragment newInstance() {
        return new TwoFactorsAuthDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new TwoFactorsPresenter(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context context = getContext();
        View contentView = LayoutInflater.from(context).inflate(R.layout.fragment_two_factors_auth, null);
        TextView lostAuthenticatorText = (TextView) contentView.findViewById(R.id.text_two_factors_auth_lost_authenticator);
        lostAuthenticatorText.setMovementMethod(LinkMovementMethod.getInstance());
        lostAuthenticatorText.setText(Html.fromHtml(String.format(getString(
                R.string.module_two_factors_auth_lost_authenticator),
                Constants.URL_RESET_PASSWORD)));
        final EditText passcodeEdit = (EditText) contentView.findViewById(R.id.edit_two_factors_auth_passcode);
        passcodeEdit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_DOWN) {
                    mPresenter.validatePasscode(passcodeEdit.getText().toString());
                }
                return false;
            }
        });
        Button startAuthenticatorButton = (Button) contentView.findViewById(android.R.id.button2);
        startAuthenticatorButton.setText(R.string.module_two_factors_auth_start_authenticator);
        Button authenticateButton = (Button) contentView.findViewById(android.R.id.button1);
        authenticateButton.setText(R.string.module_two_factors_auth_authenticate);
        startAuthenticatorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAuthenticatorUi();
            }
        });
        authenticateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.validatePasscode(passcodeEdit.getText().toString());
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), getTheme());
        builder.setView(contentView)
                .setTitle(R.string.module_two_factors_auth_please_enter_dynamic_passocde);
        Dialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        return dialog;

    }

    @Override
    public void showAuthenticatorUi() {
        //Try GA2 first
        Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.google.android.apps.authenticator2");
        if (intent == null) {
            intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.google.android.apps.authenticator");
        }

        if (intent != null) {
            startActivity(intent);
        } else {
            if (getContext() != null) {
                Toast.makeText(getContext(), R.string.module_two_factors_auth_google_authenticator_not_found, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void showInvalidPasscodeError() {
        if (getContext() != null) {
            Toast.makeText(getContext(), R.string.module_two_factors_auth_should_be_six_numbers, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showValidPasscodeUi(String passcode) {
        EventBus.getDefault().post(new PasscodeMsg(passcode));
    }
}
