package nutstore.android.docscanner.ui.login.passcodeauth;

import android.support.annotation.NonNull;

import nutstore.android.sdk.ui.base.BaseDialogView;
import nutstore.android.sdk.ui.base.BasePresenter;

/**
 * @author Zhu Liang
 */

interface TwoFactorsContract {
    interface View extends BaseDialogView<Presenter> {
        void showAuthenticatorUi();

        void showInvalidPasscodeError();

        void showValidPasscodeUi(String passcode);
    }

    interface Presenter extends BasePresenter {

        void validatePasscode(@NonNull String passcode);
    }
}
