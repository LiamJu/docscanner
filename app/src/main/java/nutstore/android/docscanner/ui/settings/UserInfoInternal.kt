package nutstore.android.docscanner.ui.settings

import nutstore.android.sdk.module.UserInfo

/**
 * @author Zhu Liang
 */
class UserInfoInternal(val username: String?, val autoUpload: Boolean, val onlyWiFi: Boolean,
                       val userInfo: UserInfo?) {

    constructor() : this(userInfo = null, autoUpload = false, onlyWiFi = false, username = null)
}