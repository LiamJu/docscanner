package nutstore.android.docscanner.ui.base

import nutstore.android.sdk.ui.base.BasePresenter

/**
 * @author Zhu Liang
 */
interface DSBasePresenter : BasePresenter {

    fun handleError(throwable: Throwable)
}