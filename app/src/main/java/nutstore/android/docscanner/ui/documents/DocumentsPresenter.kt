package nutstore.android.docscanner.ui.documents

import android.support.annotation.MainThread
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.functions.BiFunction
import nutstore.android.docscanner.data.DSDocumentResult
import nutstore.android.docscanner.data.DocScannerRepository
import nutstore.android.docscanner.data.NutstoreRepository
import nutstore.android.docscanner.data.UserInfoRepository
import nutstore.android.docscanner.service.DocumentService
import nutstore.android.docscanner.ui.base.DSAbstractPresenter
import nutstore.android.sdk.module.Metadata
import nutstore.android.sdk.util.MetadataUtils
import nutstore.android.sdk.util.NetworkUtils
import nutstore.android.sdk.util.StringUtils
import nutstore.android.sdk.util.Utils
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider

class DocumentsPresenter(view: DocumentsContract.View, schedulerProvider: BaseSchedulerProvider,
                         private val mDocScannerRepository: DocScannerRepository,
                         private val mUserInfoRepository: UserInfoRepository,
                         private val mNutstoreRepository: NutstoreRepository) : DSAbstractPresenter<DocumentsContract.View>(view, schedulerProvider), DocumentsContract.Presenter {

    override fun subscribe() {
        loadToken()

        loadDocuments()
    }

    @MainThread
    override fun loadToken() {
        if (StringUtils.isEmpty(mUserInfoRepository.token)) {
            // 如果没有登录，不能下拉刷新
            mView.setLoadingIndicatorEnabled(false)
        } else {
            // 如果已经登录，允许下拉刷新
            mView.setLoadingIndicatorEnabled(true)
        }
    }

    override fun loadDocuments() {
        loadDocumentsFromLocal()
    }

    override fun syncDocuments(manual: Boolean, results: List<DSDocumentResult>) {
        if (mUserInfoRepository.isOnlyWiFi) {
            // 如果仅用 WiFi 上传，必须连接 WiFi
            if (NetworkUtils.isWifiConnected()) {
                DocumentService.syncDocuments(Utils.getApp(), results)
            } else {
                mView.showWiFiConnectedError()
            }
        } else {
            if (NetworkUtils.isConnected()) {
                DocumentService.syncDocuments(Utils.getApp(), results)
            } else {
                mView.showNetworkConnectedError()
            }
        }

        if (manual) {
            mView.setLoadingIndicator(false)
        }
    }

    override fun deleteDocuments(results: List<DSDocumentResult>) {
        if (results.isEmpty()) return

        mDisposable.clear()

        val disposable = Flowable
                .create({ emitter: FlowableEmitter<Void> ->

                    mDocScannerRepository.deleteDocumentResults(results)

                    emitter.onComplete()

                }, BackpressureStrategy.LATEST)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe({},
                        {
                            handleError(it)
                        },
                        {
                            loadDocuments()
                        })

        mDisposable.add(disposable)
    }

    /**
     * 从服务器端加载
     */
    private fun loadDocumentsFromLocal() {
        mDisposable.clear()

        val disposable = mDocScannerRepository
                .listDocumentResultsRx()
                .doOnSubscribe {
                    mView.setLoadingIndicator(true)
                }
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            if (it.isNotEmpty()) {
                                mView.showDocuments(it)

                                if (StringUtils.isEmpty(mUserInfoRepository.token)) {
                                    mView.setLoadingIndicator(false)
                                } else {
                                    // 获取远程数据
                                    loadDocumentsFromServer(it)
                                }
                            } else {
                                mView.showNoDocuments()
                                mView.setLoadingIndicator(false)
                            }

                        },
                        {
                            mView.setLoadingIndicator(false)
                            handleError(it)
                        })

        mDisposable.add(disposable)
    }

    /**
     * 更新本地文档的同步状态，哪些文档同步完成，哪些文档尚未同步
     */
    private fun loadDocumentsFromServer(dsDocumentResults: List<DSDocumentResult>) {

        if (StringUtils.isEmpty(mUserInfoRepository.token)) {
            return
        }

        mDisposable.clear()

        val disposable = Flowable.zip(Flowable.just(dsDocumentResults), mNutstoreRepository.listNutScanMetadatas(),
                BiFunction({ results: List<DSDocumentResult>, metadataList: List<Metadata> ->

                    for (result in results) {
                        val documentName = result.document.name
                        for (metadata in metadataList) {
                            if (MetadataUtils.isFile(metadata)) {
                                val metadataName = MetadataUtils.getDisplayName(metadata)

                                if (documentName == metadataName) {
                                    result.synced = true
                                    break
                                }
                            }
                        }
                    }

                    return@BiFunction results
                }))
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            mView.showDocuments(it)

                            if (mUserInfoRepository.isAutoUpload) {
                                syncDocuments(manual = false, results = it)
                            }
                        },
                        {
                            mView.setLoadingIndicator(false)
                            handleError(it)
                        },
                        {
                            mView.setLoadingIndicator(false)
                        })

        mDisposable.add(disposable)
    }
}