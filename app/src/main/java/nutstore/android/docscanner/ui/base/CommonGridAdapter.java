package nutstore.android.docscanner.ui.base;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nutstore.android.docscanner.R;

/**
 * 每一个 Item View 的高度总是比宽度大 1.5 倍
 *
 * @author Zhu Liang
 */

public abstract class CommonGridAdapter<T> extends RecyclerAdapter<T> {

    private final int mItemWidth;
    private final int mItemHeight;
    private final int mMargin;

    public CommonGridAdapter(Context context, int layoutId) {
        this(context, new ArrayList<T>(), layoutId);
    }

    public CommonGridAdapter(Context context, List<T> items, int layoutId) {
        super(context, items, layoutId);

        final int spanCount = context.getResources().getInteger(R.integer.module_preview_pages_num_column);
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int itemWidth = dm.widthPixels / spanCount;
        int itemHeight = (int) (itemWidth * 1.5f);

        int margin = context.getResources().getDimensionPixelOffset(R.dimen.spacing_2dp);
        mItemWidth = itemWidth - 2 * margin;
        mItemHeight = itemHeight - 2 * margin;
        mMargin = margin;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = super.onCreateViewHolder(parent, viewType);

        View itemView = holder.itemView;
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(mItemWidth, mItemHeight);
        lp.leftMargin = mMargin;
        lp.topMargin = mMargin;
        lp.rightMargin = mMargin;
        lp.bottomMargin = mMargin;

        itemView.setLayoutParams(lp);

        return holder;
    }
}
