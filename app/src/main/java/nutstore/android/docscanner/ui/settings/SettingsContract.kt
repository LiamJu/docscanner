package nutstore.android.docscanner.ui.settings

import nutstore.android.docscanner.ui.base.DSBasePresenter
import nutstore.android.docscanner.ui.base.DSBaseView

/**
 * @author Zhu Liang
 */
interface SettingsContract {
    interface Presenter : DSBasePresenter {
        fun loadUserInfo()
        fun setAutoUpload(checked: Boolean)
        fun setOnlyWiFi(checked: Boolean)
        fun logout()
    }

    interface View : DSBaseView<Presenter> {
        fun showLoginUi()
        fun showUserInfo(userInfoInternal: UserInfoInternal)
        fun showDocumentsUi()
    }
}