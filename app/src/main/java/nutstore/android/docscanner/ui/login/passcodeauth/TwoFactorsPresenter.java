package nutstore.android.docscanner.ui.login.passcodeauth;

import android.support.annotation.NonNull;

import nutstore.android.sdk.util.Preconditions;

/**
 * @author Zhu Liang
 */

class TwoFactorsPresenter implements TwoFactorsContract.Presenter {

    @NonNull
    private TwoFactorsContract.View mView;

    TwoFactorsPresenter(TwoFactorsContract.View view) {
        mView = Preconditions.checkNotNull(view, "view == null");

        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void validatePasscode(@NonNull String passcode) {
        if (!passcode.matches("^[0-9]{6}$")) {
            mView.showInvalidPasscodeError();
        } else {
            mView.showValidPasscodeUi(passcode);
            mView.dismissDialog();
        }
    }
}
