package nutstore.android.docscanner.ui.welcome.wechat

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.pigcasso.photopicker.findViewById
import nutstore.android.docscanner.R

/**
 * @author Zhu Liang
 */
class PasswordFragment : Fragment() {

    private var mOnPasswordListener: OnPasswordListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnPasswordListener) {
            mOnPasswordListener = context
        } else {
            throw ClassCastException()
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnPasswordListener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_wechat_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViewById<View>(R.id.btn_wechat_password_submit)!!.setOnClickListener({
            val textInputLayout = findViewById<TextInputLayout>(R.id.inputlayout_wechat_password)!!
            val editText = textInputLayout.editText!!
            if (mOnPasswordListener != null) {
                mOnPasswordListener!!.onPassword(editText.text.toString())
            }
        })
    }

    interface OnPasswordListener {
        fun onPassword(password: String)
    }
}