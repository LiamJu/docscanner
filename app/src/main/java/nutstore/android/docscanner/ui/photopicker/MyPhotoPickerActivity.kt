package nutstore.android.docscanner.ui.photopicker

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import io.pigcasso.photopicker.*
import nutstore.android.docscanner.R
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.widget.NavigationView

class MyPhotoPickerActivity : BaseActivity(), PhotoPickerFragment.OnPhotoPickerListener {

    companion object {
        const val EXTRA_RESULT_SELECTION = "extra_result_selection"

        fun makeIntent(context: Context): Intent {
            val intent = Intent(context, MyPhotoPickerActivity::class.java)
            intent.putExtra(EXTRA_ALL_PHOTOS_ALBUM, true)
            intent.putExtra(EXTRA_CHOICE_MODE, CHOICE_MODE_MULTIPLE_NO_UPPER_LIMIT)
            intent.putExtra(EXTRA_LIMIT_COUNT, NO_LIMIT_COUNT)
            intent.putExtra(EXTRA_COUNTABLE, true)
            intent.putExtra(EXTRA_PREVIEW, false)
            intent.putExtra(EXTRA_SELECTABLE_ALL, false)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_my_photo_picker)

        checkNotNull(intent)

        var fragment = supportFragmentManager.findFragmentById(R.id.contentFrame)
        if (fragment == null) {
            fragment = PhotoPickerFragment.newInstance(
                    intent!!.getBooleanExtra(EXTRA_ALL_PHOTOS_ALBUM, true),
                    intent!!.getIntExtra(EXTRA_CHOICE_MODE, CHOICE_MODE_MULTIPLE_NO_UPPER_LIMIT),
                    intent!!.getIntExtra(EXTRA_LIMIT_COUNT, NO_LIMIT_COUNT),
                    intent!!.getBooleanExtra(EXTRA_COUNTABLE, false),
                    intent!!.getBooleanExtra(EXTRA_PREVIEW, true),
                    intent!!.getBooleanExtra(EXTRA_SELECTABLE_ALL, false))
            supportFragmentManager.beginTransaction().add(R.id.contentFrame, fragment).commit()
        }

        findViewById<NavigationView>(R.id.navigation_view).setOnNavigationViewListener(object : NavigationView.OnNavigationViewListener {
            override fun onStartClicked(view: View?) {
                finish()
            }

            override fun onEndClicked(view: View?) {
                val f = supportFragmentManager.findFragmentById(R.id.contentFrame)
                if (f != null && f.isAdded && f is PhotoPickerFragment) {
                    onSelectedResult(f.getAllCheckedPhotos())
                }
            }
        })
    }

    /**
     * 这个回调仅仅用于更新标题栏
     */
    override fun onPhotosSelect(photoPaths: List<String>) {
        findViewById<NavigationView>(R.id.navigation_view)?.setTitle(getString(R.string.module_my_photo_picker_selected, photoPaths.size))
    }

    override fun onSelectedResult(photoPaths: ArrayList<String>) {
        val data = Intent()
        data.putStringArrayListExtra(EXTRA_RESULT_SELECTION, photoPaths)
        setResult(Activity.RESULT_OK, data)
        finish()
    }
}