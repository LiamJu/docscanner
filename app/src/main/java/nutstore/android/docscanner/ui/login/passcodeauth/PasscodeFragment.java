package nutstore.android.docscanner.ui.login.passcodeauth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.R;
import nutstore.android.docscanner.common.SimpleTextWatcher;
import nutstore.android.sdk.ui.base.BaseFragment;

/**
 * 安全验证页面：获取验证码、提交验证、收不到验证码？
 *
 * @author Julian Chu
 * @since 2017/12/14
 */

public class PasscodeFragment extends BaseFragment<PasscodeAuthContract.Presenter>
        implements PasscodeAuthContract.View {

    private static final String KEY_PASSCODE = "passcode";

    private TextView mTitleText;
    private TextInputLayout mPasscodeInput;
    private EditText mPasscodeEdit;
    private Button mResendButton;
    private CharSequence mPasscode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_passcode, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            mPasscode = savedInstanceState.getCharSequence(KEY_PASSCODE);
        }

        mTitleText = (TextView) view.findViewById(R.id.text_passcode_title);
        mPasscodeInput = (TextInputLayout) view.findViewById(R.id.inputlayout_passcode);
        mPasscodeEdit = mPasscodeInput.getEditText();
        mResendButton = (Button) view.findViewById(R.id.button_resend);

        mPasscodeEdit.setText(mPasscode);

        mPasscodeEdit.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                mPasscodeInput.setError(null);
                mPasscode = s;
            }
        });

        view.findViewById(R.id.image_passcode_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        view.findViewById(R.id.button_resend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.restartCountDownTimer();
            }
        });
        view.findViewById(R.id.button_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.validatePasscode(mPasscodeEdit.getText().toString());
            }
        });
        view.findViewById(R.id.text_passcode_not_receive_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(Constants.URL_NOT_RECEIVE_CODE));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.recyclerCountDownTimer();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(KEY_PASSCODE, mPasscode);
    }

    @Override
    public void setResendUi(boolean enabled) {
        if (mResendButton != null) {
            mResendButton.setEnabled(enabled);
            if (enabled) {
                mResendButton.setText(R.string.module_passcode_resend);
            } else {
                // 当"重新发送"按钮不可点时，发送消息让接收者去发送验证码
                EventBus.getDefault().post(new SendPasscodeMsg());
            }
        }
    }

    /**
     * 更新剩余秒数
     *
     * @param leftSeconds 剩余秒数
     */
    @Override
    public void setLeftSecondsUi(long leftSeconds) {
        if (mResendButton != null) {
            mResendButton.setText(getString(R.string.module_passcode_resend_in_seconds, leftSeconds));
        }
    }

    /**
     * 显示有效的验证码
     *
     * @param passcode 验证码
     */
    @Override
    public void showValidPasscodeUi(String passcode) {
        EventBus.getDefault().post(new PasscodeMsg(passcode));
    }

    @Override
    public void showInvalidPasscodeUi() {
        mPasscodeInput.setError(getString(R.string.module_passcode_should_be_six_numbers));
    }

    @Override
    public void setPasscodeTitle(String title) {
        if (mTitleText != null) {
            mTitleText.setText(title);
        }
    }

    @Override
    public void dismissDialog() {

    }
}
