package nutstore.android.docscanner.ui.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import nutstore.android.docscanner.Injection
import nutstore.android.docscanner.R
import nutstore.android.docscanner.ui.base.BaseActivity
import nutstore.android.docscanner.util.L

/**
 * @author Zhu Liang
 */
class SettingsActivity : BaseActivity() {

    companion object {

        private val TAG = SettingsActivity::class.java.simpleName

        fun makeIntent(context: Context) = Intent(context, SettingsActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        L.d(TAG, "onCreate: ")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        var f = supportFragmentManager.findFragmentById(R.id.contentFrame)

        if (f == null) {
            f = SettingsFragment()
            supportFragmentManager.beginTransaction().add(R.id.contentFrame, f).commit()
        }
        SettingsPresenter(f as SettingsContract.View, Injection.provideSchedulerProvider(),
                Injection.provideUserInfoRepository(this),
                Injection.provideNutstoreAPI(this),
                Injection.provideDocumentRepository(this))
    }
}