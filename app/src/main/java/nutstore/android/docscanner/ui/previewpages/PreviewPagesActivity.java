package nutstore.android.docscanner.ui.previewpages;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.R;
import nutstore.android.docscanner.data.DSPage;
import nutstore.android.docscanner.event.Event;
import nutstore.android.docscanner.event.EventParams;
import nutstore.android.docscanner.event.RecordEvent;
import nutstore.android.docscanner.service.DocumentService;
import nutstore.android.docscanner.task.ImageLoader;
import nutstore.android.docscanner.ui.base.BaseActivity;
import nutstore.android.docscanner.ui.base.CommonGridAdapter;
import nutstore.android.docscanner.ui.common.OKCancelDialogFragment;
import nutstore.android.docscanner.widget.NavigationView;
import nutstore.android.sdk.util.Preconditions;

/**
 * 网格视图预览页面
 *
 * @author Zhu Liang
 */

public class PreviewPagesActivity extends BaseActivity {

    public static final String KEY_CHECKED_PAGES = "key.CHECKED_PAGES";
    private static final String FRAGMENT_TAG_DELETE = "fragment.tag.DELETE";
    private static final int FRAGMENT_DIALOG_ID_DELETE = -1;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.rv_preview_pages_content)
    RecyclerView mRecyclerView;
    @BindView(R.id.iv_preview_pages_delete)
    ImageView mDeleteIv;
    private ArrayList<Integer> mCheckedPages;
    private PreviewPagesAdapter mPagesAdapter;

    public static Intent makeIntent(Context context, ArrayList<DSPage> pages) {
        Intent intent = new Intent(context, PreviewPagesActivity.class);
        intent.putParcelableArrayListExtra(Constants.EXTRA_EDITED_PAGES, pages);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mCheckedPages = new ArrayList<>();
        } else {
            mCheckedPages = savedInstanceState.getIntegerArrayList(KEY_CHECKED_PAGES);
        }

        setContentView(R.layout.activity_preview_pages);
        ButterKnife.bind(this);

        ArrayList<DSPage> pages = getIntent().getParcelableArrayListExtra(Constants.EXTRA_EDITED_PAGES);
        Preconditions.checkArgument(pages != null && !pages.isEmpty(), "Pages is empty");
        final int spanCount = getResources().getInteger(R.integer.module_preview_pages_num_column);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
        mRecyclerView.setAdapter(mPagesAdapter = new PreviewPagesAdapter(this, pages));

        mNavigationView.setOnNavigationViewListener(new NavigationView.OnNavigationViewListener() {
            @Override
            public void onStartClicked(View view) {
                toggleCheckAll();
            }

            @Override
            public void onEndClicked(View view) {
                Intent data = new Intent();
                data.putParcelableArrayListExtra(Constants.EXTRA_PREVIEW_PAGES, new ArrayList<>(mPagesAdapter.getItems()));
                setResult(RESULT_OK, data);
                finish();
            }
        });

        updateTitleAndDeleteIconAndNavStartText();
    }

    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
    }

    /**
     * 选择全部或者取消全选
     */
    private void toggleCheckAll() {
        // 切换到"取消全选"
        if (mCheckedPages.size() == mPagesAdapter.getItemCount()) {
            mCheckedPages.clear();
            mPagesAdapter.notifyDataSetChanged();
        } else {
            // 切换到"选择全部"
            List<DSPage> items = mPagesAdapter.getItems();
            for (int i = 0; i < items.size(); i++) {
                if (!mCheckedPages.contains(i)) {
                    mCheckedPages.add(i);
                }
            }
            mPagesAdapter.notifyDataSetChanged();
        }
        updateTitleAndDeleteIconAndNavStartText();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putIntegerArrayList(KEY_CHECKED_PAGES, mCheckedPages);
    }

    private boolean isPageChecked(DSPage page) {
        int index = mPagesAdapter.indexOf(page);
        return mCheckedPages.contains(index);
    }

    @OnClick(R.id.iv_preview_pages_delete)
    public void onViewClicked() {
        OKCancelDialogFragment.newInstance(getString(R.string.module_preview_pages_delete_title),
                getString(R.string.module_preview_pages_delete_message, mCheckedPages.size()),
                getString(R.string.module_preview_pages_delete_positive),
                getString(R.string.module_preview_pages_delete_negative), FRAGMENT_DIALOG_ID_DELETE, null).show(getSupportFragmentManager(), FRAGMENT_TAG_DELETE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOKCancelMsg(OKCancelDialogFragment.OKCancelMsg okCancelMsg) {
        switch (okCancelMsg.which) {
            case FRAGMENT_DIALOG_ID_DELETE:
                if (DialogInterface.BUTTON_POSITIVE == okCancelMsg.which) {
                    ArrayList<DSPage> deleted = new ArrayList<>();
                    for (Integer index : mCheckedPages) {
                        DSPage page = mPagesAdapter.getItem(index);
                        deleted.add(page);
                    }
                    if (mPagesAdapter.getItems().size() <= deleted.size()) {
                        // 如果所有的页面都被删除，则 finish 掉当前页面
                        Intent data = new Intent();
                        data.putParcelableArrayListExtra(Constants.EXTRA_PREVIEW_PAGES, new ArrayList<DSPage>());
                        setResult(RESULT_OK, data);
                        finish();
                    } else {
                        // 删掉被选中的页面，直接登出
                        mCheckedPages.clear();
                        mPagesAdapter.removeAll(deleted);
                        updateTitleAndDeleteIconAndNavStartText();
                    }
                    // 统计多选删除
                    RecordEvent.getInstance().recordEvent(new EventParams.Builder(Event.deleteMultipageEditScan).build());
                    DocumentService.Companion.deletePages(this, deleted);
                }
                break;
        }
    }

    private static class PreviewPagesAdapter extends CommonGridAdapter<DSPage> {

        private PreviewPagesActivity mActivity;
        private final Drawable mCheckboxDrawable;
        private final Drawable mCheckboxOutlineDrawable;

        private PreviewPagesAdapter(PreviewPagesActivity context, List<DSPage> items) {
            super(context, items, R.layout.recycler_item_preview_page);

            mActivity = context;

            Drawable checkboxDrawable = ContextCompat.getDrawable(context, R.drawable.ic_check_box_black_24dp);
            checkboxDrawable = DrawableCompat.wrap(checkboxDrawable);
            DrawableCompat.setTint(checkboxDrawable, ContextCompat.getColor(context, R.color.accent));
            mCheckboxDrawable = checkboxDrawable;

            Drawable checkboxOutlineDrawable = ContextCompat.getDrawable(context, R.drawable.ic_check_box_outline_blank_black_24dp);
            checkboxOutlineDrawable = DrawableCompat.wrap(checkboxOutlineDrawable);
            DrawableCompat.setTint(checkboxOutlineDrawable, ContextCompat.getColor(context, R.color.gray));
            mCheckboxOutlineDrawable = checkboxOutlineDrawable;
        }

        @Override
        protected void bindView(@NonNull final ViewHolder holder, @NonNull final DSPage value) {
            ImageLoader.getInstance().loadImage((ImageView) holder.findViewById(R.id.iv_preview_page_thumbnail),
                    value.getPath(), value.getRotationTypeEnum(), value.getOptimizationTypeEnum(), value.getPolygonF());

            if (mActivity.isPageChecked(value)) {
                holder.setImageDrawable(R.id.iv_preview_page_checkbox, mCheckboxDrawable);
            } else {
                holder.setImageDrawable(R.id.iv_preview_page_checkbox, mCheckboxOutlineDrawable);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mActivity.isPageChecked(value)) {
                        mActivity.uncheckedPage(value);
                        holder.setImageDrawable(R.id.iv_preview_page_checkbox, mCheckboxOutlineDrawable);
                    } else {
                        mActivity.checkedPage(value);
                        holder.setImageDrawable(R.id.iv_preview_page_checkbox, mCheckboxDrawable);
                    }
                    mActivity.updateTitleAndDeleteIconAndNavStartText();
                }
            });
        }

        int indexOf(DSPage page) {
            return mItems.indexOf(page);
        }

        void removeAll(List<DSPage> c) {
            mItems.removeAll(c);
            notifyDataSetChanged();
        }

        public List<DSPage> getItems() {
            return mItems;
        }
    }

    private void uncheckedPage(DSPage page) {
        int index = mPagesAdapter.indexOf(page);
        if (index != -1 && mCheckedPages.contains(index)) {
            mCheckedPages.remove((Object) index);
        }
    }

    private void checkedPage(DSPage page) {
        int index = mPagesAdapter.indexOf(page);
        if (index != -1 && !mCheckedPages.contains(index)) {
            mCheckedPages.add(index);
        }
    }

    /**
     * 更新标题、左侧文字、删除图标
     */
    private void updateTitleAndDeleteIconAndNavStartText() {
        mNavigationView.setTitle(getString(R.string.module_preview_pages_select_count, mCheckedPages.size()));
        mDeleteIv.setEnabled(mCheckedPages.size() > 0);

        if (mCheckedPages.size() == mPagesAdapter.getItemCount()) {
            mNavigationView.setStartText(getString(R.string.module_preview_pages_unselect_all));
        } else {
            mNavigationView.setStartText(getString(R.string.module_preview_pages_select_all));
        }
    }
}
