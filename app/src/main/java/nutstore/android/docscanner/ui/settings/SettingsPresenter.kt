package nutstore.android.docscanner.ui.settings

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import nutstore.android.docscanner.data.DocScannerRepository
import nutstore.android.docscanner.data.UserInfoRepository
import nutstore.android.docscanner.ui.base.DSAbstractPresenter
import nutstore.android.sdk.NutstoreAPI
import nutstore.android.sdk.util.StringUtils
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider

/**
 * @author Zhu Liang
 */
class SettingsPresenter(view: SettingsContract.View,
                        schedulerProvider: BaseSchedulerProvider,
                        private val mUserInfoRepository: UserInfoRepository,
                        private val mNutstoreAPI: NutstoreAPI,
                        private val mDocScannerRepository: DocScannerRepository) : DSAbstractPresenter<SettingsContract.View>(view, schedulerProvider), SettingsContract.Presenter {

    override fun subscribe() {
        loadUserInfo()
    }

    override fun loadUserInfo() {
        mDisposable.clear()

        val disposable = Flowable
                .create({ it: FlowableEmitter<UserInfoInternal> ->

                    it.onNext(UserInfoInternal(mUserInfoRepository.username,
                            mUserInfoRepository.isAutoUpload,
                            mUserInfoRepository.isOnlyWiFi,
                            mUserInfoRepository.userInfo))
                    it.onComplete()

                }, BackpressureStrategy.LATEST)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            val userInfoInternal = it
                            mView.showUserInfo(userInfoInternal)

                            if (StringUtils.isNotEmpty(userInfoInternal.username)) {
                                loadUserInfoInternal(userInfoInternal)
                            }
                        },
                        {
                            handleError(it)
                        })

        mDisposable.add(disposable)
    }

    override fun setAutoUpload(checked: Boolean) {
        mUserInfoRepository.saveAutoUpload(checked)
    }

    override fun setOnlyWiFi(checked: Boolean) {
        mUserInfoRepository.saveOnlyWiFi(checked)
    }

    override fun logout() {
        mDisposable.clear()

        val disposable = Flowable
                .create({ emitter: FlowableEmitter<UserInfoInternal> ->

                    mUserInfoRepository.removeAll()
                    emitter.onNext(UserInfoInternal())
                    emitter.onComplete()

                }, BackpressureStrategy.LATEST)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            mView.showUserInfo(it)
                        },
                        {
                            handleError(it)
                        })
        mDisposable.add(disposable)

    }

    private fun loadUserInfoInternal(userInfoInternal: UserInfoInternal) {
        mDisposable.clear()

        val disposable = mNutstoreAPI
                .userInfoV2
                .doOnNext {
                    mUserInfoRepository.saveUserInfo(it)
                }
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            mView.showUserInfo(UserInfoInternal(userInfoInternal.username,
                                    userInfoInternal.autoUpload,
                                    userInfoInternal.onlyWiFi, it))
                        },
                        {
                            handleError(it)
                        })

        mDisposable.add(disposable)
    }


}
