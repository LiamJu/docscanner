package nutstore.android.docscanner.ui;

import android.content.Intent;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import net.doo.snap.ScanbotSDK;
import net.doo.snap.entity.Page;
import net.doo.snap.persistence.PageFactory;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.R;
import nutstore.android.docscanner.data.DSPage;
import nutstore.android.docscanner.service.DocumentService;
import nutstore.android.docscanner.ui.base.BaseActivity;
import nutstore.android.docscanner.ui.capture.CaptureActivity;
import nutstore.android.docscanner.ui.documents.DocumentsActivity;
import nutstore.android.docscanner.ui.editcapture.EditCapturesActivity;
import nutstore.android.docscanner.ui.editpolygon.EditPolygonActivity;
import nutstore.android.docscanner.ui.previewpages.PreviewPagesActivity;
import nutstore.android.docscanner.ui.welcome.WelcomeActivity;
import nutstore.android.docscanner.util.L;

/**
 * 考虑到不太熟悉APP的页面跳转流程，目前打算先独立开发每一个页面，等流程熟悉之后再将页面穿起来。
 */
public class SampleActivity extends BaseActivity {

    private static final String TAG = "SampleActivity";

    private ImageView mResultIv;

    private ScanbotSDK mScanbotSDK;
    private PageFactory mPageFactory;

    private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyyMMdd-kkmmss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        mResultIv = findViewById(R.id.iv_sample_result);

        mScanbotSDK = new ScanbotSDK(this);
        mPageFactory = mScanbotSDK.pageFactory();
    }

    public void showDocumentsUi(View view) {
        startActivity(DocumentsActivity.Companion.makeIntent(this));
    }

    public void showCaptureUiByAutoSnapping(View view) {
        startActivityForResult(CaptureActivity.Companion.makeIntent(this), Constants.REQUEST_CODE_CAPTURE);
    }

    public void showCaptureUiByManualSnapping(View view) {
        startActivity(CaptureActivity.Companion.makeIntent(this));
    }

    public void showEditPolygonCancelUi(View view) {
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        final File imageFile = new File(downloadDir, "img_test_receipt.jpg");
        if (!imageFile.exists()) {
            Toast.makeText(this, "请添加一张名为：img_test_receipt.jpg的图片", Toast.LENGTH_SHORT).show();
            return;
        }
        Flowable
                .create(new FlowableOnSubscribe<DSPage>() {
                    @Override
                    public void subscribe(FlowableEmitter<DSPage> e) throws Exception {
                        e.onNext(convertToDSPage(imageFile));
                    }
                }, BackpressureStrategy.LATEST)
                .subscribe(new Consumer<DSPage>() {
                    @Override
                    public void accept(DSPage page) throws Exception {
                        startActivityForResult(EditPolygonActivity.makeIntent(SampleActivity.this, page, EditPolygonActivity.TYPE_CANCEL),
                                Constants.REQUEST_CODE_EDIT_POLYGON);
                    }
                });

    }

    public void showEditPolygonRetakeUi(View view) {
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        final File imageFile = new File(downloadDir, "img_test_receipt.jpg");
        if (!imageFile.exists()) {
            Toast.makeText(this, "请添加一张名为：img_test_receipt.jpg的图片", Toast.LENGTH_SHORT).show();
            return;
        }
        Flowable
                .create(new FlowableOnSubscribe<DSPage>() {
                    @Override
                    public void subscribe(FlowableEmitter<DSPage> e) throws Exception {
                        e.onNext(convertToDSPage(imageFile));
                    }
                }, BackpressureStrategy.LATEST)
                .subscribe(new Consumer<DSPage>() {
                    @Override
                    public void accept(DSPage page) throws Exception {
                        startActivityForResult(EditPolygonActivity.makeIntent(SampleActivity.this, page, EditPolygonActivity.TYPE_RETAKE),
                                Constants.REQUEST_CODE_EDIT_POLYGON);
                    }
                });
    }

    public void showEditCapturesUi(View view) {
        ArrayList<File> uris = new ArrayList<>();
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        Flowable
                .fromIterable(uris)
                .map(new Function<File, DSPage>() {
                    @Override
                    public DSPage apply(File file) throws Exception {
                        return convertToDSPage(file);
                    }
                })
                .toList()
                .subscribe(new Consumer<List<DSPage>>() {
                    @Override
                    public void accept(List<DSPage> dsPages) throws Exception {
                        startActivity(EditCapturesActivity.Companion.makeIntent(SampleActivity.this, new ArrayList<DSPage>(dsPages)));

                    }
                });
    }

    @NonNull
    private DSPage convertToDSPage(File file) throws IOException {
        Page page = mPageFactory.buildPage(file);
        DSPage dsPage = new DSPage(page);
        dsPage.setPath(file.getAbsolutePath());
        return dsPage;
    }

    public void showPreviewPagesUi(View view) {
        ArrayList<File> uris = new ArrayList<>();
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));
        Flowable
                .fromIterable(uris)
                .map(new Function<File, DSPage>() {
                    @Override
                    public DSPage apply(File file) throws Exception {
                        return convertToDSPage(file);
                    }
                })
                .toList()
                .subscribe(new Consumer<List<DSPage>>() {
                    @Override
                    public void accept(List<DSPage> dsPages) throws Exception {
                        startActivityForResult(PreviewPagesActivity.makeIntent(SampleActivity.this, new ArrayList<DSPage>(dsPages)),
                                Constants.REQUEST_CODE_PREVIEW_PAGES);
                    }
                });
    }

    public void createDocument(View view) {
        ArrayList<File> uris = new ArrayList<>();
        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        uris.add(new File(downloadDir, "img_test_receipt.jpg"));
        uris.add(new File(downloadDir, "WechatIMG291.jpeg"));

        Flowable
                .fromIterable(uris)
                .map(new Function<File, DSPage>() {
                    @Override
                    public DSPage apply(File file) throws Exception {
                        return convertToDSPage(file);
                    }
                })
                .toList()
                .subscribe(new Consumer<List<DSPage>>() {
                    @Override
                    public void accept(List<DSPage> dsPages) throws Exception {
                        DocumentService.Companion.createDocument(SampleActivity.this, mSimpleDateFormat.format(new Date()), new ArrayList<DSPage>(dsPages));
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.REQUEST_CODE_CREATE_DOCUMENT:
                break;
            case Constants.REQUEST_CODE_EDIT_POLYGON:
                if (RESULT_OK == resultCode) {
                    Uri result = data.getData();
                    if (mResultIv != null) {
                        mResultIv.setImageURI(result);
                    }
                    ArrayList<PointF> polygon = data.getParcelableArrayListExtra(Constants.EXTRA_EDIT_POLYGON);
                    String coordinates = "";
                    for (PointF pointF : polygon) {
                        coordinates += "\n";
                        coordinates += pointF.toString();
                    }
                    L.d(TAG, "onActivityResult: " + coordinates);
                    showToast(String.format("源文件路径：%s, 剪裁坐标：%s", result.getPath(), coordinates));
                } else {
                    showToast("剪切图片失败！！");
                }
                break;
            case Constants.REQUEST_CODE_EDIT_CAPTURE:
                if (RESULT_OK == resultCode) {
                    if (mResultIv != null) {
                        mResultIv.setImageURI(data.getData());
                    }
                    showToast(String.format("处理扫描后的结果：%s", data.getData()));
                } else {
                    showToast("处理扫描后的结果失败！！");
                }
                break;
            case Constants.REQUEST_CODE_PREVIEW_PAGES:
                if (RESULT_OK == resultCode) {
                    ArrayList<DSPage> result = data.getParcelableArrayListExtra(Constants.EXTRA_PREVIEW_PAGES);
                    showToast("预览后的结果：" + result.size());
                } else {
                    showToast("预览后点击了返回键");
                }
                break;
            case Constants.REQUEST_CODE_CAPTURE:
                if (RESULT_OK == resultCode) {
                    ArrayList<DSPage> pages = data.getParcelableArrayListExtra(Constants.EXTRA_CAPTURE_PAGES);
                    throw new IllegalStateException("\"capturePages: \" + pages.size()");
                }
                break;
        }
    }

    public void showWelcomeUi(View view) {
        startActivity(WelcomeActivity.Companion.makeIntent(this));
    }
}
