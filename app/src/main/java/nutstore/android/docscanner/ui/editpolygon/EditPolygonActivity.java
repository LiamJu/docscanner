package nutstore.android.docscanner.ui.editpolygon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import net.doo.snap.lib.detector.ContourDetector;
import net.doo.snap.lib.detector.DetectionResult;
import net.doo.snap.lib.detector.Line2D;
import net.doo.snap.ui.EditPolygonImageView;
import net.doo.snap.ui.MagnifierView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import nutstore.android.docscanner.Constants;
import nutstore.android.docscanner.Injection;
import nutstore.android.docscanner.R;
import nutstore.android.docscanner.data.DSPage;
import nutstore.android.docscanner.service.DocumentService;
import nutstore.android.docscanner.ui.GlideApp;
import nutstore.android.docscanner.ui.base.BaseActivity;
import nutstore.android.docscanner.widget.NavigationView;
import nutstore.android.sdk.util.Preconditions;
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider;

/**
 * 剪裁手动扫描后的结果
 */
public class EditPolygonActivity extends BaseActivity {

    private static final String EXTRA_TYPE = "extra.TYPE";

    /**
     * 左上角显示"重拍"按钮
     */
    public static final int TYPE_RETAKE = 477;

    /**
     * 左上角显示"取消"按钮
     */
    public static final int TYPE_CANCEL = 123;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.polygon_edit_polygon_polygon)
    EditPolygonImageView editPolygonView;
    @BindView(R.id.magnifier_edit_polygon_magnifier)
    MagnifierView magnifierView;
    private Bitmap originalBitmap;
    private DSPage mEditedPage;

    private int mType;

    private CompositeDisposable mDisposable = new CompositeDisposable();
    private BaseSchedulerProvider mSchedulerProvider = Injection.provideSchedulerProvider();

    /**
     * 待剪裁原文件
     */
    public static Intent makeIntent(Context context, DSPage editedPage, int type) {
        Intent intent = new Intent(context, EditPolygonActivity.class);
        intent.putExtra(Constants.EXTRA_EDITED_PAGE, editedPage);
        intent.putExtra(EXTRA_TYPE, type);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_polygon);
        ButterKnife.bind(this);

        mEditedPage = getIntent().getParcelableExtra(Constants.EXTRA_EDITED_PAGE);
        mType = getIntent().getIntExtra(EXTRA_TYPE, TYPE_CANCEL);
        Preconditions.checkArgument(TYPE_CANCEL == mType || TYPE_RETAKE == mType);

        if (mType == TYPE_CANCEL) {
            mNavigationView.setStartText(getString(R.string.module_edit_polygon_cancel));
        } else {
            mNavigationView.setStartText(getString(R.string.module_edit_polygon_retake));
        }

        mNavigationView.setOnNavigationViewListener(new NavigationView.OnNavigationViewListener() {
            @Override
            public void onStartClicked(View view) {
                // 如果重拍，将删除这个页面
                if (mType == TYPE_RETAKE) {
                    DocumentService.Companion.deletePage(EditPolygonActivity.this, mEditedPage);
                }
                finish();
            }

            @Override
            public void onEndClicked(View view) {
                Intent intent = new Intent();
                DSPage page = mEditedPage;
                page.setPolygon(editPolygonView.getPolygon());
                intent.putExtra(Constants.EXTRA_EDITED_PAGE, page);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mType == TYPE_RETAKE) {
            DocumentService.Companion.deletePage(EditPolygonActivity.this, mEditedPage);
        }
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initBitmap(mEditedPage.getPath());
    }

    @Override
    protected void onPause() {
        super.onPause();

        mDisposable.clear();
    }

    @OnClick({R.id.btn_edit_polygon_select_all, R.id.btn_edit_polygon_detect_document})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_edit_polygon_select_all:
                // 选择全图
                setDefaultPolygon();
                break;
            case R.id.btn_edit_polygon_detect_document:
                // 寻找边框
                if (originalBitmap != null) {
                    detectPolygon(originalBitmap);
                }
                break;
        }
    }

    /**
     * 将整张图片框住
     */
    private void setDefaultPolygon() {
        editPolygonView.post(new Runnable() {
            @Override
            public void run() {
                List<PointF> polygon = new ArrayList<>(EditPolygonImageView.DEFAULT_POLYGON);
                editPolygonView.setPolygon(polygon);
            }
        });
    }

    private void initBitmap(final String pathname) {
        GlideApp
                .with(this)
                .load(new File(pathname))
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        if (resource instanceof BitmapDrawable) {
                            Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                            originalBitmap = bitmap;
                            editPolygonView.setImageBitmap(bitmap);
                            magnifierView.setupMagnifier(editPolygonView);

                            detectPolygon(bitmap);
                        }
                    }
                });
    }

    private void detectPolygon(Bitmap bitmap) {

        if (bitmap == null) {
            return;
        }

        mDisposable.clear();

        Disposable disposable = Flowable
                .just(bitmap)
                .map(new Function<Bitmap, InitImageResult>() {
                    @Override
                    public InitImageResult apply(Bitmap bitmap) throws Exception {
                        ContourDetector detector = new ContourDetector();
                        final DetectionResult detectionResult = detector.detect(bitmap);
                        Pair<List<Line2D>, List<Line2D>> linesPair = null;
                        List<PointF> polygon = new ArrayList<>(EditPolygonImageView.DEFAULT_POLYGON);
                        switch (detectionResult) {
                            case OK:
                            case OK_BUT_BAD_ANGLES:
                            case OK_BUT_TOO_SMALL:
                            case OK_BUT_BAD_ASPECT_RATIO:
                                linesPair = new Pair<>(detector.getHorizontalLines(), detector.getVerticalLines());
                                polygon = detector.getPolygonF();
                                break;
                        }

                        return new InitImageResult(linesPair, polygon);
                    }
                })
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Consumer<InitImageResult>() {
                    @Override
                    public void accept(InitImageResult initImageResult) throws Exception {
                        editPolygonView.setPolygon(initImageResult.polygon);
                        if (initImageResult.linesPair != null) {
                            editPolygonView.setLines(initImageResult.linesPair.first, initImageResult.linesPair.second);
                        }
                    }
                });

        mDisposable.add(disposable);
    }

    class InitImageResult {
        final Pair<List<Line2D>, List<Line2D>> linesPair;
        final List<PointF> polygon;

        InitImageResult(final Pair<List<Line2D>, List<Line2D>> linesPair, final List<PointF> polygon) {
            this.linesPair = linesPair;
            this.polygon = polygon;
        }
    }
}