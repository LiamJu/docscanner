package nutstore.android.docscanner.ui.settings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v4.app.TaskStackBuilder
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Switch
import com.sobot.chat.SobotApi
import com.sobot.chat.api.model.Information
import io.pigcasso.photopicker.findViewById
import nutstore.android.docscanner.BuildConfig
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.R
import nutstore.android.docscanner.event.Event
import nutstore.android.docscanner.event.EventParams
import nutstore.android.docscanner.event.RecordEvent
import nutstore.android.docscanner.ui.base.DSBaseFragment
import nutstore.android.docscanner.ui.welcome.WelcomeActivity
import nutstore.android.docscanner.util.IntentUtils
import nutstore.android.docscanner.util.L
import nutstore.android.docscanner.util.ToastUtils
import nutstore.android.docscanner.widget.ItemView
import nutstore.android.docscanner.widget.NavigationView
import nutstore.android.sdk.consts.PathConsts
import nutstore.android.sdk.util.StringUtils


class SettingsFragment : DSBaseFragment<SettingsContract.Presenter>(), SettingsContract.View {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onResume() {
        super.onResume()

        // 监听Back键
        if (view == null) return
        view!!.isFocusableInTouchMode = true
        view!!.requestFocus()
        view!!.setOnKeyListener({ _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                // handle back button's click listener
                showDocumentsUi()
                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        })
    }

    override fun showUserInfo(userInfoInternal: UserInfoInternal) {
        // 未登录
        val notLoginItemView = findViewById<ItemView>(R.id.item_view_settings_not_login)!!
        // 免费、付费用户-用户名
        val usernameItemView = findViewById<ItemView>(R.id.item_view_settings_username)!!
        // 付费用户-已用空间
        val paidUsedStorageSizeItemView = findViewById<ItemView>(R.id.item_view_settings_paid_used_storage_size)!!
        // 付费用户-到期时间
        val paidAccountExpireLeftTimeItemView = findViewById<ItemView>(R.id.item_view_settings_paid_account_expire_left_time)!!
        // 免费用户-已用上传空间
        val freeUsedUpRateItemView = findViewById<ItemView>(R.id.item_view_settings_free_used_up_rate)!!
        // 通用选项-下载选项
        val downloadAppItemView = findViewById<ItemView>(R.id.item_view_settings_download_app)!!

        val sandboxScannerItemView = findViewById<ItemView>(R.id.item_view_setting_sync_position)!!
        val autoUploadSwitch = findViewById<Switch>(R.id.switch_settings_auto_upload)!!
        val onlyWiFiSwitch = findViewById<Switch>(R.id.switch_settings_only_wifi)!!
        val logoutButton = findViewById<View>(R.id.btn_settings_logout)!!
        val reportProblem = findViewById<View>(R.id.item_view_settings_report_problem)!!
        val onlineCustomerService = findViewById<View>(R.id.item_view_settings_online_cs)!!

        // sandboxScannerItemView 显示扫描文档的路径
        sandboxScannerItemView.setEndText(getString(R.string.common_my_sandbox_display_name) +
                PathConsts.PATH_SEPARATOR +
                getString(R.string.common_nut_scan_display_name))

        val userInfo = userInfoInternal.userInfo

        if (userInfo == null) {
            // 未登录
            notLoginItemView.visibility = VISIBLE
            // 用户名
            usernameItemView.visibility = GONE
            // 已用空间
            paidUsedStorageSizeItemView.visibility = GONE
            // 过期时间
            paidAccountExpireLeftTimeItemView.visibility = GONE
            // 上传流量
            freeUsedUpRateItemView.visibility = GONE
            // 同步文件夹名称
            sandboxScannerItemView.isEnabled = false
            // 自动上传
            autoUploadSwitch.isEnabled = false
            autoUploadSwitch.isChecked = userInfoInternal.autoUpload
            // 禁用 WiFi 上传
            onlyWiFiSwitch.isEnabled = false
            onlyWiFiSwitch.isChecked = userInfoInternal.onlyWiFi
            // 退出登出
            logoutButton.visibility = GONE

        } else if (userInfo.isIsPaidUser || userInfo.isIsInTeam) {
            // 付费用户
            notLoginItemView.visibility = GONE
            // 用户名
            usernameItemView.visibility = VISIBLE
            usernameItemView.setStartText(userInfoInternal.username)
            // 已用空间
            paidUsedStorageSizeItemView.visibility = VISIBLE
            paidUsedStorageSizeItemView.setEndText(getString(R.string.module_settings_used_storage_size,
                    StringUtils.readableFileSize(userInfo.usedStorageSize),
                    StringUtils.readableFileSize(userInfo.totalStorageSize)))
            // 过期时间
            paidAccountExpireLeftTimeItemView.visibility = VISIBLE
            paidAccountExpireLeftTimeItemView.setEndText(getString(R.string.module_settings_account_expire_left_time,
                    userInfo.accountExpireLeftTime / Constants.MILLIS_IN_DAY + 1))
            // 上传流量
            freeUsedUpRateItemView.visibility = GONE
            // 同步文件夹名称
            sandboxScannerItemView.isEnabled = true
            // 自动上传
            autoUploadSwitch.isEnabled = true
            autoUploadSwitch.isChecked = userInfoInternal.autoUpload
            // 禁用 WiFi 上传
            onlyWiFiSwitch.isEnabled = true
            onlyWiFiSwitch.isChecked = userInfoInternal.onlyWiFi
            // 退出登出
            logoutButton.visibility = VISIBLE

        } else {
            // 免费用户
            notLoginItemView.visibility = GONE
            // 用户名
            usernameItemView.visibility = VISIBLE
            usernameItemView.setStartText(userInfoInternal.username)
            // 已用空间
            paidUsedStorageSizeItemView.visibility = GONE
            // 过期时间
            paidAccountExpireLeftTimeItemView.visibility = GONE
            // 上传流量
            freeUsedUpRateItemView.visibility = VISIBLE
            freeUsedUpRateItemView.setEndText(getString(R.string.module_settings_free_used_up_rate,
                    StringUtils.readableFileSize(userInfo.usedUpRate),
                    StringUtils.readableFileSize(userInfo.freeDownRate)))
            // 同步文件夹名称
            sandboxScannerItemView.isEnabled = true
            // 自动上传
            autoUploadSwitch.isEnabled = true
            autoUploadSwitch.isChecked = userInfoInternal.autoUpload
            // 禁用 WiFi 上传
            onlyWiFiSwitch.isEnabled = true
            onlyWiFiSwitch.isChecked = userInfoInternal.onlyWiFi
            // 退出登出
            logoutButton.visibility = VISIBLE

        }

        findViewById<NavigationView>(R.id.navigation_view)!!.setOnNavigationViewListener(object : NavigationView.SimpleOnNavigationViewListener() {
            override fun onStartClicked(view: View?) {
                showDocumentsUi()
            }
        })

        notLoginItemView.setOnClickListener({
            // 统计设置登录
            RecordEvent.getInstance()
                    .recordEvent(EventParams.Builder(Event.login).build())
            showLoginUi()
        })

        downloadAppItemView.setOnClickListener({
            if (context != null) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("market://details?id=${Constants.PKG_NAME_NUTSTORE}")
                startActivity(intent)
            }
        })

        autoUploadSwitch.setOnCheckedChangeListener({ _, isChecked ->
            mPresenter.setAutoUpload(isChecked)
        })

        onlyWiFiSwitch.setOnCheckedChangeListener({ _, isChecked ->
            mPresenter.setOnlyWiFi(isChecked)
        })

        logoutButton.setOnClickListener({
            mPresenter.logout()
        })

        reportProblem.setOnClickListener({
            if (context != null) {
                val logFiles = L.getLogFiles(context!!.applicationContext)
                val address = Constants.FEEDBACK_EMAIL_ADDRESS
                val title = getString(R.string.module_settings_feedback_email_title, BuildConfig.VERSION_NAME)
                val text = getString(R.string.module_settings_feedback_email_text)
                val intent = IntentUtils.makeSendMailIntent(address, title, text, logFiles)
                if (intent.resolveActivity(context!!.packageManager) != null) {
                    context!!.startActivity(intent)
                } else {
                    ToastUtils.showShort(context!!, getString(R.string.common_not_found_activity_to_share))
                }
            }
        })

        // 在线客服
        onlineCustomerService.setOnClickListener({
            SobotApi.startSobotChat(context, Information().apply {
                appkey = Constants.SOBOT_APP_KEY
            })
        })
    }

    override fun showLoginUi() {
        if (context != null) {
            startActivity(WelcomeActivity.makeIntent(context!!))
        }
    }

    override fun showDocumentsUi() {

        if (activity == null) return

        val upIntent = NavUtils.getParentActivityIntent(activity!!)
        if (NavUtils.shouldUpRecreateTask(activity!!, upIntent!!)) {
            // This activity is NOT part of this app's task, so create a new task
            // when navigating up, with a synthesized back stack.
            TaskStackBuilder.create(activity!!)
                    // Add all of this activity's parents to the back stack
                    .addNextIntentWithParentStack(upIntent)
                    // Navigate up to the closest parent
                    .startActivities()
        } else {
            // This activity is part of this app's task, so simply
            // navigate up to the logical parent activity.
            NavUtils.navigateUpTo(activity!!, upIntent)
        }
    }

}