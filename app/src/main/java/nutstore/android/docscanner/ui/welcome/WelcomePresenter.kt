package nutstore.android.docscanner.ui.welcome

import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import nutstore.android.docscanner.data.UserInfoRepository
import nutstore.android.docscanner.util.Validator
import nutstore.android.sdk.NutstoreAPI
import nutstore.android.sdk.exception.ServerException
import nutstore.android.sdk.internal.HttpConfig
import nutstore.android.sdk.module.CaptchaResponse
import nutstore.android.sdk.module.LoginBody
import nutstore.android.sdk.module.VerifyEmailBody
import nutstore.android.sdk.module.VerifyResult
import nutstore.android.sdk.util.StringUtils
import nutstore.android.sdk.util.schedulers.BaseSchedulerProvider
import org.json.JSONException
import org.json.JSONObject
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*

class WelcomePresenter(private val mView: WelcomeContract.View,
                       private val mSchedulerProvider: BaseSchedulerProvider,
                       private val mNutstoreAPI: NutstoreAPI,
                       private val mUserInfoRepository: UserInfoRepository) : WelcomeContract.Presenter {


    private val mDisposable: CompositeDisposable = CompositeDisposable()

    /**
     * 短信验证分为两种：微信身份验证和手机身份验证
     */
    private var mPhone: String? = null

    init {
        mView.setPresenter(this)
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {
        mDisposable.clear()
    }

    override fun verifyCode(code: String) {
        mDisposable.clear()

        val disposable = mNutstoreAPI.verifyWxSso(code)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .doOnNext {
                    saveLoginInfo(it)
                }
                .subscribe(
                        {
                            if (it.userName != null && it.token != null) {
                                mView.showLoginSuccessUI()
                            } else if (it.unionid != null) {
                                mView.showWechatEmailUi(it.unionid)
                            }
                        },
                        {
                            handleError(it)
                        })

        mDisposable.add(disposable)
    }

    override fun verifyEmail(email: String, unionId: String) {

        check(Validator.validateEmail(email), { "Invalid email: $email" })

        mDisposable.clear()

        val disposable = mNutstoreAPI
                .fallbackCaptchaV1
                .flatMap({ t: CaptchaResponse ->
                    val verifyEmailBody = VerifyEmailBody()
                    verifyEmailBody.email = email
                    verifyEmailBody.custom_ticket = t.custom_ticket
                    verifyEmailBody.exp = t.exp
                    verifyEmailBody.sig = t.sig
                    verifyEmailBody.reusable = t.reusable
                    mNutstoreAPI.verifyEmailV1(verifyEmailBody)
                })
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            if (it.isNotReg) {
                                // 如果邮箱没有被注册过，无需密码直接去请求Token
                                bindingEmail(email, null, unionId)
                            } else {
                                // 如果邮箱已经被注册过，需要用户输入密码
                                mView.showWechatPasswordUi()
                            }
                        },
                        {
                            handleError(it)
                        })

        mDisposable.add(disposable)

    }

    override fun bindingEmail(email: String, password: String?, unionId: String) {

        check(Validator.validateEmail(email), { "Invalid email: $email" })

        if (StringUtils.isNotEmpty(password)) {
            check(Validator.validatePassword(password), { "Invalid password: $password" })
        }

        mDisposable.clear()

        val disposable = mNutstoreAPI
                .bindWxSso(email, password, unionId)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .doOnNext {
                    saveLoginInfo(it)
                }
                .subscribe(
                        {
                            check(it.userName != null && it.token != null && it.unionid == null)
                            mView.showLoginSuccessUI()
                        },
                        {
                            handleError(it)
                        })

        mDisposable.add(disposable)
    }

    /**
     * 尝试登录
     *
     * @param email    邮箱
     * @param password 密码
     * @param passcode 验证码
     */
    override fun attemptSignIn(email: String, password: String, passcode: String?) {

        check(Validator.validateEmail(email), { "Invalid email: $email" })

        check(Validator.validatePassword(password), { "Invalid password: $password" })

        if (StringUtils.isNotEmpty(passcode)) {
            check(Validator.isPasscode(passcode!!), { "Invalid passcode: $passcode" })
        }

        mDisposable.clear()

        val disposable = mNutstoreAPI
                .loginV1(LoginBody(email, password, passcode))
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .doOnNext {
                    saveLoginInfo(verifyResult = it)
                }
                .subscribe(
                        {
                            mView.showLoginSuccessUI()
                        },
                        {
                            handleError(it)
                        })

        mDisposable.add(disposable)
    }

    /**
     * 请求服务器端发送验证码
     *
     * @param email    用户名
     * @param password 密码
     */
    override fun attemptSendTfSms(email: String, password: String) {

        check(Validator.validateEmail(email), { "Invalid email: $email" })

        check(Validator.validatePassword(password), { "Invalid password: $password" })

        mDisposable.clear()

        val disposable = mNutstoreAPI
                .sendTfSms(LoginBody(email, password, null))
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        {
                            // nothing to do
                        },
                        {
                            handleError(it)
                        })

        mDisposable.add(disposable)
    }

    private fun saveLoginInfo(verifyResult: VerifyResult) {
        val machineName = HttpConfig.uuidToString(UUID.randomUUID())
        mUserInfoRepository.saveUsernameAndTokenAndMachineName(verifyResult.userName, verifyResult.token, machineName)
    }

    /**
     * 处理服务器端异常
     */
    private fun handleRequestError(e: ServerException) {
        when (e.getErrorCode()) {
            ServerException.NEED_TWO_FACTORS_AUTHENTICATION -> mView.showLoginNeedTwoFactorsAuthError()
            ServerException.NEED_SETUP_TWO_FACTORS_AUTH -> try {
                val jsonObject = JSONObject(e.getPayload())
                mView.showLoginNeedSetUpTwoFactorsAuthError(jsonObject.optString("url"))
            } catch (e1: JSONException) {
                e1.printStackTrace()
            }

            ServerException.NEED_SMS_AUTHENTICATION -> try {
                val json = JSONObject(e.getPayload())
                mPhone = json.optString("phone")
                if ("Wechat".equals(mPhone, ignoreCase = true)) {
                    mView.showLoginNeedWechatAuthError()
                } else if (!StringUtils.isEmpty(mPhone)) {
                    mView.showLoginNeedSmsAuthError(mPhone!!)
                } else {
                    throw NullPointerException("phone is null")
                }
            } catch (je: JSONException) {
                je.printStackTrace()
            }

            ServerException.NEED_WECHAT_AUTHENTICATION -> mView.showLoginNeedWechatAuthError()
            ServerException.TWOFACTOR_AUTH_FAILED -> if (mPhone != null) {
                if ("Wechat".equals(mPhone, ignoreCase = true)) {
                    mView.showLoginNeedWechatAuthError()
                } else if (!StringUtils.isEmpty(mPhone)) {
                    mView.showLoginNeedSmsAuthError(mPhone!!)
                }
            } else {
                mView.showLoginTwoFactorAuthError(e.getPayload())
            }
            ServerException.DISABLED_BY_TEAM_ADMIN -> mView.showLoginDisabledByTeamAdminError()
            ServerException.AUTHENTICATION_FAILED, ServerException.NO_SUCH_USER, ServerException.UNAUTHORIZED -> {
                mView.showLoginAuthFailedError()
            }
            ServerException.BLOCKED_TEMPORARILY -> {
                mView.showRequestSmsTooManyError()
            }
            else -> {
                mView.showUnknownError(e.detailMsg)
            }
        }
    }

    private fun handleError(throwable: Throwable) {
        if (throwable is UnknownHostException
                || throwable is ConnectException
                || throwable is SocketTimeoutException) {
            mView.showNetworkError()
        } else if (throwable is ServerException) {
            handleRequestError(throwable)
        } else {
            mView.showUnknownError(Log.getStackTraceString(throwable))
        }
    }
}