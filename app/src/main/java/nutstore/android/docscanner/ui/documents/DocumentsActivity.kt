package nutstore.android.docscanner.ui.documents

import android.content.Context
import android.content.Intent
import android.os.Bundle
import nutstore.android.docscanner.Injection
import nutstore.android.docscanner.R
import nutstore.android.docscanner.ui.base.BaseActivity

/**
 * @author Zhu Liang
 */
class DocumentsActivity : BaseActivity() {

    companion object {
        fun makeIntent(context: Context): Intent {
            return Intent(context, DocumentsActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_documents)

        var fragment = supportFragmentManager.findFragmentById(R.id.contentFrame)
        if (fragment == null) {
            fragment = DocumentsFragment()
            supportFragmentManager.beginTransaction().add(R.id.contentFrame, fragment).commit()
        }
        DocumentsPresenter(fragment as DocumentsContract.View, Injection.provideSchedulerProvider(),
                Injection.provideDocumentRepository(this),
                Injection.provideUserInfoRepository(this),
                Injection.provideNutstoreRepository(this))
    }


}