package nutstore.android.docscanner.ui.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhu Liang
 */

public abstract class RecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    protected final Context mContext;
    protected final List<T> mItems;
    private final int mLayoutId;
    private final LayoutInflater mInflater;

    public RecyclerAdapter(Context context, int layoutId) {
        this(context, new ArrayList<T>(), layoutId);
    }

    public RecyclerAdapter(Context context, List<T> items, int layoutId) {
        mContext = context;
        mItems = items;
        mLayoutId = layoutId;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(mLayoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        T value = mItems.get(position);
        bindView(holder, value);
    }

    protected abstract void bindView(@NonNull ViewHolder holder, @NonNull T value);

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public T getItem(int index) {
        return mItems.get(index);
    }

    public void replaceItems(List<T> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public List<T> getItems() {
        return mItems;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void setText(int id, CharSequence text) {
            ((TextView) findViewById(id)).setText(text);
        }

        public void setText(int id, int resId) {
            ((TextView) findViewById(id)).setText(resId);
        }

        public void setVisibility(int id, int visibility) {
            findViewById(id).setVisibility(visibility);
        }

        public <V extends View> V findViewById(int id) {
            return itemView.findViewById(id);
        }

        public void setImageResource(int id, int resId) {
            ((ImageView) findViewById(id)).setImageResource(resId);
        }

        public void setImageDrawable(int id, Drawable drawable) {
            ((ImageView) findViewById(id)).setImageDrawable(drawable);
        }
    }
}
