package nutstore.android.docscanner.ui.welcome

import android.net.Uri
import nutstore.android.sdk.ui.base.BasePresenter
import nutstore.android.sdk.ui.base.BaseView

/**
 * @author Zhu Liang
 */
interface WelcomeContract {

    interface Presenter : BasePresenter {
        fun verifyCode(code: String)
        fun verifyEmail(email: String, unionId: String)

        /**
         * 注册、绑定用户
         * 如果 email 已经被注册，属于绑定用户（不需要输入密码）；
         * 如果 email 没有被注册，属于注册用户（需要输入密码）；
         *
         * @param email     用户名(邮箱)
         * @param password  只有在绑定既有用户的时候需要
         * @param unionId    微信unionid
         */
        fun bindingEmail(email: String, password: String?, unionId: String)

        /**
         * 尝试登录
         *
         * @param email    邮箱
         * @param password 密码
         * @param passcode 验证码
         */
        fun attemptSignIn(email: String, password: String, passcode: String?)

        /**
         * 请求服务器端发送验证码
         *
         * @param email    用户名
         * @param password 密码
         */
        fun attemptSendTfSms(email: String, password: String)
    }

    interface View : BaseView<Presenter> {

        /**
         * 登录成功，进入主页面
         */
        fun showLoginSuccessUI()

        /**
         * 打开邮箱输入页面
         */
        fun showWechatEmailUi(unionId: String)

        /**
         * 打开密码输入页面
         */
        fun showWechatPasswordUi()

        /**
         * 启用手机身份验证
         *
         * @param uri 启用手机身份验证 Url
         */
        fun showSetUpTwoFactorsAuthUI(uri: Uri)

        /**
         * 用户开启了二次验证需要输入动态验证码
         *
         *
         * 参考：LoginFragment#loginFailsWithNeedTwoFactorsAuth
         */
        fun showLoginNeedTwoFactorsAuthError()

        /**
         * 您的账户被要求启用手机身份验证，否则无法登录。请通过网页登录账户，启用手机身份验证后重试。
         *
         *
         * 参考：LoginFragment#loginFailsWithNeedSetUpTwoFactorsAuth
         *
         * @param url 手机身份验证的网页
         */
        fun showLoginNeedSetUpTwoFactorsAuthError(url: String)

        /**
         * 微信身份验证
         *
         *
         * 参考：LoginFragment#loginFailsWithNeedSmsAuth
         */
        fun showLoginNeedWechatAuthError()

        /**
         * 手机身份验证
         *
         *
         * 参考：LoginFragment#loginFailsWithNeedSmsAuth
         *
         * @param phone 手机号
         */
        fun showLoginNeedSmsAuthError(phone: String)

        /**
         * 验证错误，请确认手机时间与当前时间（%1$s）误差小于十分钟
         *
         *
         * 参考：LoginFragment#loginFailsWithTwoFactorAuthFailed
         */
        fun showLoginTwoFactorAuthError(originPayload: String)

        /**
         * 你的账号已被管理员禁用，如需启用，请联系您的管理员
         *
         *
         * 参考：LoginFragment#loginFailsWithDisabledByTeamAdmin
         */
        fun showLoginDisabledByTeamAdminError()

        /**
         * 账户或密码有误，请重新输入\r\n如果您是企业版用户，请点击“登录企业版”
         *
         *
         * 参考：LoginFragment#loginFailsWithAuthFailed
         */
        fun showLoginAuthFailedError()

        /**
         * 显示网络异常
         */
        fun showNetworkError()

        /**
         * 请求短信的次数过多
         */
        fun showRequestSmsTooManyError()

        fun showUnknownError(message: String?)
    }
}