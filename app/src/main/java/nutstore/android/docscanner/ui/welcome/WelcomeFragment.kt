package nutstore.android.docscanner.ui.welcome

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.tencent.mm.opensdk.modelmsg.SendAuth
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import io.pigcasso.photopicker.findViewById
import nutstore.android.docscanner.Constants
import nutstore.android.docscanner.R
import nutstore.android.docscanner.common.SimpleTextWatcher
import nutstore.android.docscanner.util.Validator

/**
 * @author Zhu Liang
 */
class WelcomeFragment : Fragment() {

    private var mOnWelcomeListener: OnWelcomeListener? = null

    private lateinit var mEmailInput: TextInputLayout
    private lateinit var mPasswordInput: TextInputLayout
    private lateinit var mEmailEdit: EditText
    private lateinit var mPasswordEdit: EditText

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnWelcomeListener) {
            mOnWelcomeListener = context
        } else {
            throw ClassCastException()
        }
    }

    override fun onDetach() {
        super.onDetach()

        mOnWelcomeListener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mEmailInput = findViewById(R.id.inputlayout_welcome_email)!!
        mPasswordInput = findViewById(R.id.inputlayout_welcome_password)!!
        mEmailEdit = findViewById(R.id.et_welcome_email)!!
        mPasswordEdit = findViewById(R.id.et_welcome_password)!!

        mEmailEdit.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable) {
                super.afterTextChanged(s)
                mEmailInput.error = null
            }
        })

        mPasswordEdit.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable) {
                super.afterTextChanged(s)
                mPasswordInput.error = null
            }
        })

        findViewById<View>(R.id.fl_welcome_wechat_auth)!!.setOnClickListener({
            gotoWxAuth()
        })

        findViewById<View>(R.id.btn_welcome_sign_in)!!.setOnClickListener({
            if (mOnWelcomeListener != null) {
                val emailEdit = findViewById<EditText>(R.id.et_welcome_email)!!
                val passwordEdit = findViewById<EditText>(R.id.et_welcome_password)!!

                val email = emailEdit.text.toString()
                val password = passwordEdit.text.toString()

                if (validateEmail(email) && validatePassword(password)) {
                    mOnWelcomeListener!!.onSignIn(email, password)
                }
            }
        })

        findViewById<View>(R.id.text_welcome_forgetpassword)!!.setOnClickListener({
            showForgetPasswordUI()
        })
    }

    /**
     * 微信授权
     */
    private fun gotoWxAuth() {
        // send oauth request
        val req = SendAuth.Req()
        req.scope = "snsapi_userinfo"
        req.state = "wechat_sdk_demo_test"
        val api = WXAPIFactory.createWXAPI(context, Constants.WX_APP_ID)
        api.sendReq(req)
    }

    private fun validateEmail(email: String): Boolean {
        if (TextUtils.isEmpty(email)) {
            showEmailEmptyError()
            return false
        }
        if (email.length < Validator.EMAIL_MIN_LENGTH) {
            showEmailTooShortError()
            return false
        }
        if (email.length > Validator.EMAIL_MAX_LENGTH) {
            showEmailTooLongError()
            return false
        }
        if (!Validator.validateEmail(email)) {
            showEmailMalformedError()
            return false
        }
        return true
    }

    private fun validatePassword(password: String): Boolean {
        if (TextUtils.isEmpty(password)) {
            showPasswordEmptyError()
            return false
        }
        if (password.length < Validator.PASSWORD_MIN_LENGTH) {
            showPasswordTooShortError()
            return false
        }
        if (password.length > Validator.PASSWORD_MAX_LENGTH) {
            showPasswordTooLongError()
            return false
        }
        return true
    }

    private fun showEmailEmptyError() {
        mEmailInput.error = getString(R.string.module_welcome_please_enter_email_addr)
    }

    private fun showEmailTooShortError() {
        mEmailInput.error = getString(R.string.module_welcome_email_address_too_short)
    }

    private fun showEmailTooLongError() {
        mEmailInput.error = getString(R.string.module_welcome_email_address_too_long)
    }

    private fun showEmailMalformedError() {
        mEmailInput.error = getString(R.string.module_welcome_email_address_is_malformed)
    }

    private fun showPasswordEmptyError() {
        mPasswordInput.error = getString(R.string.module_welcome_please_enter_password)
    }

    private fun showPasswordTooShortError() {
        mPasswordInput.error = getString(R.string.module_welcome_password_too_short)
    }

    private fun showPasswordTooLongError() {
        mPasswordInput.error = getString(R.string.module_welcome_password_too_long)
    }

    private fun showForgetPasswordUI() {
        if (context != null) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(Constants.URL_RESET_PASSWORD)
            startActivity(intent)
        }
    }

    interface OnWelcomeListener {
        fun onSignIn(email: String, password: String)
    }
}