package nutstore.android.docscanner.ui.documents

import nutstore.android.docscanner.data.DSDocumentResult
import nutstore.android.docscanner.ui.base.DSBasePresenter
import nutstore.android.docscanner.ui.base.DSBaseView

/**
 * @author Zhu Liang
 */
interface DocumentsContract {
    interface Presenter : DSBasePresenter {
        fun loadToken()

        fun loadDocuments()

        /**
         * 手动或者自动同步文档
         *
         * @param manual 为 true 表示手动同步， 为 false 表示自动同步
         */
        fun syncDocuments(manual: Boolean, results: List<DSDocumentResult>)

        fun deleteDocuments(results: List<DSDocumentResult>)
    }

    interface View : DSBaseView<Presenter> {


        companion object {

            /**
             * 无效的模式
             */
            const val MODE_NONE = -1

            /**
             * 正常模式，允许上传，允许查看文档详情
             */
            const val MODE_NORMAL = 1

            /**
             * 选择模式下：
             *
             * 禁用上传，禁用查看文档详情
             *
             * 0. 如果用户没有选择任一文档，禁用删除和分享
             * 1. 如果用户选择了一个文档，允许用户删除和分享
             * 2. 如果用户选择了多个文档，允许用户删除，禁用分享
             *
             */
            const val MODE_CHOICE = 2
        }

        fun showDocuments(docs: List<DSDocumentResult>)

        fun showNoDocuments()

        fun showDocumentDetails(doc: DSDocumentResult)

        fun setLoadingIndicator(active: Boolean)

        fun setLoadingIndicatorEnabled(enabled: Boolean)

        fun showSettingsUi()

        fun smoothScrollToTop()

        /**
         * 切换模式
         * 详情请查看：[MODE_NORMAL], [MODE_CHOICE]
         */
        fun switchDocumentsMode(mode: Int)

        /**
         * 设备没有可用网络
         */
        fun showNetworkConnectedError()

        /**
         * 设备没有连接 WiFi
         */
        fun showWiFiConnectedError()

        /**
         * 文档创建失败
         */
        fun showFailedToCreateDocumentError()
    }
}