package nutstore.android.docscanner.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ly.count.android.sdk.Countly;
import ly.count.android.sdk.DeviceId;
import nutstore.android.docscanner.util.ToastUtils;

/**
 * @author Zhu Liang
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected void showToast(CharSequence text) {
        ToastUtils.showShort(this, text);
    }

    protected void showToast(int resId) {
        ToastUtils.showShort(this, resId);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Countly.sharedInstance().init(this, "https://countly.jianguoyun.com", "af47f37532210204170084fdcf4e3237e23a72cb", null, DeviceId.Type.ADVERTISING_ID);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Countly.sharedInstance().onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        Countly.sharedInstance().onStop();
    }
}
