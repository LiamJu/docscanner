package nutstore.android.docscanner.ui.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhu Liang
 */

public abstract class ListAdapter<T> extends BaseAdapter {

    protected final Context mContext;
    private LayoutInflater mInflater;
    protected final List<T> mItems;
    private final int mLayoutId;

    public ListAdapter(Context context, int layoutId) {
        this(context, new ArrayList<T>(), layoutId);
    }

    public ListAdapter(Context context, List<T> items, int layoutId) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
        mLayoutId = layoutId;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public T getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = mInflater.inflate(mLayoutId, parent, false);
            itemView.setTag(new ViewHolder(itemView));
        }
        ViewHolder holder = (ViewHolder) itemView.getTag();
        bindView(holder, getItem(position));
        return itemView;
    }

    protected abstract void bindView(ViewHolder holder, T value);

    public static class ViewHolder {

        public View itemView;

        public ViewHolder(View itemView) {
            this.itemView = itemView;
        }

        public void setText(int id, CharSequence text) {
            ((TextView) findViewById(id)).setText(text);
        }

        public void setText(int id, int resId) {
            ((TextView) findViewById(id)).setText(resId);
        }

        public void setVisibility(int id, int visibility) {
            findViewById(id).setVisibility(visibility);
        }

        public <V extends View> V findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
