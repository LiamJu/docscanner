package nutstore.android.docscanner.exception;

import android.os.Build;
import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nutstore.android.docscanner.BuildConfig;
import nutstore.android.docscanner.util.L;
import nutstore.android.sdk.util.JsonWrapper;
import nutstore.android.sdk.util.Preconditions;

/**
 * @author Zhu Liang
 */

public class DSUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    private static final String TAG = "DSUncaughtExceptionHandler";

    private Thread.UncaughtExceptionHandler defaultHandler;

    public DSUncaughtExceptionHandler(@NonNull Thread.UncaughtExceptionHandler defaultHandler) {
        this.defaultHandler = Preconditions.checkNotNull(defaultHandler);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Map<String, String> info = new HashMap<>();
        info.put("version_name", BuildConfig.VERSION_NAME);
        info.put("phone", Build.MODEL);
        info.put("release", Build.VERSION.RELEASE);
        info.put("time", DateFormat.getDateTimeInstance().format(new Date()));
        info.put("thread", t.getName());

        L.e(TAG, JsonWrapper.toJson(info), e);

        defaultHandler.uncaughtException(t, e);
    }
}
